-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 13, 2017 at 01:34 PM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_ceroc`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `state_id` int(6) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `city_order` int(6) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `state_id`, `city_name`, `city_order`, `status`, `created`, `modified`) VALUES
(1, 4, 'ss', 99999, 'ACTIVE', '2017-04-26 13:19:25', '2017-04-26 13:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `customertypes`
--

CREATE TABLE IF NOT EXISTS `customertypes` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `customer_type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_type_order` int(6) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `customertypes`
--

INSERT INTO `customertypes` (`id`, `customer_type_name`, `customer_type_order`, `status`, `created_by`, `created`, `modified`) VALUES
(1, 'dfghfgh', 5, 'ACTIVE', 1, '2017-03-31 06:21:37', '2017-03-31 06:21:52'),
(2, 'a', 4, 'ACTIVE', 1, '2017-03-31 06:21:58', '2017-03-31 06:22:03'),
(3, 'bnbn', 3, 'ACTIVE', 1, '2017-04-18 12:23:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `eventtypes`
--

CREATE TABLE IF NOT EXISTS `eventtypes` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `event_type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `event_type_order` int(6) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `eventtypes`
--

INSERT INTO `eventtypes` (`id`, `event_type_name`, `event_type_order`, `status`, `created_by`, `created`, `modified`) VALUES
(3, 'tryt', 100, 'ACTIVE', 0, '2017-03-30 12:19:13', '0000-00-00 00:00:00'),
(4, 'a', 3, 'INACTIVE', 0, '2017-03-30 12:19:18', '0000-00-00 00:00:00'),
(5, 'ahtr', 23, 'ACTIVE', 0, '2017-03-30 12:19:29', '0000-00-00 00:00:00'),
(6, 'fgf', 4, 'ACTIVE', 1, '2017-03-30 13:04:45', '2017-04-18 12:19:12'),
(7, 'none', 10, 'ACTIVE', 1, '2017-04-18 12:19:20', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `offer_title` varchar(25) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `offer_order` int(5) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE','DELETE') NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(5) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_title`, `cost`, `description`, `start_date`, `end_date`, `offer_order`, `status`, `created_by`, `created`, `modified`, `ip_address`) VALUES
(1, 'wew', 4.00, '4', '2017-04-12', '2017-04-27', 7, 'ACTIVE', 1, '2017-04-18 11:49:19', '2017-04-27 09:14:54', NULL),
(2, 'ere', 66.00, 'yuiuy', '2017-04-18', '2017-04-27', 9, 'ACTIVE', 1, '2017-04-26 07:46:33', '2017-04-27 09:14:42', NULL),
(3, 'ere 44', 333.00, '33', '2017-04-26', '2017-04-30', 99999, 'ACTIVE', 1, '2017-04-26 10:18:16', '0000-00-00 00:00:00', NULL),
(4, 'ytytyty ', 4.00, '5666', '2017-04-25', '2017-04-30', 99999, 'ACTIVE', 1, '2017-04-27 09:15:19', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rdbms_system`
--

CREATE TABLE IF NOT EXISTS `rdbms_system` (
  `rdbms_id` int(4) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(30) NOT NULL DEFAULT '',
  `p_key` varchar(20) NOT NULL DEFAULT '',
  `table_name_ref` varchar(30) NOT NULL DEFAULT '',
  `f_key` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`rdbms_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rdbms_system`
--

INSERT INTO `rdbms_system` (`rdbms_id`, `table_name`, `p_key`, `table_name_ref`, `f_key`) VALUES
(1, 'user_types', 'id', 'users', 'user_type'),
(2, 'states', 'id', 'cities', 'state_id'),
(3, 'cities', 'id', 'venues', 'city_id');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(160) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `contact_name` varchar(160) NOT NULL,
  `contact_phone` varchar(15) NOT NULL,
  `fax_no` varchar(100) NOT NULL,
  `site_address` varchar(255) NOT NULL,
  `site_fab_icon` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `page_limit` int(4) NOT NULL,
  `meta_title` varchar(160) NOT NULL,
  `meta_desc` varchar(255) NOT NULL,
  `sending_mail` varchar(190) NOT NULL,
  `recv_email` varchar(190) NOT NULL,
  `contact_email` varchar(190) NOT NULL,
  `facebook_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `google_plus_link` varchar(255) NOT NULL,
  `type` enum('SA') NOT NULL COMMENT '''SA''=>''Admin site setting''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_name`, `site_url`, `contact_name`, `contact_phone`, `fax_no`, `site_address`, `site_fab_icon`, `site_logo`, `page_limit`, `meta_title`, `meta_desc`, `sending_mail`, `recv_email`, `contact_email`, `facebook_link`, `twitter_link`, `google_plus_link`, `type`) VALUES
(1, 'Ceroc Attendance', 'http://192.168.5.15/ceroc/', 'Ceroc', '9876543211', '123456', 'Test Address', '1477658778.ico', '1474291859.png', 10, 'Ceroc', 'Ceroc', 'tradefix@tradefix.com', 'info@ceroc.com', 'contact@tradefix.com', 'https://facebook.com/', 'https://twitter.com/', 'https://plus.google.com/', 'SA');

-- --------------------------------------------------------

--
-- Table structure for table `stafftypes`
--

CREATE TABLE IF NOT EXISTS `stafftypes` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `staff_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type_order` int(6) NOT NULL DEFAULT '99999',
  `created_by` int(5) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_order` int(5) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `state_order`, `status`, `created`, `modified`) VALUES
(1, 'ACT', 1, 'ACTIVE', '0000-00-00 00:00:00', '2017-04-27 05:50:12'),
(2, 'NT', 2, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'WA', 9, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'NSW', 2, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'VIC', 8, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'QLD', 3, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'SA', 6, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'TAS', 7, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'IN', 5, 'ACTIVE', '2017-04-18 12:12:51', '0000-00-00 00:00:00'),
(11, 'ertr', 4, 'ACTIVE', '2017-04-26 05:41:18', '0000-00-00 00:00:00'),
(12, 'gghj', 99999, 'ACTIVE', '2017-04-27 06:05:54', '0000-00-00 00:00:00'),
(13, 'we', 99999, 'ACTIVE', '2017-05-05 12:14:31', '0000-00-00 00:00:00'),
(14, 'asdsa', 99999, 'ACTIVE', '2017-05-05 12:16:04', '0000-00-00 00:00:00'),
(15, 'fdgfg', 99999, 'ACTIVE', '2017-05-13 07:42:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `suburbs`
--

CREATE TABLE IF NOT EXISTS `suburbs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `state_id` int(6) NOT NULL,
  `suburb_name` varchar(100) NOT NULL,
  `suburb_order` int(6) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `suburbs`
--

INSERT INTO `suburbs` (`id`, `state_id`, `suburb_name`, `suburb_order`, `status`, `created`, `modified`) VALUES
(2, 4, 'sdfgdsfsd', 2, 'ACTIVE', '2017-04-27 05:31:34', '0000-00-00 00:00:00'),
(3, 1, 'ss', 1, 'ACTIVE', '2017-04-27 05:31:48', '2017-04-27 05:31:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `membership_no` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `state` varchar(100) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `comment` text NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `user_type` int(2) NOT NULL DEFAULT '3' COMMENT '1 -> Super Admin, 2 -> Office Administrator, 3 -> Venue staff, 4 -> Customer',
  `user_order` int(6) NOT NULL DEFAULT '99999',
  `phone` bigint(20) NOT NULL DEFAULT '0',
  `mobile` int(10) NOT NULL,
  `dob` date NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `created_by` int(1) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `membership_no`, `gender`, `firstname`, `lastname`, `email`, `username`, `password`, `address`, `state`, `postcode`, `comment`, `status`, `user_type`, `user_order`, `phone`, `mobile`, `dob`, `profile_image`, `created_by`, `remember_token`, `created`, `modified`) VALUES
(1, '', '', 'Admin', 'Admin', 'admin@gmail.com', 'admin', '0945fc9611f55fd0e183fb8b044f1afe', '', '', '', '', 'ACTIVE', 1, 99999, 76768786, 0, '0000-00-00', '', 0, NULL, '2017-03-21 11:18:53', '2017-05-11 09:56:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `school` varchar(150) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `other_phone` varchar(13) NOT NULL,
  `recommended` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `parent_name` varchar(160) NOT NULL,
  `parent_phone` varchar(20) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `bank_name` varchar(160) NOT NULL,
  `bank_code` varchar(25) NOT NULL,
  `branch_code` varchar(25) NOT NULL,
  `account_no` varchar(30) NOT NULL,
  `profile_image` varchar(190) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `dob`, `school`, `qualification`, `designation`, `phone`, `other_phone`, `recommended`, `remarks`, `parent_name`, `parent_phone`, `detail`, `address`, `zip`, `bank_name`, `bank_code`, `branch_code`, `account_no`, `profile_image`) VALUES
(1, 1, '2016-08-17', 'sdf', 'sasadf', 'sdf', '999', '999', '', '0', '', '', 'sdf', 'xxxxxxxxxxxxxxxx', '', 'dd', '', '', '', '1470117918.jpg'),
(2, 2, '2016-09-14', 'test school', '', '', '4456757', '', 'test rc', 'test RE', 'test parent', '34353453', 'Details', 'Address', '123456', '', '', '', '', '1473668296.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `type_id` int(2) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL,
  `type_code` char(2) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`type_id`, `type_name`, `type_code`) VALUES
(1, 'Site Administrator', 'SA'),
(2, 'Office Administrator', 'OA'),
(3, 'Venue Staff', 'VS'),
(4, 'Customer', 'CS');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE IF NOT EXISTS `venues` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `venue_name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `txt_password` varchar(50) NOT NULL,
  `venue_refer_id` varchar(100) NOT NULL,
  `user_id` int(4) NOT NULL,
  `state_id` int(5) NOT NULL,
  `suburb_id` int(5) NOT NULL,
  `venue_order` int(5) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE','DELETE') DEFAULT 'ACTIVE',
  `created_by` int(6) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `venue_name`, `username`, `password`, `txt_password`, `venue_refer_id`, `user_id`, `state_id`, `suburb_id`, `venue_order`, `status`, `created_by`, `created`, `modified`, `ip_address`) VALUES
(1, 'test', 'test12345678', '0945fc9611f55fd0e183fb8b044f1afe', 'nopass', '3c5c6fa199', 0, 4, 2, 4, 'ACTIVE', 1, '2017-04-26 13:30:41', '2017-04-28 09:39:21', '192.168.5.15'),
(2, 'test7', '1234567', '0945fc9611f55fd0e183fb8b044f1afe', 'nopass', '4564938e02', 0, 4, 2, 4, 'ACTIVE', 1, '2017-04-26 13:35:12', '2017-04-27 05:46:52', '192.168.5.15'),
(3, 'test2 y', 'admin yyyy', '0945fc9611f55fd0e183fb8b044f1afe', 'nopass', '78de7e2038', 0, 1, 3, 6, 'DELETE', 1, '2017-04-27 05:47:28', '2017-04-27 05:48:05', '192.168.5.15'),
(4, 'bbb', '123456', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'b965ee71c6', 0, 1, 3, 99999, 'ACTIVE', 1, '2017-05-02 06:15:25', '2017-05-05 12:48:02', '192.168.5.15'),
(5, '4rtretretretre', 'ertretretretretretretretvc vcd', 'e807f1fcf82d132f9bb018ca6738a19f', '1234567890', '3390504bd9', 0, 1, 3, 99999, 'ACTIVE', 1, '2017-05-04 07:13:52', '0000-00-00 00:00:00', '192.168.5.15'),
(6, 'dfdsfd', '', '', '', 'd0e8ed0a01', 0, 1, 3, 99999, 'ACTIVE', 1, '2017-05-05 12:47:43', '2017-05-05 12:47:59', '192.168.5.15');

-- --------------------------------------------------------

--
-- Table structure for table `zips`
--

CREATE TABLE IF NOT EXISTS `zips` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `state_id` int(6) NOT NULL,
  `city_id` int(6) NOT NULL,
  `zip` int(6) NOT NULL,
  `zip_order` int(6) NOT NULL DEFAULT '99999',
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
