<?php
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
class AppController extends Controller
{
    public function initialize(){
        parent::initialize();
        $BASE_APP_NAME='';
        $BASE_URL='http://192.168.5.129/projects/'.$BASE_APP_NAME;
       // $BASE_URL='http://'.$_SERVER['HTTP_HOST'].'/projects/'.$BASE_APP_NAME;
        $PAGE_DATA_LIMIT='10';
        $SITE_LOGO='home_logo.png';
        $SITE_FAB_ICO='';
        $SITE_ADDRESS="";
        $SITE_META_TITLE="";
        $SITE_META_DESC="";
        $SITE_SENDING_EMAIL="";
        $SITE_RECV_EMAIL="";
        $SITE_CONTACT_EMAIL="";
        $SITE_CONTACT_NAME="";
        $SITE_CONTACT_PHONE="";
        
        //========::::: Load Site Settings :::===========
        $SiteSettings=TableRegistry::get('SiteSettings');
        $SiteSetting= $SiteSettings->find()->first();
        
        define("BASE_APP_NAME",$BASE_APP_NAME);
        define("BASE_URL",$BASE_URL);
        define('PAGE_DATA_LIMIT',$PAGE_DATA_LIMIT);
        define('SITE_LOGO',$SITE_LOGO);
        define('SITE_ADDRESS',$SITE_ADDRESS);
        define('SITE_FAB_ICO',$SITE_FAB_ICO);
        define('SITE_META_TITLE',$SITE_META_TITLE);
        define('SITE_META_DESC',$SITE_META_DESC);
        define('SITE_SENDING_EMAIL',$SITE_SENDING_EMAIL);
        define('SITE_RECV_EMAIL',$SITE_RECV_EMAIL);
        define('SITE_CONTACT_EMAIL',$SITE_CONTACT_EMAIL);
        define('SITE_CONTACT_NAME',$SITE_CONTACT_NAME);
        define('SITE_CONTACT_PHONE',$SITE_CONTACT_PHONE);

        /*define('SITE_FACEBOOK_LINK',$SITE_FACEBOOK_LINK);
        define('SITE_TWITTER_LINK',$SITE_TWITTER_LINK);
        define('SITE_GOOGLE_PLUS_LINK',$SITE_GOOGLE_PLUS_LINK);*/
        
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
       
    }



    function _setErrorLayout() {
        if ($this->name == ‘CakeError’) {
        $this->layout = ‘error404’;
        }
        }

    public function beforeRender(Event $event){
       
        if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])) {
            $this->set('_serialize', true);
        }
         
    }
    //=======::: isAuthorized check=========
    public function isAuthorized($user){
        if (isset($user['user_type']['type_code']) && $user['user_type']['type_code'] !== 'STU') {
            return true;
        }
        return false;
    }
   //==========::: isAdminLogedIn check:::=========
   /* public function isAdminLogedIn(){
        $return=false;
        if($this->Auth->User('ADMIN')){
            $return=true;
        }
        
        return $return;
    }*/

   
    public function isSuperAdminLogedIn(){
        $return=false;
       
        if($this->request->session()->read('sp_admin.login')==1 && $this->request->session()->read('sp_admin.type')==1){
            $return=true;
        }
        
        return $return;
    }

    public function isVenueLogedIn(){
        $return=false;
       
        if($this->request->session()->read('venue.login')==1){
           $return=true;
        }
        
        return $return;
    }

   //==========::: Sitesettings :::=========
  /*  public function sitesettingsList(){
        $return=false;

        $SitesettingsTable=TableRegistry::get('site_settings');
        $sitedata=$SitesettingsTable->find('all',['conditions' =>['id'=>1]])->first();

        return $sitedata;

    }*/

  
   
    
}
