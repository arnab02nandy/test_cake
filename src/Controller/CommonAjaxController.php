<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
class CommonAjaxController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->loadComponent('Auth');
        $this->Auth->allow(['add','edit','delete']);
    }
    //========::: is Subject Code present :::============================
    public function isCodeExixts(){
        $this->autoRender=false;
        $status=true;
        $msg="Please try again later.";
        $this->request->allowMethod(['ajax']);
        
        $Users=TableRegistry::get('Users');
        $Levels=TableRegistry::get('Levels');
        $Subjects=TableRegistry::get('Subjects');
        //$Students=TableRegistry::get('Students');

        
        //====check with USERS:id=====
        if($this->request->data('user_id') && $this->request->data('code')){
            $is_user=$Users->exists(['id !='=>$this->request->data('user_id'),'code'=>trim($this->request->data('code'))]);
            $is_level=$Levels->exists(['code'=>trim($this->request->data('code'))]);
            $is_subject=$Subjects->exists(['code'=>trim($this->request->data('code'))]);
            //$is_student=$Students->exists(['code'=>trim($this->request->data('code'))]);
            if($is_user || $is_level || $is_subject){ 
             $status=false;
             $msg="Code Already exist, please try other code.";
            }
        } else if($this->request->data('level_id') && $this->request->data('code')){
            //====check with LEVEL:id=====
            $is_level=$Levels->exists(['id !='=>$this->request->data('level_id'),'code'=>trim($this->request->data('code'))]);
            $is_subject=$Subjects->exists(['code'=>trim($this->request->data('code'))]);
            $is_user=$Users->exists(['code'=>trim($this->request->data('code'))]);
            //$is_student=$Students->exists(['code'=>trim($this->request->data('code'))]);
            if($is_user || $is_level || $is_subject ){ 
             $status=false;
             $msg="Code Already exist, please try other code.";
            }
        }else if($this->request->data('subject_id') && $this->request->data('code')){
            //====check with SUBJECT:id=====
            $is_subject=$Subjects->exists(['id !='=>$this->request->data('subject_id'),'code'=>trim($this->request->data('code'))]);
            $is_level=$Levels->exists(['code'=>trim($this->request->data('code'))]);
            $is_user=$Users->exists(['code'=>trim($this->request->data('code'))]);
            //$is_student=$Students->exists(['code'=>trim($this->request->data('code'))]);
            if($is_user || $is_level || $is_subject){ 
             $status=false;
             $msg="Code Already exist, please try other code.";
            }
           
        } else{
            //====check All Code=====
            $is_user=$Users->exists(['code'=>trim($this->request->data('code'))]);
            $is_level=$Levels->exists(['code'=>trim($this->request->data('code'))]);
            $is_subject=$Subjects->exists(['code'=>trim($this->request->data('code'))]);
            //$is_student=$Students->exists(['code'=>trim($this->request->data('code'))]);
            if($is_user || $is_level || $is_subject){ 
             $status=false;
             $msg="Code Already exist, please try other code.";
            }
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    
    
    //========::: is Username present :::============================
    public function isUserNameExixts(){
        $this->autoRender=false;
        $status=true;
        $msg="Please try again later.";
        $this->request->allowMethod(['ajax']);
        $Users=TableRegistry::get('Users');
        //$Students=TableRegistry::get('Students');
        //====check with USERS:id=====
        if($this->request->data('user_id') && $this->request->data('username')){
            $is_user=$Users->exists(['id !='=>$this->request->data('user_id'),'username'=>trim($this->request->data('username'))]);
           // $is_student=$Students->exists(['username'=>trim($this->request->data('username'))]);
            if($is_user){ 
                $status=false;
                $msg="Username Already exist";
            }
        } else if($this->request->data('student_id') && $this->request->data('username')){
            //====check with Student:id=====
           // $is_student=$Students->exists(['id !='=>$this->request->data('student_id'),'username'=>trim($this->request->data('username'))]);
            $is_user=$Users->exists(['username'=>trim($this->request->data('username'))]);
            if($is_user){ 
                $status=false;
                $msg="Username Already exist";
            }
        }else{
            //$is_student=$Students->exists(['username'=>trim($this->request->data('username'))]);
            $is_user=$Users->exists(['username'=>trim($this->request->data('username'))]);
            if($is_user){ 
                $status=false;
                $msg="Username Already exist";
            }
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    
    //========::: is Email present :::============================
    public function isEmailExixts(){
        $this->autoRender=false;
        $status=true;
        $msg="Please try again later.";
        $this->request->allowMethod(['ajax']);
        $Users=TableRegistry::get('Users');
        //$Students=TableRegistry::get('Students');
        //====check with USERS:id=====
        if($this->request->data('user_id') && $this->request->data('email')){
            $is_user=$Users->exists(['id !='=>$this->request->data('user_id'),'email'=>trim($this->request->data('email'))]);
            //$is_student=$Students->exists(['email'=>trim($this->request->data('email'))]);
            if($is_user){ 
                $status=false;
                $msg="Email Already exist.";
            }
        } else if($this->request->data('student_id') && $this->request->data('email')){
            //====check with Student:id=====
            //$is_student=$Students->exists(['id !='=>$this->request->data('student_id'),'email'=>trim($this->request->data('email'))]);
            $is_user=$Users->exists(['email'=>trim($this->request->data('email'))]);
            if($is_user){ 
                $status=false;
                $msg="Email Already exist";
            }
        }else{
            //$is_student=$Students->exists(['email'=>trim($this->request->data('email'))]);
            $is_user=$Users->exists(['email'=>trim($this->request->data('email'))]);
            if($is_user){ 
                $status=false;
                $msg="Email Already exist";
            }
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    //============== ::: Get Fees BY LEBEL:ID ::===============
    public function getFeesByLebelID(){
        $this->autoRender=false;
        $status=false;
        $this->request->allowMethod(['ajax']);
        $Fees=TableRegistry::get('Fees');
        $Fees->hasOne('FeesCategoryMasters',[
            'className' => 'FeesCategoryMasters',
            'foreignKey' => false,
            'conditions' =>['Fees.cat_id = FeesCategoryMasters.id']
            ]);
        $LevelFeeListsData=array();
        if($this->request->data('level_id')){
            $status=true;
            $LevelFeeListsData=$Fees->find('all',['conditions'=>['Fees.level_id'=>$this->request->data('level_id'),'Fees.status !='=>'DELETE'],'contain'=>['FeesCategoryMasters'],'fields'=>['Fees.id','FeesCategoryMasters.name']])->combine('id','fees_category_master.name')->toArray();
        }  
        echo json_encode(array('status'=>$status,'data'=>$LevelFeeListsData));
        exit;
    }
  //======== ::: get total amount on student reg :::===========  
    //public function totalfeeAmountOnStuReg(){
    //    $this->autoRender=false;
    //    $status=false;
    //    $this->request->allowMethod(['ajax']);
    //    $Fees=TableRegistry::get('Fees');
    //    $Fees->belongsTo('Levels',[
    //        'className' => 'Levels',
    //        'foreignKey' => false,
    //        'conditions' =>['Fees.level_id = Levels.id']
    //        ]);
    //    $total_fee_amount='0';
    //    if($this->request->data('fee_id')){
    //        $status=true;
    //        $LevelFeeData=$Fees->find('all',['conditions'=>['Fees.id'=>$this->request->data('fee_id')],'contain'=>['Levels'],'fields'=>['Levels.registration_fee','Fees.amount']])->toArray();
    //        
    //        ////===========::: get all holyday list from session start date :::========
    //        //$Holydayes = TableRegistry::get('Holydayes');
    //        //$Holyday=$Holydayes->find('all')
    //        //->select(['id' => 'id', 'startDate' =>"DATE_FORMAT(start_date,'%Y-%m-%d')",'endDate' =>"DATE_FORMAT(end_date,'%Y-%m-%d')"])
    //        //->where("DATE_FORMAT(start_date,'%Y-%m-%d') >='".date('Y-m-d',strtotime($LevelFeeData[0]['start_date']))."' and status !='DELETE'")
    //        //->orWhere("DATE_FORMAT(end_date,'%Y-%m-%d') >='".date('Y-m-d',strtotime($LevelFeeData[0]['start_date']))."' and status !='DELETE'")
    //        //->toArray();
    //        ////pr($Holyday);
    //        
    //        
    //        $total_fee_amount +=isset($LevelFeeData['0']['amount']) ? trim($LevelFeeData['0']['amount']) : 0;
    //        $total_fee_amount +=isset($LevelFeeData['0']['level']['registration_fee']) ? trim($LevelFeeData['0']['level']['registration_fee']) : 0;   
    //    }
    //    echo json_encode(array('status'=>$status,'amount'=>$total_fee_amount));
    //    exit;
    //    
    //}
    
    public function totalfeeAmountOnStuReg(){
            $calculated_fee_amount=0;
            $this->autoRender=false;
            $status=true;
            $this->request->allowMethod(['ajax']);
            $total_class_days=[];
            $DayListArr=['1'=>'mon','2'=>'tue','3'=>'wed','4'=>'thu','5'=>'fri','6'=>'sat','7'=>'sun'];
            $DayListArr=array_flip($DayListArr);
            
            //======MonthArr=========
            $monthListArr=['1'=>'jan','2'=>'feb','3'=>'mar','4'=>'apr','5'=>'may','6'=>'jun','7'=>'jul','8'=>'aug','9'=>'sep','10'=>'oct','11'=>'nov','12'=>'dec'];
            
            $requested_fee_id=$this->request->data('fee_id');
            if(trim($requested_fee_id)!="" && trim($requested_fee_id)>0){
                //===========::: get selected Fee info :::============
                $Fees=TableRegistry::get('Fees');
                $Fees->belongsTo('Levels',[
                    'className' => 'Levels',
                    'foreignKey' => false,
                    'conditions' =>['Fees.level_id = Levels.id']
                    ]);
                $LevelFeeData=$Fees->find('all',['conditions'=>['Fees.id'=>$this->request->data('fee_id')],'contain'=>['Levels'],'fields'=>['Levels.registration_fee','Levels.id','Fees.id','Fees.amount','Fees.start_date','Fees.duration']])->toArray();
                
                $calculated_fee_amount=($LevelFeeData['0']['amount']+$LevelFeeData[0]['level']['registration_fee']);
                
                
                $start_date=date('Y-m-d',strtotime($LevelFeeData[0]['start_date']));
                $l_s_duration=$LevelFeeData[0]['duration'];
                $end_date=date('Y-m-d', strtotime('+'.$l_s_duration.' day', strtotime($start_date)));
                //
                $start_month=date('m',strtotime($start_date));
                $start_day=strtolower(date('D',strtotime($start_date)));
                $end_month=date('m',strtotime($end_date));
                //pr($LevelFeeData);
                
                //=============::: get schedule days & subject of selected level=============
                $Schedules=TableRegistry::get('Schedules');
                $Schedules->belongsTo('DayMasters',[
                    'className' => 'DayMasters',
                    'foreignKey' => false,
                    'conditions' =>['DayMasters.id=Schedules.day_id']
                    ]);
                $schedulesList=$Schedules->find('all',['conditions'=>['Schedules.level_id'=>$LevelFeeData[0]['level']['id']],'contain'=>['DayMasters'],'fields'=>['Schedules.id','DayMasters.id','DayMasters.name','DayMasters.code']])->toArray();
                //pr($schedulesList);
                
                //===============::: get all H-day :::================
                $Holydayes = TableRegistry::get('Holydayes');
                $Holyday=$Holydayes->find('all')
                ->select(['id' => 'id', 'startDate' =>"DATE_FORMAT(start_date,'%Y-%m-%d')",'endDate' =>"DATE_FORMAT(end_date,'%Y-%m-%d')"])
                ->where("DATE_FORMAT(start_date,'%Y-%m-%d') between '".$start_date."' and '".$end_date."' and status !='DELETE'")
                ->orWhere("DATE_FORMAT(end_date,'%Y-%m-%d') between '".$start_date."' and '".$end_date."' and status !='DELETE'")->toArray();
                //pr($Holyday);
                
                for($i=$start_date;$i<=$end_date;$i=date('Y-m-d',strtotime('+1 day', strtotime($i)))){
                    $is_holyday=false;
                    foreach($Holyday as $holyday){
                        for($h=$holyday['startDate'];$h<=$holyday['endDate'];$h=date('Y-m-d',strtotime('+1 day', strtotime($h)))){
                            if($h==$i){
                                $is_holyday=true;
                            }
                        }
                    }
                    if($is_holyday!==true){
                        //echo "</br>".$i;
                        
                        //0 or CAL_GREGORIAN - Gregorian Calendar
                        //1 or CAL_JULIAN - Julian Calendar
                        //2 or CAL_JEWISH - Jewish Calendar
                        //3 or CAL_FRENCH - French Revolutionary Calendar
                        $days = cal_days_in_month(CAL_GREGORIAN, 1, date('Y',strtotime($i)));
                        $week_day = date("N", mktime(0,0,0,date('m',strtotime($i)),1,date('Y',strtotime($i))));
                        $weeks = ceil(($days + $week_day) / 7);
                        $total_class_days[strtolower(date('M',strtotime($i)))]['total_week']=$weeks;
                        
                        //$num_of_days = date("t", mktime(0,0,0,date('m',strtotime($i)),1,date('Y',strtotime($i)))); 
                        //$l_day = date("t", mktime(0, 0, 0, date('m',strtotime($i)), 1, date('Y',strtotime($i)))); 
                        //$no_of_weeks = 0; 
                        //$count_weeks = 0; 
                        //while($no_of_weeks < $l_day){ 
                        //    $no_of_weeks += 7; 
                        //    $count_weeks++; 
                        //} 
                        //$total_class_days[strtolower(date('M',strtotime($i)))]['total_week']=$count_weeks;
                        
                        if($schedulesList){
                            foreach($schedulesList as $schedule_list){
                                if(trim(strtolower($schedule_list['day_master']['code']))==strtolower(date('D',strtotime($i)))){
                                    
                                    $current_week=(ceil((date("d",strtotime($i)) - date("w",strtotime($i)) - 1) / 7) + 1);
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['working_week'][]=$current_week;
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['working_week']=array_unique($total_class_days[strtolower(date('M',strtotime($i)))]['working_week']);
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['working_week']=array_values($total_class_days[strtolower(date('M',strtotime($i)))]['working_week']);
                                    
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['working_days'][$current_week][]=['date'=>$i,'day'=>strtolower(date('D',strtotime($i)))];
                                    
                                }   
                            }
                        }
                        
                    } else {
                        if($schedulesList){
                            foreach($schedulesList as $schedule_list){
                                if(trim(strtolower($schedule_list['day_master']['code']))==strtolower(date('D',strtotime($i)))){
                                    
                                    $current_week=(ceil((date("d",strtotime($i)) - date("w",strtotime($i)) - 1) / 7) + 1);
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week'][]=$current_week;
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']=array_unique($total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']);
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']=array_values($total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']);
                                    
                                    $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_days'][$current_week][]=['date'=>$i,'day'=>strtolower(date('D',strtotime($i)))];
                                    
                                }   
                            }
                        }  
                    } 
                }
                //===================::::: Fee calculate ::::===============
                
                if(!empty($total_class_days)){
                    foreach($total_class_days as $val){
                        $total_fee_amount=0;
                        $total_h_amount=0;
                        $total_week=0;
                        $per_week=0;
                        if(isset($val['working_week']) && !empty($val['working_week'])){
                            $per_week=($LevelFeeData[0]['amount']/$val['total_week']);
                            
                            if(isset($val['holiday_week']) && !empty($val['holiday_week'])){
                                foreach($val['holiday_week'] as $h_week_key=>$h_week_val){
                                    $h_amount=0;
                                    if(isset($val['holiday_days'][$h_week_val]) && !empty($val['holiday_days'][$h_week_val])){
                                        $h_amount +=($per_week/count($val['holiday_days'][$h_week_val]));
                                    }
                                    $total_h_amount=($h_amount*count($val['holiday_days'][$h_week_val]));
                                }
                            }
                        } 
                    }
                    $calculated_fee_amount=$calculated_fee_amount-$total_h_amount;  
                }
            }
            $requested_bill_amount='';
            $billable_item_ids=$this->request->data('billable_item_ids');
            if(trim($billable_item_ids)!=""){
                $BillableItems=TableRegistry::get('BillableItems');
                $requested_bill_amount=$BillableItems->find('all')->select(['total_billable_amount'=>'sum(amount)'])->where("id IN('".$billable_item_ids."')")->toArray();
                $BillableItems=TableRegistry::get('BillableItems');
                $BillableItem=$BillableItems->find('all')->select(['total_billable_amount'=>'sum(amount)'])->where("id IN(".$billable_item_ids.")")->toArray();
                $requested_bill_amount=isset($BillableItem[0]['total_billable_amount'])?$BillableItem[0]['total_billable_amount']:'0';
            }
            
            if(trim($requested_bill_amount)!=""){
                if($requested_bill_amount<0){
                    $requested_bill_amount=str_replace('-','',$requested_bill_amount);
                    $calculated_fee_amount=$calculated_fee_amount-($requested_bill_amount);
                }else{
                    $calculated_fee_amount=$calculated_fee_amount+($requested_bill_amount);
                } 
            }
            echo json_encode(array('status'=>$status,'amount'=>$calculated_fee_amount,'bill_ids'=>$requested_bill_amount));
            exit;    
    }
    
    
    
    
    
    //====================::: get TEST fee calculate ===========
    public function testFeeCalculate(){
            
        
            $total_class_days=[];
            $DayListArr=['1'=>'mon','2'=>'tue','3'=>'wed','4'=>'thu','5'=>'fri','6'=>'sat','7'=>'sun'];
            $DayListArr=array_flip($DayListArr);
            
            //======MonthArr=========
            $monthListArr=['1'=>'jan','2'=>'feb','3'=>'mar','4'=>'apr','5'=>'may','6'=>'jun','7'=>'jul','8'=>'aug','9'=>'sep','10'=>'oct','11'=>'nov','12'=>'dec'];
            
            //===========::: get selected Fee info :::============
            $Fees=TableRegistry::get('Fees');
            $Fees->belongsTo('Levels',[
                'className' => 'Levels',
                'foreignKey' => false,
                'conditions' =>['Fees.level_id = Levels.id']
                ]);
            $LevelFeeData=$Fees->find('all',['conditions'=>['Fees.id'=>1],'contain'=>['Levels'],'fields'=>['Levels.registration_fee','Levels.id','Fees.id','Fees.amount','Fees.start_date','Fees.duration']])->toArray();
            
            $start_date=date('Y-m-d',strtotime($LevelFeeData[0]['start_date']));
            $l_s_duration=$LevelFeeData[0]['duration'];
            $end_date=date('Y-m-d', strtotime('+'.$l_s_duration.' day', strtotime($start_date)));
            //
            $start_month=date('m',strtotime($start_date));
            $start_day=strtolower(date('D',strtotime($start_date)));
            $end_month=date('m',strtotime($end_date));
            //pr($LevelFeeData);
            
            //=============::: get schedule days & subject of selected level=============
            $Schedules=TableRegistry::get('Schedules');
            $Schedules->belongsTo('DayMasters',[
                'className' => 'DayMasters',
                'foreignKey' => false,
                'conditions' =>['DayMasters.id=Schedules.day_id']
                ]);
            $schedulesList=$Schedules->find('all',['conditions'=>['Schedules.level_id'=>1],'contain'=>['DayMasters'],'fields'=>['Schedules.id','DayMasters.id','DayMasters.name','DayMasters.code']])->toArray();
            //pr($schedulesList);
            
            
            //===============::: get all H-day :::================
            $Holydayes = TableRegistry::get('Holydayes');
            $Holyday=$Holydayes->find('all')
            ->select(['id' => 'id', 'startDate' =>"DATE_FORMAT(start_date,'%Y-%m-%d')",'endDate' =>"DATE_FORMAT(end_date,'%Y-%m-%d')"])
            ->where("DATE_FORMAT(start_date,'%Y-%m-%d') between '".$start_date."' and '".$end_date."' and status !='DELETE'")
            ->orWhere("DATE_FORMAT(end_date,'%Y-%m-%d') between '".$start_date."' and '".$end_date."' and status !='DELETE'")->toArray();
            //debug($Holyday);
            //pr($Holyday);
            
            for($i=$start_date;$i<=$end_date;$i=date('Y-m-d',strtotime('+1 day', strtotime($i)))){
                $is_holyday=false;
                foreach($Holyday as $holyday){
                    for($h=$holyday['startDate'];$h<=$holyday['endDate'];$h=date('Y-m-d',strtotime('+1 day', strtotime($h)))){
                        if($h==$i){
                            $is_holyday=true;
                        }
                    }
                }
                if($is_holyday!==true){
                    //echo "</br>".$i;
                    
                    //0 or CAL_GREGORIAN - Gregorian Calendar
                    //1 or CAL_JULIAN - Julian Calendar
                    //2 or CAL_JEWISH - Jewish Calendar
                    //3 or CAL_FRENCH - French Revolutionary Calendar
                    $days = cal_days_in_month(CAL_GREGORIAN, 1, date('Y',strtotime($i)));
                    $week_day = date("N", mktime(0,0,0,date('m',strtotime($i)),1,date('Y',strtotime($i))));
                    $weeks = ceil(($days + $week_day) / 7);
                    $total_class_days[strtolower(date('M',strtotime($i)))]['total_week']=$weeks;
                    
                    //$num_of_days = date("t", mktime(0,0,0,date('m',strtotime($i)),1,date('Y',strtotime($i)))); 
                    //$l_day = date("t", mktime(0, 0, 0, date('m',strtotime($i)), 1, date('Y',strtotime($i)))); 
                    //$no_of_weeks = 0; 
                    //$count_weeks = 0; 
                    //while($no_of_weeks < $l_day){ 
                    //    $no_of_weeks += 7; 
                    //    $count_weeks++; 
                    //}
                    //$total_class_days[strtolower(date('M',strtotime($i)))]['total_week']=$count_weeks;
                    
                    if($schedulesList){
                        foreach($schedulesList as $schedule_list){
                            if(trim(strtolower($schedule_list['day_master']['code']))==strtolower(date('D',strtotime($i)))){
                                
                                $current_week=(ceil((date("d",strtotime($i)) - date("w",strtotime($i)) - 1) / 7) + 1);
                                $total_class_days[strtolower(date('M',strtotime($i)))]['working_week'][]=$current_week;
                                $total_class_days[strtolower(date('M',strtotime($i)))]['working_week']=array_unique($total_class_days[strtolower(date('M',strtotime($i)))]['working_week']);
                                $total_class_days[strtolower(date('M',strtotime($i)))]['working_week']=array_values($total_class_days[strtolower(date('M',strtotime($i)))]['working_week']);
                                
                                $total_class_days[strtolower(date('M',strtotime($i)))]['working_days'][$current_week][]=['date'=>$i,'day'=>strtolower(date('D',strtotime($i)))];
                                
                            }   
                        }
                    }
                    
                } else {
                    if($schedulesList){
                        foreach($schedulesList as $schedule_list){
                            if(trim(strtolower($schedule_list['day_master']['code']))==strtolower(date('D',strtotime($i)))){
                                
                                $current_week=(ceil((date("d",strtotime($i)) - date("w",strtotime($i)) - 1) / 7) + 1);
                                $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week'][]=$current_week;
                                $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']=array_unique($total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']);
                                $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']=array_values($total_class_days[strtolower(date('M',strtotime($i)))]['holiday_week']);
                                
                                $total_class_days[strtolower(date('M',strtotime($i)))]['holiday_days'][$current_week][]=['date'=>$i,'day'=>strtolower(date('D',strtotime($i)))];
                                
                            }   
                        }
                    }  
                } 
            }
            //===================::::: Fee calculate ::::===============
            echo "==Total-Amount=>>".$calculated_fee_amount=($LevelFeeData['0']['amount']+$LevelFeeData['0']['level']['registration_fee']).'</br>';  
            if(!empty($total_class_days)){
                foreach($total_class_days as $val){
                    $total_fee_amount=0;
                    $total_h_amount=0;
                    $total_week=0;
                    $per_week=0;
                    if(isset($val['working_week']) && !empty($val['working_week'])){
                        //if(isset($val['working_week']) && !empty($val['working_week'])){
                        //   $total_week +=count($val['working_week']);
                        //}
                        //if(isset($val['holiday_week']) && !empty($val['holiday_week'])){
                        //   $total_week +=count($val['holiday_week']);
                        //}
                        //$per_week=($LevelFeeData[0]['amount']/$total_week);
                        
                       echo "====Per-Week===>>".$per_week=($LevelFeeData[0]['amount']/$val['total_week'])."</br>";
                        
                        //if(isset($val['working_week']) && !empty($val['working_week'])){
                        //    foreach($val['working_week'] as $w_week_key=>$w_week_val){
                        //        $per_class=0;
                        //        if(isset($val['working_days'][$w_week_val])){
                        //            $per_class=($per_week/count($val['working_days'][$w_week_val]));
                        //        }
                        //        $total_fee_amount +=$per_class*count($val['working_days'][$w_week_val]);
                        //    }
                        //}
                        
                        if(isset($val['holiday_week']) && !empty($val['holiday_week'])){
                            foreach($val['holiday_week'] as $h_week_key=>$h_week_val){
                                $h_amount=0;
                                if(isset($val['holiday_days'][$h_week_val]) && !empty($val['holiday_days'][$h_week_val])){
                                    $h_amount +=($per_week/count($val['holiday_days'][$h_week_val]));
                                }
                                 echo "====h-Amount===>>".$total_h_amount=($h_amount*count($val['holiday_days'][$h_week_val]))."</br>";
                            }
                        }
                    } 
                }
                $calculated_fee_amount=$calculated_fee_amount-$total_h_amount; 
            }
            
            echo "======>>> Calculated fee amount===>>".$calculated_fee_amount."</br>";
            pr($total_class_days);
            die;
    }
    
    
    
    public function testLevelGridView(){
        $Schedules = TableRegistry::get('Schedules');  // load  Schedules  model
        $Schedules->hasOne('Subjects',[
                'className' => 'Subjects',
                'foreignKey' => false,
                'conditions' =>['Schedules.subject_id = Subjects.id']
                ]);
        $Schedules->hasOne('DayMasters',[
                'className' => 'DayMasters',
                'foreignKey' => false,
                'conditions' =>['Schedules.day_id = DayMasters.id']
                ]);
        $schedule=$Schedules->find('all',[
                        'conditions'=>['Schedules.status'=>'ACTIVE'],
                        'contain'=>['Subjects','DayMasters'],
                        'fields'=>[
                            'Schedules.id','Schedules.level_id','Schedules.subject_id','Schedules.day_id','Schedules.start_time','Schedules.end_time',
                            'Subjects.id','Subjects.name','Subjects.code',
                            'DayMasters.id','DayMasters.name'
                        ]
                    ])->toArray();
        
        $Fees = TableRegistry::get('Fees');  // load  Fees  model
        $Fees->hasOne('Levels',[
                'className' => 'Levels',
                'foreignKey' => false,
                'conditions' =>['Fees.level_id = Levels.id']
                ]);
        $Fees->hasOne('FeesCategoryMasters',[
                'className' => 'FeesCategoryMasters',
                'foreignKey' => false,
                'conditions' =>['Fees.cat_id = FeesCategoryMasters.id']
                ]);
        
        $Fees->belongsTo('FeesSubjectMaps',[
                'className' => 'FeesSubjectMaps',
                'foreignKey' =>false,
                'conditions' =>['FeesSubjectMaps.fees_id = Fees.id']
                ]);
        $Fees->hasOne('Subjects',[
                'className' => 'Subjects',
                'foreignKey' =>false,
                'conditions' =>['FeesSubjectMaps.subject_id = Subjects.id']
                ]);
        
         $fee=$Fees->find('all',[
                    'conditions'=>['Fees.status'=>'ACTIVE'],
                    'contain'=>['Levels','FeesSubjectMaps','FeesCategoryMasters','Subjects'],
                    'fields'=>[
                        'Levels.id','Levels.name','Levels.code','Levels.registration_fee',
                        'Fees.id','Fees.level_id','Fees.amount','Fees.duration',
                        'FeesCategoryMasters.id','FeesCategoryMasters.name','FeesCategoryMasters.code',
                        'Subjects.id','Subjects.name','Subjects.code'
                    ]
                ])->toArray();
         $fee_res_arr=[];
         if(!empty($fee)){
            foreach($fee as $val){
                if(!isset($fee_res_arr[$val['level_id']]['level'])){
                   $fee_res_arr[$val['level_id']]['level']=['id'=>$val['level']['id'],'name'=>$val['level']['name'],'code'=>$val['level']['code'],'registration_fee'=>$val['level']['registration_fee']]; 
                }
                $fee_res_arr[$val['level_id']]['fees'][$val['fees_category_master']['code']]=[
                    'id'=>$val['id'],
                    'level_id'=>$val['level_id'],
                    'amount'=>$val['amount'],
                    'duration'=>$val['duration'],
                    'category_code'=>$val['fees_category_master']['code'],
                    'category_name'=>$val['fees_category_master']['name']
                ];
                $fee_res_arr[$val['level_id']]['subjects'][$val['fees_category_master']['code']][]=['id'=>$val['subject']['id'],'code'=>$val['subject']['code'],'name'=>$val['subject']['name']];
            }
         }
         $schedule_result_arr=[];
         if(!empty($schedule)){
            foreach($schedule as $val){
                
                $schedule_result_arr[$val['level_id']]['schedule_level']=isset($fee_res_arr[$val['level_id']]) ? $fee_res_arr[$val['level_id']]: array();
                $schedule_result_arr[$val['level_id']]['schedule'][]=[
                    'id'=>$val['id'],
                    'start_time'=>$val['start_time'],
                    'end_time'=>$val['end_time'],
                    'day_master'=>[
                        'id'=>$val['day_master']['id'],
                        'name'=>$val['day_master']['name'],
                    ],
                    'subject'=>[
                        'id'=>$val['subject']['id'],
                        'code'=>$val['subject']['code'],
                        'name'=>$val['subject']['name'],
                    ],
                   
                ];
                
                
                
            }
         }
         
         
         pr($schedule_result_arr);
         
         
         
         
         
        //pr($fee_res_arr);
        
       // pr($fee);
        //pr($schedule);
        TableRegistry::clear();
        die;
        //TableRegistry::clear(); //flash the table registry
    }
    
}
?>