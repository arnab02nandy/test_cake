<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class EventtypesController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);     
               
    }

    //======  Function for listing event types ==========
    public function eventtypeList(){
        
        //--------- is admin login ------------
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }         

        $data['heading']="Event Type";
        $data['left_sidebar_parent']="Event-type-list";        
        $meta_data['meta_title']="Eventtype-List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        //$eventtypes=$this->Eventtypes->find('all')->order(['event_type_order' => 'ASC','event_type_name' => 'ASC']);

        $connection = ConnectionManager::get('default');
        $eventtypes = $connection->execute('SELECT * FROM eventtypes order by event_type_order ASC,event_type_name ASC ')->fetchAll('assoc');

         /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
           $connection = ConnectionManager::get('default');
           $order= $this->request->data['event_type_order'];
           //print_r($order);exit;
           foreach($order as $key=> $val)
           {
            if($val!='')
            {
            $query = $connection->execute("UPDATE eventtypes SET  event_type_order='".$val."' WHERE id='".$key."'");
            }
           }
        $this->Flash->success('Event type order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/eventtype-list');
        }

        $this->set(compact('eventtypes'));
        $this->set('_serialize', ['eventtypes']);
    }
    
    //======  Function for add  event type  ==========
    public function eventtypeAdd(){
        
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }
        $data['heading']="Add Event Type";        
        $meta_data['meta_title']="Add-Event Type | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');

        $eventtype = $this->Eventtypes->newEntity();
        if($this->request->is('post')){
            $this->request->data['created_by']=$this->request->session()->read('sp_admin.id');
            $this->request->data['created']=date('Y-m-d H:i:s');
            $eventtype = $this->Eventtypes->patchEntity($eventtype,$this->request->data);
            if($this->Eventtypes->save($eventtype)){
                $this->Flash->success('New event type has been added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/eventtype-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('eventtype'));
        $this->set('_serialize', ['eventtype']);
    }

    //======  Function for edit event type =========
    public function eventtypeEdit($id = null){
      
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }

        $data['heading']="Edit Event Type";
        
        $meta_data['meta_title']="Edit-Event Type | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');
        $eventtype = $this->Eventtypes->get($id);
        if(!isset($eventtype->id) || trim($eventtype->id)<=0){
            $this->Flash->error('Invalid request',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
           return $this->redirect('/admin/eventtype-list');
        }
        if($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $eventtype = $this->Eventtypes->patchEntity($eventtype,$this->request->data);
            if($this->Eventtypes->save($eventtype)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/eventtype-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('eventtype'));
        $this->set('_serialize', ['eventtype']);
    }
    
    //======  Function for change status of event type ==========
   
    public function changeStatus($id = null){
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $eventtype = $this->Eventtypes->get($this->request->data('id'));
        if($eventtype){
            $change_status=trim($eventtype->status)=='ACTIVE' ? "INACTIVE" : "ACTIVE";
            $eventtype->status=$change_status;
            if($this->Eventtypes->save($eventtype)){
                $status='SUCCESS';
                $msg="Record status has been changed successfully.";
            }else{
                $change_status=trim($eventtype->status)=='INACTIVE' ? "ACTIVE" : "INACTIVE";
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg,"change_status"=>$change_status));
        exit;
    }
    
    //======  Function for delete event type ==========
    public function eventtypeDelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $eventtype = $this->Eventtypes->get($this->request->data('id'));
        if($eventtype){       	
			
            $result = $this->Eventtypes->delete($eventtype);         
			if($result){				
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }

    //======  Function for event type exist ==========
    public function eventtypeExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('event_type_name')){
            $counter=$this->Eventtypes->find('all',['conditions' =>['event_type_name'=>trim($this->request->data('event_type_name')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('event_type_name')){
            $counter=$this->Eventtypes->find('all',['conditions' =>['event_type_name'=>trim($this->request->data('event_type_name'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Event type is already exist, please enter other.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
}
?>