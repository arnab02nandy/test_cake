<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class CustomertypesController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);     
               
    }

    //======  Function for listing customer types ==========
    public function customertypeList(){
        
        //--------- is admin login ------------
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }         

        $data['heading']="Customer Type";
        $data['left_sidebar_parent']="customer-type-list";        
        $meta_data['meta_title']="Customertype List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
       // $customertypes=$this->Customertypes->find('all')->order(['customer_type_order' => 'ASC','customer_type_name' => 'ASC']);

        $connection = ConnectionManager::get('default');
        $customertypes = $connection->execute('SELECT * FROM customertypes order by customer_type_order ASC,customer_type_name ASC ')->fetchAll('assoc');
       // echo "<pre>";print_r($customertypes);exit;


         /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
         $connection = ConnectionManager::get('default');
         $order= $this->request->data['customer_type_order'];          
         foreach($order as $key=> $val)
         {
         if($val!='')
          {
            $query = $connection->execute("UPDATE customertypes SET  customer_type_order='".$val."' WHERE id='".$key."'");
          }
         }
         $this->Flash->success('Customer type order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/customertype-list');
        }

        $this->set(compact('customertypes'));
        $this->set('_serialize', ['customertypes']);
    }
    
    //======  Function for add  customer type  ==========
    public function customertypeAdd(){
        
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }
        $data['heading']="Add Customer Type";        
        $meta_data['meta_title']="Add Customer Type | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');

        $customertype = $this->Customertypes->newEntity();
        if($this->request->is('post')){
            $this->request->data['created_by']=$this->request->session()->read('sp_admin.id');
            $this->request->data['created']=date('Y-m-d H:i:s');
            $customertype = $this->Customertypes->patchEntity($customertype,$this->request->data);
            if($this->Customertypes->save($customertype)){
                $this->Flash->success('New customer type has been added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/customertype-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('customertype'));
        $this->set('_serialize', ['customertype']);
    }

    //======  Function for edit customer type =========
    public function customertypeEdit($id = null){
      
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }

        $data['heading']="Edit Customer Type";
        
        $meta_data['meta_title']="Edit Customer Type | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');
        $customertype = $this->Customertypes->get($id);
        if(!isset($customertype->id) || trim($customertype->id)<=0){
            $this->Flash->error('Invalid request',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
           return $this->redirect('/admin/customertype-list');
        }
        if($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $customertype = $this->Customertypes->patchEntity($customertype,$this->request->data);
            if($this->Customertypes->save($customertype)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/customertype-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('customertype'));
        $this->set('_serialize', ['customertype']);
    }
    
    //======  Function for change status of customer type ==========
   
    public function changeStatus($id = null){
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $customertype = $this->Customertypes->get($this->request->data('id'));
        if($customertype){
            $change_status=trim($customertype->status)=='ACTIVE' ? "INACTIVE" : "ACTIVE";
            $customertype->status=$change_status;
            if($this->Customertypes->save($customertype)){
                $status='SUCCESS';
                $msg="Record status has been changed successfully.";
            }else{
                $change_status=trim($customertype->status)=='INACTIVE' ? "ACTIVE" : "INACTIVE";
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg,"change_status"=>$change_status));
        exit;
    }
    
    //======  Function for delete customer type ==========
    public function customertypeDelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $customertype = $this->Customertypes->get($this->request->data('id'));
        if($customertype){       	
			
            $result = $this->Customertypes->delete($customertype);         
			if($result){				
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }

    //======  Function for customer type exist ==========
    public function customertypeExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('customer_type_name')){
            $counter=$this->Customertypes->find('all',['conditions' =>['customer_type_name'=>trim($this->request->data('customer_type_name')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('customer_type_name')){
            $counter=$this->Customertypes->find('all',['conditions' =>['customer_type_name'=>trim($this->request->data('customer_type_name'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Customer type is already exist, please enter other.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
}
?>