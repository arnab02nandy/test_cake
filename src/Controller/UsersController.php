<?php
/* This controller is for admin functionalities */
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;


class UsersController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);       
    }

     //======  Super Administrator Dashboard ==========
     public function index(){       

        $this->viewBuilder()->layout('admin');
        $data['left_sidebar_parent']="dashboard";
        $data['left_sidebar_sub']="dashboard";
        $data['page_content']="Welcome to ".SITE_META_TITLE;
        $data['heading']="Welcome to ".SITE_META_TITLE;
        $meta_data['meta_title']="Home | ".SITE_META_TITLE;
        $meta_data['meta_desc']="Ceroc";
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        
        $Users=TableRegistry::get('users'); // Users model
        $Users->hasOne('user_types',[
            'className' => 'user_types',
            'foreignKey' => false,
            'conditions' =>['user_types.type_id=users.user_type_id']
        ]);
      
    }
     //======  Super Administrator Login ==========
     public function login(){          
       
        //======is admin login==========
        if($this->isSuperAdminLogedIn()===true){
        return $this->redirect('/admin/home');                
        }   
        $session = $this->request->session();            
        $data['heading']="Admin-Login";
        $meta_data['meta_title']="Admin-Login | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        if ($this->request->is('post'))
        {           
           $password=md5($this->request->data["password"]);             
           $adminlogin=$this->Users->find('all',['conditions'=>['status'=>'ACTIVE','user_type'=>1,'username'=>$this->request->data["username"] ,'password'=>$password]])->toArray();
           $count=count($adminlogin);    
           if($count>0)
           {
           $adminlogin=$adminlogin[0];     
          
           $session->write(['sp_admin.id'=>$adminlogin->id,'sp_admin.firstname'=>$adminlogin->firstname,'sp_admin.lastname'=>$adminlogin->lastname,'sp_admin.email'=>$adminlogin->email,'sp_admin.type'=>$adminlogin->user_type,'sp_admin.login'=>1]);  
           return $this->redirect('/admin/home');      
           
           }
           $this->Flash->error(__('Invalid username or password, try again'));
             
        }
    }
   
    //======  Super Administrator Profile Edit ==========
    public function myProfile($id = null){  

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }      
        $session = $this->request->session();  
        $data['heading']="My Profile";
        $data['left_sidebar_parent']="profile";
        $meta_data['meta_title']="Profile | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $user = $this->Users->get($session->read('sp_admin.id'));
        if($this->request->is(['patch', 'post', 'put'])){
            $data_error=array();

                $this->request->data['modified']=date('Y-m-d H:i:s');
                $user = $this->Users->patchEntity($user, $this->request->data);
                if($this->Users->save($user)) {
                    
                    $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                    return $this->redirect('/admin/profile');
                } else {
                    
                    $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
                }
           
            $this->set(compact('data_error'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    //======  Super Administrator Change Password ==========
    public function changePassword(){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 
        $session = $this->request->session(); 
        $data['heading']="Change Password";
        $data['left_sidebar_parent']="change-password";
        $meta_data['meta_title']="ChangePassword | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $user = $this->Users->get($session->read('sp_admin.id'));
        if($this->request->is(['patch', 'post', 'put']))
        {
           /* $data_error=array();
                        
            $this->request->data['id']=$this->request->data('id');
            $password=md5($this->request->data('new_password'));
            $this->request->data['password']=$password;
            $this->request->data['modified']=date('Y-m-d H:i:s');
            
            $user = $this->Users->patchEntity($user, ['password'=>$password]);
            
            if($this->Users->save($user)) {
                $this->Flash->success('Password has been changed successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/change-password');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
            $this->set(compact('data_error'));*/


        $con = ConnectionManager::get('default');
        $query = $con->execute("UPDATE users SET  password='".md5($this->request->data('new_password'))."' WHERE id='".$session->read('sp_admin.id')."'");                
        $this->Flash->success('Password has been changed successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
        return $this->redirect('/admin/change-password');

        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
   
    //======  Super Administrator Logout ==========
    public function logout(){
        $session = $this->request->session();
        $session->delete('sp_admin.id');
        $session->delete('sp_admin.firstname');
        $session->delete('sp_admin.lastname');
        $session->delete('sp_admin.type');
        $session->delete('sp_admin.login');
        return $this->redirect('/admin/login');     
    }
    
    //######################### ::: Ajax all Action :::########################
    //=============::: change  status ::: =======================
    public function changestatus($id = null){
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        
        $user = $this->Users->get($this->request->data('id'));
        if($user){
            $change_status=trim($user->status)=='ACTIVE' ? "INACTIVE" : "ACTIVE";
            $user->status=$change_status;
            if($this->Users->save($user)){
                $status='SUCCESS';
                $msg="Record status has been changed successfully.";
            }else{
                $change_status=trim($user->status)=='INACTIVE' ? "ACTIVE" : "INACTIVE";
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg,"change_status"=>$change_status));
        exit;
    }
   
   
    //=============::: Delete user  ::: =======================
    public function ajaxdelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $user = $this->Users->get($this->request->data('id'));
        if($user)
        {
            $this->request->data['status']='DELETE';
            $user = $this->Users->patchEntity($user, $this->request->data);
            if($this->Users->save($user)) {
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }
    
    //========::: is UserName exist :::============================
    public function isUsernameExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('username')){
            $counter=$this->Users->find('all',['conditions' =>['username'=>trim($this->request->data('username')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('username')){
            $counter=$this->Users->find('all',['conditions' =>['username'=>trim($this->request->data('username'))]])->count('id');
        }
        
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Username exist, please enter other username.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    
    //========::: is Email exist :::============================
    public function isEmailidExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('email')){
            $counter=$this->Users->find('all',['conditions' =>['email'=>trim($this->request->data('email')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('email')){
            $counter=$this->Users->find('all',['conditions' =>['email'=>trim($this->request->data('email'))]])->count('id');
        }        
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Email-id exist, please enter other email-id";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    
    //=============::: is old password valid in change password :::===========
    public function isOldpasswordValid(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        $user=$this->Users->find('all',['conditions'=>['id'=>$this->request->data('id')]])->first();
        if(isset($user->password))
        {            
            if(md5($this->request->data('old_password'))==$user->password){
                $status=true;
                $msg="success";  
            }else{
                 $status=false;
                 $msg="Old password does not match.";
            } 
        }else{
            $status=false;
            $msg="Old password does not match.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    //=============::: is Code Exixts:::===========
    public function isCodeExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('code')){
            $counter=$this->Users->find('all',['conditions' =>['code'=>trim($this->request->data('code')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('code')){
            $counter=$this->Users->find('all',['conditions' =>['code'=>trim($this->request->data('code'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Code exist, please enter other code";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
    ############################################################
       //=============::: For Venue Staff:::=========== //


     public function venuestaffList(){
       
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 

        $data['heading']="Venue Staff";
        $data['left_sidebar_parent']="venue-staff";
        $data['left_sidebar_sub']="stafflist";
        $meta_data['meta_title']="Venue-Staff-List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $rows=$this->Users->find('all',["conditions"=>["id !="=>$this->request->session()->read('sp_admin.id'),'status !='=>'DELETED','user_type'=>3]])->order(['user_order' => 'ASC']);

         /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
           $connection = ConnectionManager::get('default');
           $order= $this->request->data['user_order'];
           //print_r($order);exit;
           foreach($order as $key=> $val)
           {
            if($val!='')
            {
            $query = $connection->execute("UPDATE users SET  user_order='".$val."' WHERE id='".$key."'");
            }
           }
        $this->Flash->success('Venue staff order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/venue-staff-list');
        }




        $this->set(compact('rows'));
        $this->set('_serialize', ['rows']);
        TableRegistry::clear(); 
    }
    //==========:::: Add Venue Staff :::=====================
    public function venuestaffAdd(){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }        
        
        $data['heading']="Add Venue Staff";
        $data['left_sidebar_parent']="venue-staff";
        $data['left_sidebar_sub']="addstaff";
        $meta_data['meta_title']="Add-Venue-Staff | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');
        $user = $this->Users->newEntity();
        
        if($this->request->is('post'))
        {
            
          /* if(isset($this->request->data['profile_image']['name']) && trim($this->request->data['profile_image']['name'])!=""){
             
                $file = $this->request->data['profile_image'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    if(in_array($ext,$arr_ext)){
                        $file['name']=time().'.'.$ext;
                        $upload_file_name=$file['name'];                  
                    move_uploaded_file($file['tmp_name'], WWW_ROOT .'uploads/profile_image/'. $file['name']);
                    }else{                                                                  
                        $error_counter++;
                        $data_error['service_logo']="Upload image in jpg,jpeg,gif or png";
                 
                    }
                    //==============image upload========
                $this->loadComponent('ImageResize');
                $original_path = WWW_ROOT.'uploads/profile_image/'.$file['name'];
                $thumb_path = WWW_ROOT.'uploads/profile_image/resize/'.$file['name'];    
                move_uploaded_file($file['tmp_name'], $original_path);
                $this->ImageResize->smart_resize_image($original_path, 263,124, true, $thumb_path, false, false);
                        
        } */

        //$this->request->data['profile_image']=$upload_file_name;
        $this->request->data['user_type']=3;
        $this->request->data['created_by']=$this->request->session()->read('sp_admin.id');
        $this->request->data['created']=date('Y-m-d H:i:s'); 
        $user = $this->Users->patchEntity($user,$this->request->data);
            
        if($this->Users->save($user)){
             
            $this->Flash->success('New venue staff added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
            return $this->redirect('/admin/venue-staff-list');
        } else {
             
            $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
        }               
        
          } 

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        TableRegistry::clear(); // unload all the models
    }
    //=================== ::: Edit Staff ::: ===========
    public function venuestaffEdit($id = null){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 

        $data['heading']="Edit Venue Staff";
        $data['left_sidebar_parent']="venue-staff";
        $data['left_sidebar_sub']="stafflist";
        $meta_data['meta_title']="Edit-Venue-Staff | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');    
       
        $user = $this->Users->get($id);
        if($this->request->is(['patch', 'post', 'put'])) {
            
            
       /* if(isset($this->request->data['profile_image']['name']) && trim($this->request->data['profile_image']['name'])!=""){
             
                $file = $this->request->data['profile_image'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                if(in_array($ext,$arr_ext)){
                    $file['name']=time().'.'.$ext;
                    $upload_file_name=$file['name'];
                    $this->request->data['profile_image']=$upload_file_name;
                  
                    move_uploaded_file($file['tmp_name'], WWW_ROOT .'uploads/profile_image/'. $file['name']);

                    //==============image upload========
                        $this->loadComponent('ImageResize');
                        $original_path = WWW_ROOT.'uploads/profile_image/'.$file['name'];
                        $thumb_path = WWW_ROOT.'uploads/profile_image/resize/'.$file['name']; 
                        move_uploaded_file($file['tmp_name'], $original_path);        
                          
                        $this->ImageResize->smart_resize_image($original_path, 263,124, true, $thumb_path, false, false);                  
             
                }else{                                                                  
                         $error_counter++;
                         $data_error['profile_image']="Upload image in jpg,jpeg,gif or png";
                     }
             }
             else
             {

                 $this->request->data['profile_image']=$user->profile_image;
             } 
             */  
           
            
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $user = $this->Users->patchEntity($user,$this->request->data);
            if($this->Users->save($user)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/venue-staff-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
            
        
          }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        TableRegistry::clear(); // unload all the models
    }

     //======  Function for change view venue staff ==========
     public function venuestaffView($id = null){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }  
        
        $this->viewBuilder()->layout('ajax');  

        $connection = ConnectionManager::get('default');
        $venueinfo = $connection->execute('SELECT * FROM venue_to_staff_tagging where staff_id="'.$id.'"')->fetchAll('assoc'); 

       //  echo "<pre>"; print_r($venueinfo);exit;
        if(count($venueinfo)>0)
        {
           // echo $venueinfo->venue_id;exit;
        $Venuetable=TableRegistry::get('venues');        
        $venuedata=$Venuetable->find('all',['conditions'=>['id'=>$venueinfo[0]['venue_id']]])->first(); 
        $venue_name=   $venuedata->venue_name;
        }
        else
        {
        $venue_name="";  
        }
        $this->set(compact('venue_name'));
        $this->set('_serialize', ['venue_name']);
       
        $row=$this->Users->find('all',[
            'conditions'=>['id'=>$id],
            ])->first();  
        //echo "<pre>";print_r($row);exit;     
        $this->set(compact('row'));
        $this->set('_serialize', ['row']);
      
       
    }

     public function venuestaffTag($id = null){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }        
        
        $data['heading']="Assign Staff With Venue";             
        $meta_data['meta_title']="Assign-Staff-With-Venue | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');

        // ------------  venue list -------------

        $Venuetable=TableRegistry::get('venues');       
        $venuelist=$Venuetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['venue_order' => 'ASC','venue_name' => 'ASC']);        
        $venuedata=[];
        if($venuelist){
            foreach($venuelist as $data){
                $venuedata[$data->id]=$data->venue_name;
            }
        }
        $this->set(compact('venuedata'));
        $this->set('_serialize', ['venuedata']);

        // ------------  Get staff info --------------
        $staffinfo=$this->Users->find('all',["conditions"=>["id"=>$id]])->first();      

        // ------------- ----------------------------------

        $Taggingtable=TableRegistry::get('venue_to_staff_tag');               
        if($this->request->is('post'))
        {    
        // ------------  Get venue info --------------
        $venueinfo=$Venuetable->find('all',['conditions'=>['id'=>$this->request->data['venue_id']]])->first();
        //echo "<pre>";print_r($venueinfo);exit;

        // ------------  First delete if tagging already exist then insert --------------
          
        $conmng = ConnectionManager::get('default');
        $conmng->execute("DELETE FROM venue_to_staff_tagging WHERE staff_id=$id "); 
       
        $insert=$conmng->insert('venue_to_staff_tagging',['venue_id' => $this->request->data['venue_id'],
                                                          'staff_id' => $id,
                                                          'created'  => date('Y-m-d H:i:s')
                                                     ]);

        // ------------  Email send to staff --------------
        $email = new Email('default');
        $email->from([SITE_RECV_EMAIL => 'Ceroc Attendance'])
        ->template('taggingmail', 'default')
        ->emailFormat('html')
        ->viewVars(['staff_name'=>$staffinfo->firstname.' '.$staffinfo->lastname,'venue_name'=>$venueinfo->venue_name,'venue_username'=>$venueinfo->username,'venue_password'=>$venueinfo->txt_password])
        ->to($staffinfo->email)
        ->subject('Venue login details of ceroc attendance')
       ->send(); 

        if($insert)
        {
        $this->Flash->success('Assign staff with venue added successfully and email has been sent to staff including username and password.',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
            return $this->redirect('/admin/venue-staff-list');
        } else {
             
            $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
        }               
        
        } 

        //$this->set(compact('user'));
        //$this->set('_serialize', ['user']);
        TableRegistry::clear(); // unload all the models
    }

    //=============::: Delete venue staff  ::: =======================
    public function venuestaffdelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $user = $this->Users->get($this->request->data('id'));
        if($user)
        {
            $this->request->data['status']='DELETED';
            $user = $this->Users->patchEntity($user, $this->request->data);
            if($this->Users->save($user)) {
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }
    
    
   ###################################### Customer  ######################################

     public function customerList(){
       
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 

        $data['heading']="Venue Staff";
        $data['left_sidebar_parent']="venue-staff";
        $data['left_sidebar_sub']="stafflist";
        $meta_data['meta_title']="Venue-Staff-List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $rows=$this->Users->find('all',["conditions"=>["id !="=>$this->request->session()->read('sp_admin.id'),'status !='=>'DELETED']])->order(['user_order' => 'ASC']);

         /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
           $connection = ConnectionManager::get('default');
           $order= $this->request->data['user_order'];
           //print_r($order);exit;
           foreach($order as $key=> $val)
           {
            if($val!='')
            {
            $query = $connection->execute("UPDATE users SET  user_order='".$val."' WHERE id='".$key."'");
            }
           }
        $this->Flash->success('Venue staff order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/venue-staff-list');
        }




        $this->set(compact('rows'));
        $this->set('_serialize', ['rows']);
        TableRegistry::clear(); 
    }
    //==========:::: Add customer :::=====================
    public function customerAdd(){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }        
        
        $data['heading']="Add Venue Staff";
        $data['left_sidebar_parent']="venue-staff";
        $data['left_sidebar_sub']="addstaff";
        $meta_data['meta_title']="Add-Venue-Staff | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');
        $user = $this->Users->newEntity();
        
        if($this->request->is('post'))
        {
            
          /* if(isset($this->request->data['profile_image']['name']) && trim($this->request->data['profile_image']['name'])!=""){
             
                $file = $this->request->data['profile_image'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                    if(in_array($ext,$arr_ext)){
                        $file['name']=time().'.'.$ext;
                        $upload_file_name=$file['name'];                  
                    move_uploaded_file($file['tmp_name'], WWW_ROOT .'uploads/profile_image/'. $file['name']);
                    }else{                                                                  
                        $error_counter++;
                        $data_error['service_logo']="Upload image in jpg,jpeg,gif or png";
                 
                    }
                    //==============image upload========
                $this->loadComponent('ImageResize');
                $original_path = WWW_ROOT.'uploads/profile_image/'.$file['name'];
                $thumb_path = WWW_ROOT.'uploads/profile_image/resize/'.$file['name'];    
                move_uploaded_file($file['tmp_name'], $original_path);
                $this->ImageResize->smart_resize_image($original_path, 263,124, true, $thumb_path, false, false);
                        
        } */

        //$this->request->data['profile_image']=$upload_file_name;
        $this->request->data['user_type']=3;
        $this->request->data['created_by']=$this->request->session()->read('sp_admin.id');
        $this->request->data['created']=date('Y-m-d H:i:s'); 
        $user = $this->Users->patchEntity($user,$this->request->data);
            
        if($this->Users->save($user)){
             
            $this->Flash->success('New venue staff added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
            return $this->redirect('/admin/venue-staff-list');
        } else {
             
            $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
        }               
        
          } 

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        TableRegistry::clear(); // unload all the models
    }
    //=================== ::: Edit customer::: ===========
    public function customerEdit($id = null){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 

        $data['heading']="Edit Venue Staff";
        $data['left_sidebar_parent']="venue-staff";
        $data['left_sidebar_sub']="stafflist";
        $meta_data['meta_title']="Edit-Venue-Staff | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');    
       
        $user = $this->Users->get($id);
        if($this->request->is(['patch', 'post', 'put'])) {
            
            
       /* if(isset($this->request->data['profile_image']['name']) && trim($this->request->data['profile_image']['name'])!=""){
             
                $file = $this->request->data['profile_image'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                if(in_array($ext,$arr_ext)){
                    $file['name']=time().'.'.$ext;
                    $upload_file_name=$file['name'];
                    $this->request->data['profile_image']=$upload_file_name;
                  
                    move_uploaded_file($file['tmp_name'], WWW_ROOT .'uploads/profile_image/'. $file['name']);

                    //==============image upload========
                        $this->loadComponent('ImageResize');
                        $original_path = WWW_ROOT.'uploads/profile_image/'.$file['name'];
                        $thumb_path = WWW_ROOT.'uploads/profile_image/resize/'.$file['name']; 
                        move_uploaded_file($file['tmp_name'], $original_path);        
                          
                        $this->ImageResize->smart_resize_image($original_path, 263,124, true, $thumb_path, false, false);                  
             
                }else{                                                                  
                         $error_counter++;
                         $data_error['profile_image']="Upload image in jpg,jpeg,gif or png";
                     }
             }
             else
             {

                 $this->request->data['profile_image']=$user->profile_image;
             } 
             */  
           
            
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $user = $this->Users->patchEntity($user,$this->request->data);
            if($this->Users->save($user)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/venue-staff-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
            
        
          }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        TableRegistry::clear(); // unload all the models
    }

     //======  Function view customer ==========
     public function customerView($id = null){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }  
        
        $this->viewBuilder()->layout('ajax');  

        $connection = ConnectionManager::get('default');
        $venueinfo = $connection->execute('SELECT * FROM venue_to_staff_tagging where staff_id="'.$id.'"')->fetchAll('assoc'); 

       //  echo "<pre>"; print_r($venueinfo);exit;
        if(count($venueinfo)>0)
        {
           // echo $venueinfo->venue_id;exit;
        $Venuetable=TableRegistry::get('venues');        
        $venuedata=$Venuetable->find('all',['conditions'=>['id'=>$venueinfo[0]['venue_id']]])->first(); 
        $venue_name=   $venuedata->venue_name;
        }
        else
        {
        $venue_name="";  
        }
        $this->set(compact('venue_name'));
        $this->set('_serialize', ['venue_name']);
       
        $row=$this->Users->find('all',[
            'conditions'=>['id'=>$id],
            ])->first();  
        //echo "<pre>";print_r($row);exit;     
        $this->set(compact('row'));
        $this->set('_serialize', ['row']);
      
       
    }


}
?>