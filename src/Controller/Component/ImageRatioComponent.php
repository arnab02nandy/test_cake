<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
class ImageRatioComponent extends Component
{

  
//$this->ImageResize->smart_resize_image($original_path, 263,124, true, $thumb_path, false, false);
  public function smart_resize_image($file,
                              $width              = 0, 
                              $height             = 0, 
                              $proportional       = false, 
                              $output             = 'file', 
                              $delete_original    = true, 
                              $use_linux_commands = false ) {
      $info                         = getimagesize($file);
    if ( $height <= 0 && $width <= 0 ) return false;
    $image='';

          /*============== This is a crop image in a ratio ===============*/
              list($originalWidth, $originalHeight) = getimagesize($file);
              $ratio = $originalWidth / $originalHeight;
              
              //Then, this algorithm fits the image into the target size as best it can, keeping the original aspect ratio, not stretching the image larger than the original:

              $targetWidth = $targetHeight = min($size, max($originalWidth, $originalHeight));

                if ($ratio < 1) {
                    $targetWidth = $targetHeight * $ratio;
                } else {
                    $targetHeight = $targetWidth / $ratio;
                }

                $srcWidth = $originalWidth;
                $srcHeight = $originalHeight;
                $srcX = $srcY = 0;

                //This crops the image to fill the target size completely, not stretching it:

                $targetWidth = $targetHeight = min($originalWidth, $originalHeight, $size);

                  if ($ratio < 1) {
                      $srcX = 0;
                      $srcY = ($originalHeight / 2) - ($originalWidth / 2);
                      $srcWidth = $srcHeight = $originalWidth;
                  } else {
                      $srcY = 0;
                      $srcX = ($originalWidth / 2) - ($originalHeight / 2);
                      $srcWidth = $srcHeight = $originalHeight;
                  }

                    //And this does the actual resizing:

                  $targetImage = imagecreatetruecolor($targetWidth, $targetHeight);
                  imagecopyresampled($targetImage, $originalImage, 0, 0, $srcX, $srcY, $targetWidth, $targetHeight, $srcWidth, $srcHeight);
          /*============== End crop ratio image ==================*/
/*
          # Loading image to memory according to type
    switch ( $info[2] ) {
      case IMAGETYPE_GIF:   $image = imagecreatefromgif($file);   break;
      case IMAGETYPE_JPEG:  $image = imagecreatefromjpeg($file);  break;
      case IMAGETYPE_PNG:   $image = imagecreatefrompng($file);   break;
      default: return false;
    }
     $image_resized = imagecreatetruecolor( $targetWidth, $targetHeight );
     if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
      $transparency = imagecolortransparent($image);

      if ($transparency >= 0) {
        $trnprt_color="";
        $transparent_color  = imagecolorsforindex($image, $trnprt_indx);
        $transparency       = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
        imagefill($image_resized, 0, 0, $transparency);
        imagecolortransparent($image_resized, $transparency);
      }
      elseif ($info[2] == IMAGETYPE_PNG) {
        imagealphablending($image_resized, false);
        $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
        imagefill($image_resized, 0, 0, $color);
        imagesavealpha($image_resized, true);
      }
    }*/





    return true;
  }
  
}
?>