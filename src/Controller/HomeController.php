<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
class HomeController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
       
    }
    public function index(){
        $meta_data['meta_title']="Home | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;       
        $this->set(compact('meta_data')); 
    }


    //======  Venue Login ==========
     public function login(){          
       
        //======is venue login==========
        /* if($this->isVenueLogedIn()===true){
        return $this->redirect('/venue/dashboard');                
        }*/   
        $session = $this->request->session();            
        $data['heading']="Venue-Login";
        $meta_data['meta_title']="Venue-Login | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $Venuetable=TableRegistry::get('venues'); 

        if ($this->request->is('post'))
        {                
        $password=md5($this->request->data["password"]);             
        $venuelogin=$Venuetable->find('all',['conditions'=>['status'=>'ACTIVE','username'=>$this->request->data["username"] ,'password'=>$password]])->toArray();
        $count=count($venuelogin);    
           if($count>0) 
           {            
               $venuelogin=$venuelogin[0];

               //----------------- checking if venue tagged with staff -------------------
               $Table=TableRegistry::get('venue_to_staff_tag');
               $vanuetagcheck=$Table->find('all',['conditions'=>['venue_id'=>$venuelogin->id]])->toarray();
               $tagcount=count($vanuetagcheck);
               if($tagcount>0) 
               {               
               $session->write(['venue.id'=>$venuelogin->id,'venue.venue_name'=>$venuelogin->venue_name,'venue.venue_refer_id'=>$venuelogin->venue_refer_id,'venue.status'=>$venuelogin->status,'venue.login'=>1]);  
                  return $this->redirect('/venue/dashboard');  
               } 
               else
               {               
                $this->Flash->error(__('Staff not tagged, try again'));
               }

           } // end $count 
         
           $this->Flash->error(__('Invalid username or password, try again'));
             
        }
    }

   
	
	
	
	
	 
	

	

	
	



	
	
	
	

}
?>