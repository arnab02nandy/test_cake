<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

class StaffsController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);              
    }

    //======  Venue Login ==========
     public function login(){         
       
        //======is venue login==========
         if($this->isVenueLogedIn()===true){
        return $this->redirect('/staff/dashboard');                
        }   
        $session = $this->request->session();            
        $data['heading']="Venue-Login";
        $meta_data['meta_title']="Venue-Login | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('staff_login');
        //---------------  Event type dropdown ------------------//
        $Eventtypetable=TableRegistry::get('eventtypes');       
        $eventtypelist=$Eventtypetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['event_type_order' => 'ASC','event_type_name' => 'ASC'])->toArray();        
        $eventdata=[];

        if($eventtypelist){            
            foreach($eventtypelist as $data){
                $eventdata[$data->id]=$data->event_type_name;
            }
        }
        
        $this->set(compact('eventdata'));
        $this->set('_serialize', ['eventdata']);
        $Venuelogintable=TableRegistry::get('venue_users');

        if ($this->request->is('post'))
        {  
                        
        $password=md5($this->request->data["password"]);             
        $venuelogin=$Venuelogintable->find('all',['conditions'=>['status'=>'ACTIVE','username'=>$this->request->data["username"] ,'password'=>$password]])->toArray();
        $count=count($venuelogin);    
           if($count>0) 
           { 
            $venuelogin=$venuelogin[0];
            //----------------- fetch venue data -------------------
            $Venuetable=TableRegistry::get('venues');
            $venuedata=$Venuetable->find('all',['conditions'=>['id'=>$venuelogin->venue_id]])->toarray();
            //echo "<pre>";print_r($venuedata);exit;
            $venuedata=$venuedata[0];
            //----------------- store session data -------------------       
            $session->write(['venue.id'=>$venuedata->id,'venue.venue_name'=>$venuedata->venue_name,'venue.venue_refer_id'=>$venuedata->venue_refer_id,'venue.status'=>$venuedata->status,'venue.event_type_id'=>$this->request->data["event_type_id"],'venue.login'=>1]);  
            return $this->redirect('/staff/dashboard');
           } // end $count 
         
        $this->Flash->error(__('Invalid username or password, try again'));
             
        }
    }

    //======  Function for venue dashboard ==========

    public function venuedashboard()
    {
     
       //======is venue login==========
        if($this->isVenueLogedIn()===false){
        return $this->redirect('/staff/login');                
        }
        $data['heading']="Venue-Login";
        $meta_data['meta_title']="Venue dashboard | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $venue_id=$this->request->session()->read('venue.id');       
        $this->viewBuilder()->layout('staff');

          // $Table=TableRegistry::get('venuemessages');
        // $rows=$Table->find('all',[   
        //  "conditions"=>['venue_id'=>1,'display_start_date >='=>$current_date,'status'=>'ACTIVE']             
        //     ])->order(['id'=>'DESC'])->toArray();

        $connection = ConnectionManager::get('default');
       // $rows = $connection->execute('SELECT * FROM venuemessages where (venue_id="'.$venue_id.'" or venue_id=99999) and (display_start_date >="'.date("Y-m-d").'" and display_end_date <="'.date("Y-m-d").'" )')-->fetchAll('assoc');
        $rows = $connection->execute('SELECT * FROM venuemessages where (venue_id="'.$venue_id.'" or venue_id=99999) and status ="ACTIVE" order by id desc')->fetchAll('assoc');
         $todaysDate='2017-05-10';
        // $rows = $connection->execute("SELECT * FROM venuemessages where display_start_date <= '.$todaysDate.' AND display_end_date >= '.$todaysDate.' ")->fetchAll('assoc');

        


        // echo "<pre>";print_r($rows);exit;
        $this->set(compact('rows'));
        $this->set('_serialize', ['rows']);

        

    }

    public function venueMessage()
    {
      //======is venue login==========
        if($this->isVenueLogedIn()===false){
        return $this->redirect('/staff/login');                
         }
       
        $current_date=date('Y-m-d');
      
        $meta_data['meta_title']="Message list | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->viewBuilder()->layout('staff');

        $venue_id=$this->request->session()->read('venue.id');
        

        // $Table=TableRegistry::get('venuemessages');
        // $rows=$Table->find('all',[   
        //  "conditions"=>['venue_id'=>1,'display_start_date >='=>$current_date,'status'=>'ACTIVE']             
        //     ])->order(['id'=>'DESC'])->toArray();

        $connection = ConnectionManager::get('default');
       // $rows = $connection->execute('SELECT * FROM venuemessages where (venue_id="'.$venue_id.'" or venue_id=99999) and (display_start_date >="'.date("Y-m-d").'" and display_end_date <="'.date("Y-m-d").'" )')-->fetchAll('assoc');
         $rows = $connection->execute('SELECT * FROM venuemessages where venue_id="'.$venue_id.'" and status ="ACTIVE" order by id desc')->fetchAll('assoc');


       // echo "<pre>";print_r($rows);exit;
        $this->set(compact('rows'));
        $this->set('_serialize', ['rows']);
        

    }

    public function viewMessage($id=null)
    {
        //======is venue login==========
        // if($this->isVenueLogedIn()===false){
        // return $this->redirect('/venue/login');                
        // }

        $meta_data['meta_title']="View message | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->viewBuilder()->layout('ajax');
        $Table=TableRegistry::get('venuemessages');        
        $row=$Table->find('all',[   
         "conditions"=>['id'=>$id]            
            ])->toArray();      
       // echo '<pre>';print_r($row);exit;  

        $this->set(compact('row'));
         $this->set('_serialize', ['row']);
    }

    public function sendMessage()
    {
        //======is venue login==========
       /* if($this->isVenueLogedIn()===false){
        return $this->redirect('/venue/login');                
        } */
        $this->request->allowMethod(['ajax']);   

         /* if($this->request->is('post')){

            echo 'success';exit;


              if($this->request->data['message']!="")
                 {
               $email = new Email('default');
                 $email->from([$this->request->data['email'] => $this->request->data['name']])
                  ->template('servicemail', 'default')
                  ->emailFormat('html')
                  ->viewVars(['to_name'=>$this->request->data['to_name'],'name'=>$this->request->data['name'],'email'=>$this->request->data['email'],'contact_no'=>$this->request->data['contact_no'],'message'=>$this->request->data['message']])
                  ->to($this->request->data['to_email'])
                  ->subject('')
                  ->send();   

            echo 'success';die; 
                 }
       
            echo 'failed';die; 



         }    */

    }

    public function sendMail()
    {

       if($this->request->data['message']!="")
                 {
                 $email = new Email('default');
                 $email->from(["noreply@ceroc.com.au" => "Ceroc Attendance"])
                  ->template('venuemessage', 'default')
                  ->emailFormat('html')
                  ->viewVars(['venue_name'=>$this->request->data['venue_name'],'message'=>$this->request->data['message']])
                  ->to("info@ceroc.com.au")
                   // ->to("test.developers2016@gmail.com")
                  ->subject('Venue Message')
                  ->send();    

            echo 'success';die; 
                 }
       
            echo 'failed';die; 
              

    }

    public function customerAdd()
    {
       /* if($this->isVenueLogedIn()===false){
        return $this->redirect('/venue/login');                
        } */
        $venue_id=$this->request->session()->read('venue.id');
        $venue_name=$this->request->session()->read('venue.name');

        $meta_data['meta_title']="Message list | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->viewBuilder()->layout('staff');

        $Statetable=TableRegistry::get('States');       
        $statelist=$Statetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['state_order' => 'ASC','state_name' => 'ASC']);        
        $statedata=[];
        if($statelist){
            foreach($statelist as $data){
                $statedata[$data->id]=$data->state_name;
            }
        }
        $this->set(compact('statedata'));
        $this->set('_serialize', ['statedata']);

        //------------  dropdown venue list -------------

        $Venuetable=TableRegistry::get('venues');       
        $venuelist=$Venuetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['venue_order' => 'ASC','venue_name' => 'ASC'])->toArray();        
        $venuedata=[];

        if($venuelist){
            
            foreach($venuelist as $data){
                $venuedata[$data->id]=$data->venue_name;
            }
        }
        $this->set(compact('venuedata'));
        $this->set('_serialize', ['venuedata']);



        $Customertable=TableRegistry::get('Customertypes');       
        $customerlist=$Customertable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['customer_type_order' => 'ASC','customer_type_name' => 'ASC']);        
        $customerdata=[];
        if($customerlist){
            foreach($customerlist as $data){
                $customerdata[$data->id]=$data->customer_type_name;
            }
        }
        $this->set(compact('customerdata'));
        $this->set('_serialize', ['customerdata']);

        $Usertable=TableRegistry::get('Users');
        $userdata =$Usertable->newEntity();
        if($this->request->is('post')){
         // echo 123;exit;

            $membership_no=md5(time());
            $membership_no=substr($membership_no,0,10);

            $password=rand(9999,9999999999);
            $password=substr(md5($password),0,6);

           // $username=rand(1000,99999999);
            $username=$this->request->data['firstname'].substr(md5(time()),3,3);

            $this->request->data['user_type']='4';
            $this->request->data['membership_no']=$membership_no;
            $this->request->data['username']=$username;
            $this->request->data['password']=md5($password);
            $this->request->data['created']=date('Y-m-d H:i:s');
            $userdata = $Usertable->patchEntity($userdata,$this->request->data);
            if($Usertable->save($userdata)){

                //--------------- For email -------------------
                 $email = new Email('default');
                 $email->from(["info@ceroc.com.au" => "Ceroc Attendance"])
                  ->template('customerregmail', 'default')
                  ->emailFormat('html')
                  ->viewVars(['name'=>$this->request->data['firstname'].' '.$this->request->data['lastname'],'email'=>$this->request->data['email'],'username'=>$username,'password'=>$password])
                  ->to($this->request->data['email'])
                  ->subject('New registration in ceroc attendance')
                  ->send();

                $this->Flash->success('New customer has been added successfully.',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                //return $this->redirect('/admin/');
            } else {
                $this->Flash->error('Please try again later.',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('userdata'));
        $this->set('_serialize', ['userdata']);

    }

     //========::: is Email exist :::============================
    public function emailexist(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        $Table=TableRegistry::get('users');
        if($this->request->data('id') && $this->request->data('email')){
            $counter=$Table->find('all',['conditions' =>['email'=>trim($this->request->data('email')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('email')){
            $counter=$Table->find('all',['conditions' =>['email'=>trim($this->request->data('email'))]])->count('id');
        }        
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Email-id exist, please enter other email-id";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }

    public function logout(){
        $session = $this->request->session();       
        $session->delete('venue.login');
        $session->delete('venue.id');
        $session->delete('venue.venue_name');
        return $this->redirect('/staff/login');     
    }

   

   
   
    
    //======  Function for add venue ==========
  

    //======  Function for edit venue =========
   
    
    //======  Function for change status of venue ==========
   
   
    
    //======  Function for delete venue ==========
   

    //======  Function for venue exist ==========
  


     //======  Function for venue username exist ==========
  

     //======  Function for ajax get city fro state ==========
   
    
}
?>