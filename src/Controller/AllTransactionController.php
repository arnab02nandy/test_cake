<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
class AllTransactionController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow(['allPaymentList']);
        //=======::: access permissition of login user :::=======
        if($this->request->session()->read('Auth.User.ADMIN.role_type')==='TECH'){
            return $this->redirect($this->Auth->redirectUrl());
        }
    }
    //================:::: all Payment List :::============
    public function allPaymentList($level_id=null){
        //======is admin login==========
        if($this->isAdminLogedIn()===false){
            return $this->redirect('/admin');
        }
        $data['heading']="All Transaction";
        $data['left_sidebar_parent']="AllTransaction";
        $data['left_sidebar_sub']="allPaymentList";
        $meta_data['meta_title']="All Transaction List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $AllPaymentLogs=TableRegistry::get('AllPaymentLogs'); // Load AllPaymentLogs model
        $search_date=date('Y-m-d');
        if($this->request->data('search_date')){
            $search_date=$this->request->data('search_date');
        }
        $all_transaction_data=$AllPaymentLogs->find('all',['conditions'=>['created_date LIKE'=>'%'.$search_date.'%']])->order(['created_date' => 'DESC'])->toArray();

        TableRegistry::clear(); // unload all the models
        $all_payments=['total_billable_item_amount'=>0,'total_student_payment_amount'=>0,'billable_item_payments'=>[],'student_payments'=>[]];
        if(!empty($all_transaction_data)){
            foreach($all_transaction_data as $val){
                if($val['reference_type']=='BLI'){
                    $all_payments['billable_item_payments'][]=$val;
                    $all_payments['total_billable_item_amount']=$all_payments['total_billable_item_amount']+$val['amount'];
                }
                if($val['reference_type']=='SPA'){
                    $all_payments['student_payments'][]=$val;
                    $all_payments['total_student_payment_amount']=$all_payments['total_student_payment_amount']+$val['amount'];
                } 
            }
        }
        $this->set(compact('all_payments'));
    }
}
?>