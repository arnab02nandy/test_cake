<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class SuburbsController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
       
    }
    
    //======  Function for listing suburbs ==========
    public function suburbList(){
        //--------- is admin login ------------
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }      
       
        $this->paginate = array(
            'limit' => 50
        );

        $data['heading']="Suburb";
        $data['left_sidebar_parent']="suburb-list";       
        $meta_data['meta_title']="Suburb-List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));	
        $this->viewBuilder()->layout('admin');
        //$cities=$this->Cities->find('all',["contain"=>["States"]])->order(['suburb_order' => 'ASC','suburb_name' => 'ASC']);  

        $Suburbtable=TableRegistry::get('suburbs');
       
        $Suburbtable->hasOne('States',[
            'className' => 'States',
            'foreignKey' => false,
            'conditions' =>["States.id=suburbs.state_id"]
        ]);
  
        $suburbs=$Suburbtable->find('all',[         
            "contain"=>["States"
                ]
            ])->order(['suburb_order' => 'ASC','suburb_name' => 'ASC'])->toArray();  

        /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
           $connection = ConnectionManager::get('default');
           $order= $this->request->data['suburb_order'];
           //print_r($order);exit;
           foreach($order as $key=> $val)
           {
            if($val!='')
            {
            $query = $connection->execute("UPDATE suburbs SET  suburb_order='".$val."' WHERE id='".$key."'");
            }
           }
        $this->Flash->success('Suburb order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/suburb-list');
        }


        $this->set(compact('suburbs'));
        $this->set('_serialize', ['suburbs']);
    }
  
    //======  Function for add suburb ==========
    public function suburbAdd(){
       
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 
		
		$data['heading']="Add Suburb";
        $data['left_sidebar_parent']="SuburbList";        
        $meta_data['meta_title']="Add-Suburb | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
		
        $Statetable=TableRegistry::get('States'); 		
		$statelist=$Statetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['state_order' => 'ASC','state_name' => 'ASC']);		
		$statedata=[];
        if($statelist){
            foreach($statelist as $data){
                $statedata[$data->id]=$data->state_name;
            }
        }
		$this->set(compact('statedata'));
        $this->set('_serialize', ['statedata']);  

        $this->viewBuilder()->layout('ajax');
        $suburb = $this->Suburbs->newEntity();
        if($this->request->is('post')){
            $this->request->data['created']=date('Y-m-d H:i:s');
            $suburb = $this->Suburbs->patchEntity($suburb,$this->request->data);
            if($this->Suburbs->save($suburb)){
                $this->Flash->success('New suburb has been added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/suburb-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('suburb'));
        $this->set('_serialize', ['suburb']);
    }
   
    //======  Function for edit suburb ==========
    public function suburbEdit($id = null){
       
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 
		
		$data['heading']="Edit Suburb";
        $data['left_sidebar_parent']="SuburbList";        
        $meta_data['meta_title']="Edit-Suburb | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));

		$Statetable=TableRegistry::get('States');		
		$statelist=$Statetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['state_order' => 'ASC','state_name' => 'ASC']);		
		$statedata=[];
        if($statelist){
            foreach($statelist as $data){
                $statedata[$data->id]=$data->state_name;
            }
        }
		$this->set(compact('statedata'));
        $this->set('_serialize', ['statedata']);        
        $this->viewBuilder()->layout('ajax');
        $suburb = $this->Suburbs->get($id);
        if(!isset($suburb->id) || trim($suburb->id)<=0){
            $this->Flash->error('Invalid request',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            return $this->redirect('/admin/suburb-list');
        }
        if($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $suburb = $this->Suburbs->patchEntity($suburb,$this->request->data);
            if($this->Suburbs->save($suburb)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/suburb-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('suburb'));
        $this->set('_serialize', ['suburb']);
    }
    

    //======  Function for change status of suburb ==========
    public function changeStatus($id = null){
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $suburb = $this->Suburbs->get($this->request->data('id'));
        if($suburb){
            $change_status=trim($suburb->status)=='ACTIVE' ? "INACTIVE" : "ACTIVE";
            $suburb->status=$change_status;
            if($this->Suburbs->save($suburb)){
                $status='SUCCESS';
                $msg="Record status has been changed successfully.";
            }else{
                $change_status=trim($suburb->status)=='INACTIVE' ? "ACTIVE" : "INACTIVE";
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg,"change_status"=>$change_status));
        exit;
    }
    
    //======  Function for delete suburb ==========
    public function suburbDelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $suburb = $this->Suburbs->get($this->request->data('id'));
        if($suburb){           
            $result = $this->Suburbs->delete($suburb);	          
			if($result){				
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }
   
    //======  Function for check suburb exist ==========
    public function suburbExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('suburb_name')){
            $counter=$this->Suburbs->find('all',['conditions' =>['suburb_name'=>trim($this->request->data('suburb_name')),'state_id'=>trim($this->request->data('state_id')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('suburb_name')){
            $counter=$this->Suburbs->find('all',['conditions' =>['suburb_name'=>trim($this->request->data('suburb_name')),'state_id'=>trim($this->request->data('state_id'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Suburb name is already exist, please enter other.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
}
?>