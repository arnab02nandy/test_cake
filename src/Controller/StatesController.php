<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class StatesController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);      
               
    }

    //======  Function for listing states ==========
    public function stateList(){
        
        //--------- is admin login ------------
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }         

        $data['heading']="State";
        $data['left_sidebar_parent']="state-list";
        $data['left_sidebar_sub']="StateList";
        $meta_data['meta_title']="State-List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $states=$this->States->find('all')->order(['state_order' => 'ASC','state_name' => 'ASC']);

        /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
           $connection = ConnectionManager::get('default');
           $order= $this->request->data['state_order'];
           //print_r($order);exit;
           foreach($order as $key=> $val)
           {
            if($val!='')
            {
            $query = $connection->execute("UPDATE states SET  state_order='".$val."' WHERE id='".$key."'");
            }
           }
        $this->Flash->success('State order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/state-list');
        }


        $this->set(compact('states'));
        $this->set('_serialize', ['states']);
    }
    
    //======  Function for add state ==========
    public function stateAdd(){
        
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }

        $data['heading']="Add State";
        $data['left_sidebar_parent']="StateList";
        $data['left_sidebar_sub']="StateAdd";
        $meta_data['meta_title']="Add-State | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');

        $state = $this->States->newEntity();
        if($this->request->is('post')){
            $this->request->data['created']=date('Y-m-d H:i:s');
            $state = $this->States->patchEntity($state,$this->request->data);
            if($this->States->save($state)){
                $this->Flash->success('New state has been added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/state-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('state'));
        $this->set('_serialize', ['state']);
    }

    //======  Function for edit state =========
    public function stateEdit($id = null){
      
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }

        $data['heading']="Edit State";
        $data['left_sidebar_parent']="StateList";
        $data['left_sidebar_sub']="StateEdit";
        $meta_data['meta_title']="Edit-State | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');
        $state = $this->States->get($id);
        if(!isset($state->id) || trim($state->id)<=0){
            $this->Flash->error('Invalid request',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
           return $this->redirect('/admin/state-list');
        }
        if($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $state = $this->States->patchEntity($state,$this->request->data);
            if($this->States->save($state)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/state-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('state'));
        $this->set('_serialize', ['state']);
    }
    
    //======  Function for change status of state ==========
   
    public function changeStatus($id = null){
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $state = $this->States->get($this->request->data('id'));
        if($state){
            $change_status=trim($state->status)=='ACTIVE' ? "INACTIVE" : "ACTIVE";
            $state->status=$change_status;
            if($this->States->save($state)){
                $status='SUCCESS';
                $msg="Record status has been changed successfully.";
            }else{
                $change_status=trim($state->status)=='INACTIVE' ? "ACTIVE" : "INACTIVE";
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg,"change_status"=>$change_status));
        exit;
    }
    
    //======  Function for delete state ==========
    public function stateDelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $state = $this->States->get($this->request->data('id'));
        if($state){
           // $this->request->data['status']='DELETE';
           // $state = $this->States->patchEntity($state, $this->request->data);		
			
            $result = $this->States->delete($state);				
           // if($this->States->save($state)){
			if($result){
				
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }

    //======  Function for state exist ==========
    public function stateExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('state_name')){
            $counter=$this->States->find('all',['conditions' =>['state_name'=>trim($this->request->data('state_name')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('state_name')){
            $counter=$this->States->find('all',['conditions' =>['state_name'=>trim($this->request->data('state_name'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="State name is already exist, please enter other.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }
}
?>