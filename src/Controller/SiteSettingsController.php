<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
class SiteSettingsController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        
        if($this->request->session()->read('sp_admin.type')!==1){
            return $this->redirect('/admin/login/');
        }
    }
    //=================== ::: siteSettingUpdate   ::: ===========
    public function siteSettingUpdate(){

        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        } 

        $data['heading']="Update Site Settings";
        $data['left_sidebar_parent']="SiteSetting";
        $data['left_sidebar_sub']="SiteSetting";
        $meta_data['meta_title']="Update-Siite-Setting | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');
        $SiteSettings = TableRegistry::get('SiteSettings');  // load  SiteSettings  model
        $SiteSetting=$SiteSettings->find('all')->where(['type'=>'SA'])->first();
        if($this->request->is(['patch', 'post', 'put'])) {
            $data_error=array();
            $error_counter=1;
            $new_site_logo="";
            $old_site_logo=isset($SiteSetting->site_logo) ? ($SiteSetting->site_logo) : "";
            $new_site_icon="";
            $old_site_icon=isset($SiteSetting->site_fab_icon) ? ($SiteSetting->site_fab_icon) : "";
            //Check if image has been uploaded
            if(isset($this->request->data['image']['name']) && trim($this->request->data['image']['name'])!=""){
                $file = $this->request->data['image'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('png'); //set allowed extensions
                if(in_array($ext,$arr_ext)){
                    $file['name']=time().'.'.$ext;
                    $new_site_logo=$file['name'];
                    move_uploaded_file($file['tmp_name'], WWW_ROOT .'images/'. $file['name']);
                    $this->request->data['site_logo'] = $file['name'];
                }else{
                    $error_counter++;
                    $data_error['upload_image']="Upload .png image";
                }
            }
            if(isset($this->request->data['icon']['name']) && trim($this->request->data['icon']['name'])!=""){
                $file = $this->request->data['icon'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('ico'); //set allowed extensions
                if(in_array($ext,$arr_ext)){
                    $file['name']=time().'.'.$ext;
                    $new_site_icon=$file['name'];
                    move_uploaded_file($file['tmp_name'], WWW_ROOT .'images/'. $file['name']);
                    $this->request->data['site_fab_icon'] = $file['name'];
                }else{
                    $error_counter++;
                    $data_error['upload_icon']="Upload .ico image";
                }
            }
            if($error_counter==1){
            	//echo "<pre>";print_r($this->request->data);exit;
                $SiteSetting = $SiteSettings->patchEntity($SiteSetting,$this->request->data);
                if($SiteSettings->save($SiteSetting)){
                  // echo "<pre>";print_r($result=$SiteSettings->save($SiteSetting));exit;

                    if(trim($new_site_logo)!=""){
                        @unlink(WWW_ROOT.'images/'.$old_site_logo);
                    }
                    if(trim($new_site_icon)!=""){
                        @unlink(WWW_ROOT.'images/'.$old_site_icon);
                    }
                   $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]); 
                }else{
                    $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
                }
                return $this->redirect('/admin/site-setting');
            }
        }
        TableRegistry::clear(); // unload all the models
        $this->set(compact('SiteSetting'));
        $this->set('_serialize', ['SiteSetting']);
        
    }
    
}
?>