<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class VenuesController extends AppController{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);      
               
    }

    //======  Function for listing venues ==========
    public function venueList(){
        
        //--------- is admin login ------------
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }         

        $data['heading']="Venue";
        $data['left_sidebar_parent']="venue";
        $data['left_sidebar_sub']="VenueList";
        $meta_data['meta_title']="Venue-List | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('admin');


       // $venues=$this->Venues->find('all');

        $Venuetable=TableRegistry::get('venues');
       
        $Venuetable->hasOne('States',[
            'className' => 'States',
            'foreignKey' => false,
            'conditions' =>["States.id=venues.state_id"]
        ]);
        $Venuetable->hasOne('Suburbs',[
            'className' => 'Suburbs',
            'foreignKey' => false,
            'conditions' =>["Suburbs.id=venues.suburb_id"]
        ]);        

  
         $venues=$Venuetable->find('all',[   
         "conditions"=>['venues.status !='=>"DELETE"] ,            
            "contain"=>[
                            "States","Suburbs"
                ]
            ])->order(['venue_order' => 'ASC','venue_name' => 'ASC'])->toArray();   

         /*----------------  For Ordering --------------------*/
        if($this->request->is('post'))
        {
           $connection = ConnectionManager::get('default');
           $order= $this->request->data['venue_order'];
           //print_r($order);exit;
           foreach($order as $key=> $val)
           {
            if($val!='')
            {
            $query = $connection->execute("UPDATE venues SET  venue_order='".$val."' WHERE id='".$key."'");
            }
           }
         $this->Flash->success('Venue order has been updated successfully ',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
         return $this->redirect('/admin/venue-list');
        }


        $this->set(compact('venues'));
        $this->set('_serialize', ['venues']);
    }
    
    //======  Function for add venue ==========
    public function VenueAdd(){
        
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }

        $Statetable=TableRegistry::get('States');       
        $statelist=$Statetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['state_order' => 'ASC','state_name' => 'ASC']);        
        $statedata=[];
        if($statelist){
            foreach($statelist as $data){
                $statedata[$data->id]=$data->state_name;
            }
        }
        $this->set(compact('statedata'));
        $this->set('_serialize', ['statedata']);

        $data['heading']="Add Venue";       
        $meta_data['meta_title']="Add-Venue | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');

        $venue = $this->Venues->newEntity();      
        if($this->request->is('post')){
            $venue_refer_id=md5(time());
            $venue_refer_id=substr($venue_refer_id,0,10);
            // $txt_password=$this->request->data['password'];

            // $this->request->data['password']=md5($txt_password); 
            // $this->request->data['txt_password']=$txt_password;
            $this->request->data['venue_refer_id']=$venue_refer_id;            
            $this->request->data['created_by']=$this->request->session()->read('sp_admin.id');
            $this->request->data['created']=date('Y-m-d H:i:s');
            $this->request->data['ip_address']=$_SERVER['REMOTE_ADDR'];
            $venue = $this->Venues->patchEntity($venue,$this->request->data);             
            if($this->Venues->save($venue)){
                $this->Flash->success('New venue has been added successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/venue-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('venue'));
        $this->set('_serialize', ['venue']);
    }

    //======  Function for edit venue =========
    public function venueEdit($id = null){
      
        if($this->isSuperAdminLogedIn()===false){
        return $this->redirect('/admin/login');                
        }

        $Statetable=TableRegistry::get('States');       
        $statelist=$Statetable->find('all',['conditions'=>['status'=>'ACTIVE']])->order(['state_order' => 'ASC','state_name' => 'ASC']);        
        $statedata=[];
        if($statelist){
            foreach($statelist as $data){
                $statedata[$data->id]=$data->state_name;
            }
        }
        $this->set(compact('statedata'));
        $this->set('_serialize', ['statedata']);

        $data['heading']="Edit Venue";        
        $meta_data['meta_title']="Edit-Venue | ".SITE_META_TITLE;
        $meta_data['meta_desc']=SITE_META_DESC;
        $this->set(compact('meta_data'));
        $this->set(compact('data'));
        $this->viewBuilder()->layout('ajax');
        $venue = $this->Venues->get($id);

        $Suburbtable=TableRegistry::get('Suburbs');        
        $suburblist=$Suburbtable->find('all',['conditions'=>['state_id'=>$venue->state_id,'status'=>'ACTIVE']])->order(['suburb_order'=>'ASC','suburb_name'=>'ASC']);
        
        $suburbdata=[];
        if($suburblist){
            foreach($suburblist as $data){
                $suburbdata[$data->id]=$data->suburb_name;
            }
        }
        $this->set(compact('suburbdata'));
        $this->set('_serialize', ['suburbdata']); 

        if(!isset($venue->id) || trim($venue->id)<=0){
            $this->Flash->error('Invalid request',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
           return $this->redirect('/admin/venue-list');
        }
       /* $txt_password=$this->request->data['txt_password'];                         
        $this->request->data['password']=md5($txt_password); 
        $this->request->data['txt_password']=$txt_password;*/
        $this->request->data['modified']=date('Y-m-d H:i:s');
        $this->request->data['ip_address']=$_SERVER['REMOTE_ADDR'];
        if($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified']=date('Y-m-d H:i:s');
            $venue = $this->Venues->patchEntity($venue,$this->request->data);
            if($this->Venues->save($venue)) {
                $this->Flash->success('Record has been updated successfully',['key'=>'SUCCESS','params'=>['class' =>'alert-success']]);
                return $this->redirect('/admin/venue-list');
            } else {
                $this->Flash->error('Please try again later',['key'=>'ERROR','params'=>['class' =>'alert-danger']]);
            }
        }
        $this->set(compact('venue'));
        $this->set('_serialize', ['venue']);
    }
    
    //======  Function for change status of venue ==========
   
    public function changeStatus($id = null){
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $venue = $this->Venues->get($this->request->data('id'));
        if($venue){
            $change_status=trim($venue->status)=='ACTIVE' ? "INACTIVE" : "ACTIVE";
            $venue->status=$change_status;
            if($this->Venues->save($venue)){
                $status='SUCCESS';
                $msg="Record status has been changed successfully.";
            }else{
                $change_status=trim($venue->status)=='INACTIVE' ? "ACTIVE" : "INACTIVE";
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg,"change_status"=>$change_status));
        exit;
    }
    
    //======  Function for delete venue ==========
    public function venueDelete($id = null){ 
        $this->autoRender=false;
        $status='ERROR';
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $venue = $this->Venues->get($this->request->data('id'));
        if($venue){           		
           // $result = $this->Venues->delete($venue);

           $users = TableRegistry::get('venues');
            $query = $users->query();
           $result= $query->update()
                ->set(['status' => 'DELETE'])
                ->where(['id' => $this->request->data('id')])
                ->execute();

			if($result){
				
                $status='SUCCESS';
                $msg="Record has been deleted successfully.";
            }else{
                $status='ERROR';
                $msg="Please try again later.";
            }
        }
        echo json_encode(array("status"=>$status,"msg"=>$msg));
        exit;
    }

    //======  Function for venue exist ==========
    public function venueExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('venue_name')){
            $counter=$this->Venues->find('all',['conditions' =>['venue_name'=>trim($this->request->data('venue_name')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('venue_name')){
            $counter=$this->Venues->find('all',['conditions' =>['venue_name'=>trim($this->request->data('venue_name'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Venue name is already exist, please enter other.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }


     //======  Function for venue username exist ==========
    public function usernameExixts(){
        $this->autoRender=false;
        $status=false;
        $msg="Please try again later.";
        $change_status="";
        $this->request->allowMethod(['ajax']);
        $counter=0;
        if($this->request->data('id') && $this->request->data('username')){
            $counter=$this->Venues->find('all',['conditions' =>['username'=>trim($this->request->data('username')),'id !='=>$this->request->data('id')]])->count('id');
        }
        else if($this->request->data('username')){
            $counter=$this->Venues->find('all',['conditions' =>['username'=>trim($this->request->data('username'))]])->count('id');
        }
        if($counter<=0){
            $status=true;
            $msg="success";
        } else {
            $status=false;
            $msg="Venue username is already exist, please enter other.";
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg));
        exit;
    }

     //======  Function for ajax get suburb from state ==========
    public function getSuburb(){
        $this->autoRender=false;
        $status=false;
        $this->request->allowMethod(['ajax']);
        $Suburb=TableRegistry::get('suburbs');        
        $suburblist=array();
        if($this->request->data('state_id')){
            $status=true;
            /*$citylist=$City->find('all',['conditions'=>['state_id'=>$this->request->data('state_id')],
                 'order' => ['city_name' => 'ASC'],
                ])->combine('id','city_name')->toArray();*/

            $suburblist=$Suburb->find('all',['conditions'=>['state_id'=>$this->request->data('state_id'),'status'=>'ACTIVE']])->order(['suburb_order'=>'ASC','suburb_name' => 'ASC']);

        $suburbdata=[];
        if($suburblist){
            foreach($suburblist as $data){
                $suburbdata[$data->id]=$data->suburb_name;
            }
        }


       
        }  
        echo json_encode(array('status'=>$status,'data'=>$suburbdata));
        exit;        
        
    }
    
}
?>