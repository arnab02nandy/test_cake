<div id="wrapper">
      <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']) : "";?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?>
            <?php echo $this->Form->create($SiteSetting,['type' => 'file',"name"=>"editSiteSettingsFrm","id"=>"editSiteSettingsFrm","role"=>"form"]); ?>
                <div class="form-group">
                     <?php echo $this->Form->input('site_name',['label' =>["class"=>"control-label"],'placeholder' => __('site name'),'autofocus',"class"=>"form-control required","id"=>'siteName']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('site_url',['label' =>["class"=>"control-label"],'placeholder' => __('Site Url'),'autofocus',"class"=>"form-control required url","id"=>'siteUrl']);?>
                </div>
                 <div class="form-group">
                <label>Site Email</label>
                  <?php echo $this->Form->input('recv_email',["class"=>"control-label","label"=>false,'placeholder' => __('Recever Email'),'autofocus',"class"=>"form-control required email","id"=>'receverEmail']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('contact_name',['label' =>["class"=>"control-label"],'placeholder' => __('Contact Name'),'autofocus',"class"=>"form-control required","id"=>'contactName']);?>
                </div>
                <div class="form-group">
                    <label for="fname" class="control-label">Contact Phone</label>
                  <?php echo $this->Form->input('contact_phone',['label' =>false,'placeholder' => __('Contact Phone'),'autofocus',"class"=>"form-control required",'id'=>'contactPhone']);?>
                </div>

                <div class="form-group">
                  <?php echo $this->Form->input('site_address',['label' =>["class"=>"control-label"],'placeholder' => __('Site Address'),'autofocus',"class"=>"form-control required","id"=>'siteAddress']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('page_limit',['label' =>["class"=>"control-label"],'placeholder' => __('Page Limit'),'autofocus',"class"=>"form-control required","id"=>'pageLimit','type'=>'text']);?>
                </div>
                <?php /*<div class="form-group">
                  <?php echo $this->Form->input('meta_title',['label' =>["class"=>"control-label"],'placeholder' => __('Meta Title'),'autofocus',"class"=>"form-control required","id"=>'metaTitle']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('meta_desc',['label' =>["class"=>"control-label"],'placeholder' => __('Meta Description'),'autofocus',"class"=>"form-control","id"=>'metaDesc']);?>
                </div>*/?>
                <div class="form-group">
                  <?php //echo $this->Form->input('sending_mail',['label' =>["class"=>"control-label"],'placeholder' => __('Sending Email'),'autofocus',"class"=>"form-control required email","id"=>'sendingEmail']);?>
                </div>
               
                <div class="form-group">
                  <?php //echo $this->Form->input('contact_email',['label' =>["class"=>"control-label"],'placeholder' => __('Contact Email'),'autofocus',"class"=>"form-control required email","id"=>'contactEmail']);?>
                </div>
               
                
                
               
                <div class="form-group">
                  <?php echo  $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]) ?>
                </div>
            <?php echo $this->Form->end(); ?>
          </div>
        </div>
    </div>
  </div>
<script>
$(document).ready(function(){
    $("#editSiteSettingsFrm").validate({
        rules: {
            'page_limit':{'digits':$("#pageLimit").val(),maxlength:100},
            'fax_no':{'digits':$("#faxNo").val()},
            'contact_phone':{'digits':$("#contactPhone").val(),'maxlength':15},
            'site_address':{'maxlength':'255'},
            'meta_desc':{'maxlength':'255'},
            'image':{'accept':'png'},
            'icon':{'accept':'ico'},
        },
        submitHandler:function(form) {
           $('button').prop('disabled',true);
           form.submit();
        }
    });
});
</script>