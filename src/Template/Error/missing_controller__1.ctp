<?php
$this->layout = 'error';
$this->assign('title', $message);
$this->assign('templateName', 'missing_controller.ctp');
$this->start('file');
?>
<h2><?= h($message) ?></h2>
<p class="error">Invalid Request.</p>