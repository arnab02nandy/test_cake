<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Offer Details </h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">

             <div class="table-responsive tfModalTable">           
             <table class="table table-bordered">            
             <tbody>              
              <tr>
                <td><label>Staff Name:&nbsp;</label></td>
                <td><p><?php echo $row->firstname.' '.$row->lastname ?></p></td>
              </tr>
              <tr>
                <td><label>Email:&nbsp;</label></td>
                <td><p><?php echo $row->email ?></p></td>
              </tr>
              <tr>
                <td><label>Contact No:&nbsp;</label></td>
                <td> <p>
               <?php echo $row->phone ?>
                </p></td>
              </tr>

              <tr>
                <td><label>Address:&nbsp;</label></td>
                <td> <p>
                 <?php echo $row->address ?>
                </p></td>
              </tr>
             
              <tr>
                <td> <label>Comment:&nbsp;</label></td>
                <td><p> <?php echo $row->comment ?></p></td>
              </tr>

              <tr>
                <td> <label>Assigned Venue:&nbsp;</label></td>
                <td><p> <?php echo ($venue_name!=""? $venue_name: "No venue assigned till now"); ?></p></td>
              </tr>

             <?php /*  <tr>
                <td> <label>&nbsp;</label></td>
                <td><p> <?php echo $this->Html->image(BASE_URL."/uploads/profile_image/resize/". $row->profile_image, array('alt' =>'Image', 'data-src' => '')); ?></p></td>
              </tr> */?>

            </tbody>
            </table>
            </div>

        </div>
    </div>
</div>
