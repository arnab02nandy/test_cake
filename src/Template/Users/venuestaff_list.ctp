<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']).' List' :""; ?>
                    <div class="header-btn">
                        <a href="javascript:void(0)" alt="Add" title="Add" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/venue-staff-add');?>"><button class="btn btn-success btn-bg-change">Add <?php echo isset($data['heading']) ? trim($data['heading']) :""; ?></button></a>
                    </div>
                </h1>
            </div>
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div id="notifyMessage"><?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?> </div>
                <div class="panel panel-default">
                <div class="panel-body table-responsive">
                <div class="dataTable_wrapper">
                <form action="" name="f1" id="f1" method="post">
                    <table class="table table-bordered" id="dataTablesApnd">
                        <thead>
                            <tr>
                                <!-- <th>Sl No</th>   -->   
                                 <th>Order<input type="submit" class="Update-Order-Btn" name="submit" value="Update Order" ></th>   
                                <th>Name</th>
                                <th>Email</th>                           
                                 
                                 <th>Status</th>                                
                                <th class="actions">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($rows)){
                          $counter=1;
                          foreach ($rows as $val){
                        ?>
                          <tr id="dataRow_<?php echo $val->id;?>">
                           <td align="center"><input type="text" name="user_order<?php echo '['.$val->id.']'?>" id="user_order" class="order_field" value="<?php echo ($val->user_order !='99999' ? $val->user_order : '');?>" ></td>
                                                      
                              <td align="center"><?php echo $val->firstname.' '.$val->lastname; ?></td>
                               <td align="center"><?php echo $val->email; ?></td>
                             <?php /* <td align="center"><input type="text" name="state_order<?php echo '['.$state->id.']'?>" id="state_order" value="<?php echo $state->state_order ?>"></td>*/ ?>
                            
                              <td align="center">
                              <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Users/changeStatus');?>" data-id="<?php echo $val->id;?>", class="ChangeStatus" id="ChangeStatus_<?php echo $val->id;?>" data-status="<?php echo $val->status;?>">
                                  <?php if(trim($val->status)=='ACTIVE'){?>
                                  <i style="color:#439f43" title="Click to Inactive" class="fa fa-check-square fa-lg" aria-hidden="true"></i>
                                  <?php } else {?>
                                  <i style="color:#FF0000" title="Click to Active" class="fa fa-check-square fa-lg" aria-hidden="true"></i>
                                  <?php } ?>
                                  </a>
                              </td>
                              <td align="center" class="actions">
                                <a href="javascript:void(0)" alt="Edit" title="Click to edit" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/venue-staff-edit/'.$val->id);?>"><i class="fa fa-pencil-square-o fa-lg"></i></a>

                                <a href="javascript:void(0)" alt="view" title="Click to view details" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/venue-staff-view/'.$val->id);?>"><i class="fa fa-eye fa-lg"></i></a>
                                
                                <?php /* <a href="javascript:void(0)" alt="Tagging" title="Assign staff with venue" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/venue-staff-tagging/'.$val->id);?>"><i class="fa fa-tags"></i></a>*/?>

                                  <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Users/venuestaffdelete');?>" data-id="<?php echo $val->id;?>", class="deleteDataRow" ><i style="color:#FF0000" title="Click to delete" class="fa fa-times-circle fa-lg" aria-hidden="true"></i></a>
                                  
                                 
                              </td>
                          </tr>
                          <?php
                              $counter++;
                              }
                          }
                          ?>
                        </tbody>
                    </table>
                    </form>
                    
                </div>
                </div>
                </div>
            </div>
       <!-- </div>-->
        <!-- /.Icon Table -->
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Icon Info
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <a><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;Edit&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-eye fa-lg"></i></a>&nbsp;View &nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-times-circle fa-lg" style="color:#FF0000"></i></a>&nbsp;Delete&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-check-square fa-lg" style="color:#439f43"></i></a>&nbsp;Active&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-check-square fa-lg" style="color:#FF0000"></i></a>&nbsp;Inactive
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        <!--</div>-->
        <!-- /.Icon Table -->
    </div>
</div>
<!-- Data Tables -->
<script>
$(document).ready(function() {
    $('#dataTablesApnd').DataTable({
            responsive: true,
            "ordering": false,
            "info":     false,
            "oLanguage":{
            "sEmptyTable":"No Records found.",
            "emptyTable": "No Records found.",
            },
            "language": {
                "emptyTable": "No Records found.",
            },
            "aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
            "iDisplayLength": '<?php echo PAGE_DATA_LIMIT;?>', //Pagination limit
    });
});
</script>


