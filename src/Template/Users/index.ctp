<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12"><h1 class="page-header page_title">Welcome To  Dashboard</h1></div>
       
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                <div class="panel-body" ">
                    <div class="row">
                       
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-fw fa-cogs fa-lg fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php //echo isset($categories) ? $categories : '0'; ?></div>
                                            <div>Site Settings</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo $this->Url->build('/admin/site-setting');?>">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        

                        
                        

                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-fw fa-university fa-lg fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php //echo isset($teacher_count) ? $teacher_count : '0'; ?></div>
                                            <div>States</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo $this->Url->build('/admin/state-list');?>">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                       
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-building fa-lg fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php //echo isset($subject_count) ? $subject_count : '0'; ?></div>
                                            <div>Suburbs</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo $this->Url->build('/admin/suburb-list');?>">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                       


                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-location-arrow fa-lg fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php //echo isset($level_count) ? $level_count : '0'; ?></div>
                                            <div>Event Types</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo $this->Url->build('/admin/eventtype-list');?>">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                       
                        
                        
                     
                         <div class="col-lg-3 col-md-6">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-map-marker fa-lg fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php //echo isset($student_count) ? $student_count : '0'; ?></div>
                                            <div>Customer Types</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo $this->Url->build('/admin/customertype-list');?>">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                              </div>
                        </div>


                       

                       

                        

                       

                       

                       



                        </div>
                    </div>
                </div>
                </div>
            </div>
        <!--</div>-->
        
        <?php //} ?>
        
    </div>
</div>  