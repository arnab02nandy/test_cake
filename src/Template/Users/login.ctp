<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
           
           
           
           <h2 class="login-heading">Administrator Login</h2>
           
           <p class="login-icon">
<?php // echo $this->Html->image(BASE_URL.'/backend/images/ceroc-logo.jpg',array('style'=>'background-color:#FFF;','width'=>'175px','height'=>'140px'))?>


           </p>
           
           
            <div class="">
                <div class="panel-heading">
                <h3 class="panel-title">
                 Login
                </h3>
                </div>
                <div class="">
                    
                    <?php echo $this->Form->create('',["name"=>'loginFrm',"id"=>"loginFrm","role"=>"form"]) ?>
                    <fieldset>
                        <?php echo $this->Flash->render() ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('username',["class"=>" required", "id"=>"userName","placeholder"=>"Username","autofocus"=>true,'autocomplete'=>'off']) ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('password',["class"=>" required", "placeholder"=>"Password","type"=>"password","autofocus"=>true]) ?>
                        </div>
                        <?php echo $this->Form->button(__('Login'),["class"=>""]); ?>
                    </fieldset>
                    <?php echo $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
  	     $("#loginFrm").validate({
            rules:{
               
            },
         });
         
    });
</script>
