<?php  $session=$this->request->session();
       $admin_id=$session->read('sp_admin.id');?>
<div id="wrapper">
      <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']) : "";?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?>
            <?php echo $this->Form->create($user,["name"=>"ChangePasswordFrm","id"=>"ChangePasswordFrm","role"=>"form"]); ?>
                <div class="form-group">
                     <?php echo $this->Form->input('old_password',['label' =>["class"=>"control-label"],'placeholder' => __('Old Password'),'autofocus',"class"=>"form-control required","id"=>'OldPassword','type'=>'password']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('new_password',['label' =>["class"=>"control-label"],'placeholder' => __('New Password'),'autofocus',"class"=>"form-control required minlength[6]","id"=>'NewPassword','type'=>'password']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('confirm_password',['label' =>["class"=>"control-label"],'placeholder' => __('Confirm Password'),'autofocus',"class"=>"form-control required equalTo",'type'=>'password','id'=>'ConfirmPassword']);?>
                </div>
                
                <div class="form-group">
                  <?php echo $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]); ?>
                </div>
            <?php echo $this->Form->end(); ?>
          </div>
        </div>
    </div>
  </div>
<script>
$(document).ready(function(){
    $("#ChangePasswordFrm").validate({
        rules:{
           
            new_password:{notEqualTo: "#OldPassword",minlength: "6"},
            
            confirm_password:{equalTo: "#NewPassword"},
        },
        submitHandler:function(form) {
           $('button').prop('disabled',true);
            $.post('<?php echo $this->Url->build('/Users/isOldpasswordValid')?>',{'old_password':$('#OldPassword').val(),'id':'<?php echo $admin_id;?>'},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#OldPasswordERROR').remove();
                    $('#OldPassword').after('<label for="Code" generated="true" class="error" id="OldPasswordERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                    form.submit();
                }
            });
        }
    });
});
</script>