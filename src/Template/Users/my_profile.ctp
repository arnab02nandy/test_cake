<?php  $session=$this->request->session();
       $admin_id=$session->read('sp_admin.id');?>
<div id="wrapper">
      <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']) : "";?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?>
            <?php echo $this->Form->create($user,['type' => 'file',"name"=>"EditProfileFrm","id"=>"EditProfileFrm","role"=>"form"]) ?>
                
                
                <div class="form-group">
                    <label for="firstname" class="control-label">First name</label>
                  <?php echo $this->Form->input('firstname',['label' =>false,'placeholder' => __('First Name'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="lastname" class="control-label">Last Name</label>
                  <?php echo $this->Form->input('lastname',['label' =>false,'placeholder' => __('Last Name'),'autofocus',"class"=>"form-control required"]);?>
                </div>

                <div class="form-group">
                     <?php echo $this->Form->input('username',['label' =>["class"=>"control-label"],'placeholder' => __('User Name'),'autofocus',"class"=>"form-control required","id"=>'username']);?>
                </div>

                <div class="form-group">
                  <?php echo $this->Form->input('email',['label' =>["class"=>"control-label"],'placeholder' => __('Email'),'autofocus',"class"=>"form-control required email","id"=>'email']);?>
                </div>
                
                
                <div class="form-group">
                  <?= $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]) ?>
                </div>
            <?php echo $this->Form->end() ?>
          </div>
        </div>
    </div>
  </div>


<script>
$(document).ready(function(){
    
    $("#EditProfileFrm").validate({
        rules: {
                
        },
        submitHandler:function(form) {
           $('button').prop('disabled',true);
            $.post('<?php echo $this->Url->build('/Users/isEmailidExixts')?>',{'email':$('#email').val(),'id':'<?php echo $admin_id;?>'},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#email').after('<label for="email" generated="true" class="error">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                    $.post('<?php echo $this->Url->build('/Users/isUsernameExixts')?>',{'username':$('#username').val(),'id':'<?php  echo $admin_id;?>'},function(rtn_data){
                        var rtn_data_arr=JSON.parse(rtn_data);
                        if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                          $('#username').after('<label for="username" generated="true" class="error">'+rtn_data_arr.msg+'</label>');
                          $('button').prop('disabled',false);
                        } else {
                          form.submit();
                        }
                    });
                }
            });
        }
    });
});
</script>
	

