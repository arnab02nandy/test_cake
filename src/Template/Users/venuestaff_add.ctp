<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo isset($data['heading']) ? trim($data['heading']) : "Add";?></h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <?php echo $this->Form->create($user,['type' => 'file',"name"=>"addform","id"=>"addform","role"=>"form"]) ?>
                        
                         <div class="form-group">
                            <label for="firstname" class="control-label">First name</label>
                          <?php echo $this->Form->input('firstname',['label' =>false,'placeholder' => __('First Name'),'autofocus',"class"=>"form-control required"]);?>
                         </div>

                        <div class="form-group">
                            <label for="lastname" class="control-label">Last Name</label>
                          <?php echo $this->Form->input('lastname',['label' =>false,'placeholder' => __('Last Name'),'autofocus',"class"=>"form-control required"]);?>
                         </div>


                        <div class="form-group">
                          <?php echo $this->Form->input('email',['label' =>["class"=>"control-label"],'placeholder' => __('Email'),'autofocus',"class"=>"form-control required email","id"=>'email']);?>
                        </div>                         

                        <div class="form-group">
                        <label for="" class="control-label">Contact No</label>
                         <?php echo $this->Form->input('phone',['label' =>["class"=>"control-label"],'placeholder' => __('Contact No'),'autofocus',"class"=>"form-control number_field required ","id"=>'phone','type'=>'text','label'=>false]);?>
                        </div>

                        <div class="form-group">
                         <label for="" class="control-label">Address</label>
                         <?php echo $this->Form->textarea('address',['label' =>["class"=>"control-label"],'placeholder' => __('Address'),'autofocus',"class"=>"form-control ","id"=>'address','type'=>'text']);?>
                        </div>

                        <div class="form-group">
                         <label for="" class="control-label">Comment</label>
                         <?php echo $this->Form->textarea('comment',['label' =>["class"=>"control-label"],'placeholder' => __('Comment'),'autofocus',"class"=>"form-control ","id"=>'comment','type'=>'text']);?>
                        </div> 

                      <?php /*   <div class="form-group">
                         <label for="profile_image" class="control-label">Profile Image</label>
                         <?php echo $this->Form->file('profile_image',['label' =>false,'autofocus',"class"=>"form-control required"]);?>                         
                         </div>  */  ?>                                                              
                        
                        <div class="form-group">
                         <?php echo $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]) ?>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>						
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $("#addform").validate({
        rules: {                  
           
               },
                submitHandler:function(form) {
                $('button').prop('disabled',true);
                $.post('<?php echo $this->Url->build('/Users/isEmailidExixts')?>',{'email':$('#email').val()},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#emailERROR').remove();
                    $('#email').after('<label for="Code" generated="true" class="error" id="emailERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                     form.submit();
                }
            });
        }
        
                  

    });
});
</script>
<script>
$(document).ready(function(){

    $(".number_field").keyup(function() {
    var newval = $(this).val();    
    $(this).val(newval.replace(/[^0-9'\s]/gi, ''));                
    });

});
</script>