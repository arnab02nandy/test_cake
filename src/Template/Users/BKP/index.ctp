<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12"><h1 class="page-header page_title">Manage Users</h1></div>
            <div class="col-lg-2 add_btn" >
                <a href="<?php echo $this->Url->build('/admin/add-user');?>"><button class="btn btn-success">Add User</button></a>
            </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="notifyMessage"><?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?> </div>
                <div class="panel panel-default">
                <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>SL</th>        
                                <th><?php echo $this->Paginator->sort('username',"Name") ?></th>
                                <th><?php echo $this->Paginator->sort('email') ?></th>
                                <th>TYPE</th>
                                <th>Payment</th>
                                <th class="actions">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($users)){
                          $counter=1;
                          foreach ($users as $user){
                        ?>
                          <tr id="dataRow_<?php echo $user->id;?>">
                              <td align="center"><?php echo $counter;?></td>
                              <td align="center"><?php echo $user->username; ?></td>
                              <td align="center"><?php echo $user->email; ?></td>
                              <td align="center"><?php echo h($user->user_type->type_name); ?></td>
                              <td align="center">
                              <?php echo  $this->Html->link($this->Html->tag('i', '',array('class' => 'fa fa-usd fa-lg','title'=>'Click to View Payment')), '/admin/user-payment/'.$user->id, array('escape' => false)) ?>
                              </td>
                              <td align="center" class="actions">
                                <?php //echo $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                                <?php //echo $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                                <?php echo  $this->Html->link($this->Html->tag('i', '',array('class' => 'fa fa-pencil-square-o fa-lg','title'=>'Click to Edit')), '/admin/edit-user/'.$user->id, array('escape' => false)) ?>
                                  <?php //echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $counter)]) ?>
                                  <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Users/ajaxdelete');?>" data-id="<?php echo $user->id;?>", class="deleteDataRow" ><i style="color:#FF0000" title="Click to Delete" class="fa fa-times-circle fa-lg" aria-hidden="true"></i></a>
                                  <?php if(trim($user->status)=='ACTIVE'){?>
                                  <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Users/changestatus');?>" data-id="<?php echo $user->id;?>", class="ChangeStatus" id="ChangeStatus_<?php echo $user->id;?>" data-status="<?php echo $user->status;?>"><i style="color:#439f43" title="Click to Inactive" class="fa fa-check-square fa-lg" aria-hidden="true"></i></a>
                                  <?php } else{ ?>
                                  <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Users/changestatus');?>" data-id="<?php echo $user->id;?>", class="ChangeStatus" id="ChangeStatus_<?php echo $user->id;?>" data-status="<?php echo $user->status;?>"><i style="color:#FF0000" title="Click to Active" class="fa fa-check-square fa-lg" aria-hidden="true"></i></a>
                                  
                                  <?php } ?>
                              </td>
                          </tr>
                          <?php
                              $counter++;
                              }
                          }else {
                              echo '<tr><td align="center" colspan="6">No Result Found.</td></tr>';
                          }
                          ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <p><?= $this->Paginator->counter() ?></p>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>