<div id="wrapper">
      <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']) : "";?></h1>
              <div class="col-lg-2 add_btn" >
                <a href="<?php echo $this->Url->build('/admin/userlist');?>"><button class="btn btn-success">Back</button></a>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-offset-2">
               <?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?>
            <?php echo $this->Form->create($user,['type' => 'file',"name"=>"addUserFrm","id"=>"addUserFrm","role"=>"form"]) ?>
                <div class="form-group">
                     <?php echo $this->Form->input('username',['label' =>["class"=>"control-label"],'placeholder' => __('User Name'),'autofocus',"class"=>"form-control required","id"=>'username']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('email',['label' =>["class"=>"control-label"],'placeholder' => __('Email'),'autofocus',"class"=>"form-control required email","id"=>'email']);?>
                </div>
                
                <div class="form-group">
                  <?php echo $this->Form->input('code',['label' =>["class"=>"control-label"],'placeholder' => __('Code'),'autofocus',"class"=>"form-control required","id"=>'Code']);?>
                </div>
                
                <div class="form-group">
                  <?php echo $this->Form->input('password',['label' =>["class"=>"control-label"],'placeholder' => __('password'),'autofocus',"class"=>"form-control required minlength[6]","id"=>'Password','type'=>'password']);?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->input('confirm_password',['label' =>["class"=>"control-label"],'placeholder' => __('Confirm Password'),'autofocus',"class"=>"form-control required equalTo",'type'=>'password','id'=>'ConfirmPassword']);?>
                </div>
                <div class="form-group">
                    <label for="fname" class="control-label">First name</label>
                  <?php echo $this->Form->input('fname',['label' =>false,'placeholder' => __('First Name'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="lname" class="control-label">Last Name</label>
                  <?php echo $this->Form->input('lname',['label' =>false,'placeholder' => __('Last Name'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                
                <div class="form-group">
                    <label>User Type</label>
                    <?php echo $this->Form->select('user_type_id',$UserTypesList,['empty' => '---Select---','class'=>'form-control required','autofocus']); ?>
                </div>
               
                <div class="form-group">
                    <label for="phone" class="control-label">Primary Phone-no</label>
                    <?php echo $this->Form->input('user_profile.phone',['label' =>false,'placeholder' => __('Primary Phone-no'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="other_phone" class="control-label">Other Phone-no</label>
                    <?php echo $this->Form->input('user_profile.other_phone',['label' =>false,'placeholder' => __('Other phone-no'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="qualification" class="control-label">Qualification</label>
                    <?php echo $this->Form->input('user_profile.qualification',['label' =>false,'placeholder' => __('Qualification'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                   <label for="designation" class="control-label">Designation</label>
                    <?php echo $this->Form->input('user_profile.designation',['label' =>false,'placeholder' => __('Designation'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="school" class="control-label">School</label>
                    <?php echo $this->Form->input('user_profile.school',['label' =>false,'placeholder' => __('School'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="address" class="control-label">Address</label>
                    <?php echo $this->Form->input('user_profile.address',['label' =>false,'placeholder' => __('Address'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="postal_code" class="control-label">Zip Code</label>
                    <?php echo $this->Form->input('user_profile.postal_code',['label' =>false,'placeholder' => __('Zip Code'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                
                <div class="form-group">
                    <label for="account_no" class="control-label">Bank Account-no</label>
                    <?php echo $this->Form->input('user_profile.account_no',['label' =>false,'placeholder' => __('Bank Account-no'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="bank_name" class="control-label">Bank Name</label>
                    <?php echo $this->Form->input('user_profile.bank_name',['label' =>false,'placeholder' => __('Bank Name'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="bank_code" class="control-label">Bank Code</label>
                    <?php echo $this->Form->input('user_profile.bank_code',['label' =>false,'placeholder' => __('Bank Code'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="branch_code" class="control-label">Branch Code</label>
                    <?php echo $this->Form->input('user_profile.branch_code',['label' =>false,'placeholder' => __('Branch Code'),'autofocus',"class"=>"form-control required"]);?>
                </div>
                <div class="form-group">
                    <label for="profile_image" class="control-label">Profile Image</label>
                    <?php echo $this->Form->file('user_profile.upload_image',['label' =>false,'autofocus',"class"=>"form-control required"]);?>
                    <?php if(isset($data_error['upload_image']) && trim($data_error['upload_image'])!=""){ ?>
                    <label for="profile_image" generated="true" class="error"><?php echo $data_error['profile_image']; ?></label>
                    <?php } ?>
                </div>
                <div class="form-group">
                  <?php echo $this->Form->button(__('Save'),["class"=>"btn btn-primary"]) ?>
                </div>
            <?php echo $this->Form->end() ?>
          </div>
        </div>
    </div>
  </div>


<script>
$(document).ready(function(){
    $("#addUserFrm").validate({
        rules: {
            username:{numberLetter:$("#username").val()},
            code:{numberLetter:$("#Code").val()},
            confirm_password:{equalTo: "#Password"},
            password:{minlength: "6"},
        },
        submitHandler:function(form) {
           $('button').prop('disabled',true);
            $.post('<?php echo $this->Url->build('/Users/isEmailidExixts')?>',{'email':$('#email').val(),'id':''},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#email').after('<label for="email" generated="true" class="error">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                    $.post('<?php echo $this->Url->build('/Users/isUsernameExixts')?>',{'username':$('#username').val(),'id':''},function(rtn_data){
                        var rtn_data_arr=JSON.parse(rtn_data);
                        if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                          $('#username').after('<label for="username" generated="true" class="error">'+rtn_data_arr.msg+'</label>');
                          $('button').prop('disabled',false);
                        } else {
                            if($("#Code").val().trim()!==""){
                                $.post('<?php echo $this->Url->build('/Users/isCodeExixts')?>',{'username':$("#Code").val().trim(),'id':''},function(rtn_data){
                                    var rtn_data_arr=JSON.parse(rtn_data);
                                    if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                                        $('#Code').after('<label for="code" generated="true" class="error">'+rtn_data_arr.msg+'</label>');
                                        $('button').prop('disabled',false);
                                    }else{
                                        form.submit();
                                    }
                                });
                            }else{
                                form.submit();
                            }
                        }
                    });
                }
            });
        }
        
    });
});
</script>