<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo isset($data['heading']) ? trim($data['heading']) : "Add";?></h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <?php echo $this->Form->create($customertype,['type' => 'file',"name"=>"editform","id"=>"editform","role"=>"form"]) ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('customer_type_name',['label' =>["class"=>"control-label"],'placeholder' => __(''),'autofocus',"class"=>"form-control required","id"=>'customertype']);?>
                        </div>
                       
                        <div class="form-group">
                         <?php echo $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]) ?>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>						
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $("#editform").validate({
        rules: {
                
           
        },
        submitHandler:function(form) {
           $('button').prop('disabled',true);
            $.post('<?php echo $this->Url->build('/customertypes/customertypeExixts')?>',{'id':'<?php echo $customertype->id;?>','customer_type_name':$('#customertype').val()},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#customertypeERROR').remove();
                    $('#customertype').after('<label for="Code" generated="true" class="error" id="customertypeERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                     form.submit();
                }
            });
        }
    });
});
</script>