<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Contact to service provider</h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">
            <div class="row">
				<div class="col-md-12">
				    <div class="sp-tbl-wrapper">     

<input type="hidden" name="to_name" id="to_name"   value="<?php echo $memberdata->name;?>">
<input type="hidden" name="to_email" id="to_email" value="<?php echo $memberdata->email;?>">

<p class="pri-message success" id="success_msg" style="display:none;">                
              Your Query has been submitted successfully.               </p>
               <p class="pri-message faild" id="error_msg" style="display:none;">
                Error in mail sending, please try again.   
               </p> 


<?php echo $this->Form->create('contact',["name"=>"contactform","id"=>"contactform","role"=>"form"]) ?>   
                                    
                   
                  
             
              <div class="form-group">
                
                <?php echo $this->Form->input('name',['placeholder' => 'Name','label'=>false,"class"=>"form-control ","id"=>'name','onkeypress'=>"remove_style('name')"]);?>

                <p class="error"></p>
                
              </div>
              
              <div class="form-group">
                
               
                 <?php echo $this->Form->input('email',['placeholder' =>'Email','label'=>false,'autocomplete'=>'off','autofocus',"class"=>"form-control","id"=>'email','onkeypress'=>"remove_style('email')"]);?>
                <p class="error" id="email-error"></p>
                
              </div>
              
             
             
             
              <div class="form-group">
               
               
                <?php echo $this->Form->input('contact_no',['placeholder' => 'Contact No','label'=>false,"class"=>"contact_field form-control","id"=>'contact_no','onkeypress'=>"remove_style('contact_no')"]);?>
                <p class="error"></p>
              </div>


              <div class="form-group">              
               
                <?php echo $this->Form->textarea('message',['placeholder' => 'Message','label'=>false,"class"=>"form-control","id"=>'message','onkeypress'=>"remove_style('message')"]);?>
                <p class="error"></p>
              </div>
               
              <div class="form-group">                 
                  <!--<p class="note">if you can't login, please reset the password and try again.</p> -->
              </div>    
             <input type="hidden" name="type" value="register">
            <?php //echo $this->Form->submit('Submit', array('label' => false, 'name' => 'regsubmit', 'class' => 'btn-submit', 'id' => 'regsubmit', 'alt' => 'Submit', ));?>
            <?php echo $this->Form->button(__('send'),["class"=>"btn-submit",'id'=>'contact_submit']) ?>
             <!--<i class="fa fa-user" aria-hidden="true"></i>-->

            <?php echo $this->Form->end(); ?>


                          


                    </div>
				    
				    
				</div>
				
            </div>						
        </div>
    </div>
</div>
<style>
    .errorClass { border:  2px solid red !important; }
</style>


<script type="text/javascript">              
     function remove_style(id)
        {               
            $("#"+id).removeClass("errorClass");
        }  
    
     $(document).ready(function(){  
     $('#success_msg').hide();
     $('#error_msg').hide();     
        
    $("#contact_submit").click(function(e){     
    
    var to_name               = $.trim($('#to_name').val());
    var to_email              = $.trim($('#to_email').val());
    var name                  = $.trim($('#name').val());       
    var email                 = $.trim($('#email').val());     
    var contact_no            = $.trim($('#contact_no').val()); 
    var message               = $.trim($('#message').val()); 
   
   
      if(!name)
      {                
            $('#name').addClass('errorClass');  
            $('#name').focus();
            return false;
      } 
        
      else if(!email)
      {       
        $('#email').addClass('errorClass');  
            $("#email").focus();
            return false;
      }
     else if((/^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z.]{2,5}$/).exec(email)== null)
      { 
        $('#email').addClass('errorClass');  
            $("#email").focus();
            return false;
      }
      
         
     else if(!contact_no)
      { 
            $('#contact_no').addClass('errorClass');  
            $("#contact_no").focus();
            return false;
      }

       else if(contact_no.length !=10){
            $('#contact_no').addClass('errorClass');  
            $('#contact_no').focus();
            
            return false;
        }
      else if(!message)
      { 
            $('#message').addClass('errorClass');  
            $("#message").focus();
            return false;
      }
        
        // e.preventDefault();
       
        $.ajax({         
              url : "<?php echo $this->Url->build('/Categoryservices/Contactmail')?>",          
              type : "POST",
              data : {to_name:to_name,to_email:to_email,name:name,email:email,contact_no:contact_no,message:message},
              success:function(data)
              {         
                  if(data=="success") 
                  {
                    $("#contactform")[0].reset();
                     $('#error_msg').hide();
                      
                       $('#success_msg').show(800).delay(4000).hide(800);  
                  } 
                  else 
                  {
                     $("#contactform")[0].reset();
                     $('#success_msg').hide();
                     
                     $('#error_msg').show(800).delay(4000).hide(800);  

                  }    
              } 
             });
    
        return false;       
             
    }); 

    }); 

 $(".contact_field").keyup(function() {
    var newval = $(this).val();    
    $(this).val(newval.replace(/[^0-9'\s]/gi, ''));                
    });
         </script>
