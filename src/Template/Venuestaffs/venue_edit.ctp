<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo isset($data['heading']) ? trim($data['heading']) : "Add";?></h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <?php echo $this->Form->create($venue,['type' => 'file',"name"=>"editform","id"=>"editform","role"=>"form"]) ?>

                        <div class="form-group">
                        <label>Select State</label>
                        <?php echo $this->Form->select('state_id',$statedata,['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'state_id']); ?>
                        </div>  
                        <div class="form-group">
                        <label>Select Suburb</label>
                        <?php echo $this->Form->select('suburb_id',$suburbdata,['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'suburb_id']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('venue_name',['label' =>["class"=>"control-label"],'placeholder' => __('Venue Name'),'autofocus',"class"=>"form-control required","id"=>'venueName']);?>
                        </div>

                         <div class="form-group">
                            <?php echo $this->Form->input('username',['label' =>["class"=>"control-label"],'placeholder' => __('Username'),'autofocus',"class"=>"form-control required","id"=>'username']);?>
                        </div>  

                        <div class="form-group">
                        <label>Password</label>
                        <?php echo $this->Form->input('txt_password',['label' =>["class"=>"control-label"],'placeholder' => __('Password'),'autofocus',"class"=>"form-control required","id"=>'password','type'=>'password','label'=>false]);?>
                        </div>  

                        <div class="form-group">
                            <?php //echo $this->Form->input('username',['label' =>["class"=>"control-label"],'placeholder' => __('Username'),'autofocus',"class"=>"form-control required","id"=>'username']);?>
                        </div> 
                       
                        <div class="form-group">
                         <?php echo $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]) ?>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>						
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $("#editform").validate({
        rules: {
                 username:{minlength: "6"},
           
        },
        

           submitHandler:function(form) 
           {
           $('button').prop('disabled',true);
            $('#usernameERROR').remove();
            $.post('<?php echo $this->Url->build('/venues/venueExixts')?>',{'id':'<?php echo $venue->id;?>','venue_name':$('#venueName').val()},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                $('#venueNameERROR').remove();
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    //$('#venueNameERROR').remove();
                    $('#venueName').after('<label for="Code" generated="true" class="error" id="venueNameERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                  } else {

                 $.post('<?php echo $this->Url->build('/venues/usernameExixts')?>',{'id':'<?php echo $venue->id;?>','username':$('#username').val()},function(rtn_data) {
                 var rtn_data_arr=JSON.parse(rtn_data);
                 
                 if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                   // $('#usernameERROR').remove();
                    $('#username').after('<label for="Code" generated="true" class="error" id="usernameERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                        } else {
                          form.submit();
                        }
                    });
                }
             });
           }


    });


    $("#state_id").change(function(){
        var a=$(this).val();
        
    $("#suburb_id").html('<option value="">---Select---</option>');
    $.post('<?php echo $this->Url->build('/Venues/getSuburb');?>',{'state_id':$(this).val()},function(rtn_data){
        var rtn_data_arr=JSON.parse(rtn_data);
        
        if(rtn_data_arr.status!==undefined && rtn_data_arr.status===true){
            $.each(rtn_data_arr.data,function(k,val){
                $("#suburb_id").append('<option value="'+k+'">'+val+'</option>');
            });
        }
    });
});    
});
</script>