<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']).' List' :""; ?>
                    <div class="header-btn">
                        <a href="javascript:void(0)" alt="Add" title="Add" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/venue-add');?>"><button class="btn btn-success btn-bg-change">Add <?php echo isset($data['heading']) ? trim($data['heading']) :""; ?></button></a>
                    </div>
                </h1>
            </div>
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div id="notifyMessage"><?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?> </div>
                <div class="panel panel-default">
                <div class="panel-body table-responsive">
                <div class="dataTable_wrapper">
                 <form action="" name="" id="" method="post">
                    <table class="table table-bordered" id="dataTablesApnd">
                        <thead>
                            <tr>
                                <!-- <th>Sl No</th>  -->
                                 <th>Order<input type="submit" class="Update-Order-Btn" name="submit" value="Update Order" ></th>
                                <th>State</th>       
                                <th>Suburb</th>
                                <th>Venue Name</th>
                                <!-- <th>Username</th> -->
                               
                                 <th>Status</th>                                
                                <th class="actions">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($venues)){

                         // echo "<pre>";print_r($venues);exit;
                          $counter=1;
                          foreach ($venues as $venue){
                        ?>
                          <tr id="dataRow_<?php echo $venue->id;?>">
                             <td align="center"><input type="text" name="venue_order<?php echo '['.$venue->id.']'?>" id="venue_order" class="order_field" value="<?php echo ($venue->venue_order !='99999' ? $venue->venue_order : '');?>" ></td>
                              <td align="center"><?php echo $venue->state->state_name ?></td>
                              <td align="center"><?php echo $venue->suburb->suburb_name ?></td>                          
                              <td align="center"><?php echo $venue->venue_name ?></td>                             
                             
                              <td align="center">
                              <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Venues/changeStatus');?>" data-id="<?php echo $venue->id;?>", class="ChangeStatus" id="ChangeStatus_<?php echo $venue->id;?>" data-status="<?php echo $venue->status;?>">
                                  <?php if(trim($venue->status)=='ACTIVE'){?>
                                  <i style="color:#439f43" title="Click to Inactive" class="fa fa-check-square fa-lg" aria-hidden="true"></i>
                                  <?php } else {?>
                                  <i style="color:#FF0000" title="Click to Active" class="fa fa-check-square fa-lg" aria-hidden="true"></i>
                                  <?php } ?>
                                  </a>
                              </td>
                              <td align="center" class="actions">
                                
                                <a href="javascript:void(0)" alt="Edit" title="Click to edit" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/venue-edit/'.$venue->id);?>"><i class="fa fa-pencil-square-o fa-lg"></i></a>
                                
                                  <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/Venues/venueDelete');?>" data-id="<?php echo $venue->id;?>", class="deleteDataRow" ><i style="color:#FF0000" title="Click to delete" class="fa fa-times-circle fa-lg" aria-hidden="true"></i></a>
                                  
                                 
                              </td>
                          </tr>
                          <?php
                              $counter++;
                              }
                          }
                          ?>
                        </tbody>
                    </table>
                    </form>
                    
                </div>
                </div>
                </div>
            </div>
       <!-- </div>-->
        <!-- /.Icon Table -->
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Icon Info
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <a><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;View & Edit&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-times-circle fa-lg" style="color:#FF0000"></i></a>&nbsp;Delete&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-check-square fa-lg" style="color:#439f43"></i></a>&nbsp;Active&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-check-square fa-lg" style="color:#FF0000"></i></a>&nbsp;Inactive
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        <!--</div>-->
        <!-- /.Icon Table -->
    </div>
</div>
<!-- Data Tables -->
<script>
$(document).ready(function() {
    $('#dataTablesApnd').DataTable({
            responsive: true,
            "ordering": false,
            "info":     false,
            "oLanguage":{
            "sEmptyTable":"No Records found.",
            "emptyTable": "No Records found.",
            },
            "language": {
                "emptyTable": "No Records found.",
            },
            "aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
            "iDisplayLength": '<?php echo PAGE_DATA_LIMIT;?>', //Pagination limit
    });
});
</script>


