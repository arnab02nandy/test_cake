<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header page_title"><?php echo isset($data['heading']) ? trim($data['heading']).' List' :""; ?>
                    <div class="header-btn">
                        <a href="javascript:void(0)" alt="Add" title="Add" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/state-add');?>"><button class="btn btn-success btn-bg-change">Add <?php echo isset($data['heading']) ? trim($data['heading']) :""; ?></button></a>
                    </div>
                </h1>
            </div>
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div id="notifyMessage"><?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?> </div>
                <div class="panel panel-default">
                <div class="panel-body table-responsive">
                <div class="dataTable_wrapper">
                <form action="" name="f1" id="f1" method="post">
                    <table class="table table-bordered" id="dataTablesApnd">
                        <thead>
                            <tr>
                                <!-- <th>Sl No</th> -->  
                                 <th>Order<input type="submit" class="Update-Order-Btn" name="submit" value="Update Order" ></th>
                                <th>State Name</th>
                                
                                 <th>Status</th>                                
                                <th class="actions">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($states)){
                          $counter=1;
                          foreach ($states as $state){
                        ?>
                          <tr id="dataRow_<?php echo $state->id;?>">
                            <td align="center"><input type="text" name="state_order<?php echo '['.$state->id.']'?>" id="state_order" class="order_field" value="<?php echo ($state->state_order !='99999' ? $state->state_order : '');?>" ></td>
                                                      
                              <td align="center"><?php echo $state->state_name ?></td>
                             <?php /* <td align="center"><input type="text" name="state_order<?php echo '['.$state->id.']'?>" id="state_order" value="<?php echo $state->state_order ?>"></td>*/ ?>
                              <td align="center">
                              <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/States/changeStatus');?>" data-id="<?php echo $state->id;?>", class="ChangeStatus" id="ChangeStatus_<?php echo $state->id;?>" data-status="<?php echo $state->status;?>">
                                  <?php if(trim($state->status)=='ACTIVE'){?>
                                  <i style="color:#439f43" title="Click to Inactive" class="fa fa-check-square fa-lg" aria-hidden="true"></i>
                                  <?php } else {?>
                                  <i style="color:#FF0000" title="Click to Active" class="fa fa-check-square fa-lg" aria-hidden="true"></i>
                                  <?php } ?>
                                  </a>
                              </td>
                              <td align="center" class="actions">
                                
                                <a href="javascript:void(0)" alt="Edit" title="Click to edit" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/admin/state-edit/'.$state->id);?>"><i class="fa fa-pencil-square-o fa-lg"></i></a>
                                
                                  <a href="javascript:void(0)" data-href="<?php echo $this->Url->build('/States/stateDelete');?>" data-id="<?php echo $state->id;?>", class="deleteDataRow" ><i style="color:#FF0000" title="Click to Delete" class="fa fa-times-circle fa-lg" aria-hidden="true"></i></a>
                                  
                                 
                              </td>
                          </tr>
                          <?php
                              $counter++;
                              }
                          }
                          ?>
                        </tbody>
                    </table>
                    </form>
                    
                </div>
                </div>
                </div>
            </div>
       <!-- </div>-->
        <!-- /.Icon Table -->
        <!--<div class="row">-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Icon Info
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <a><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;View & Edit&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-times-circle fa-lg" style="color:#FF0000"></i></a>&nbsp;Delete&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-check-square fa-lg" style="color:#439f43"></i></a>&nbsp;Active&nbsp;&nbsp;|&nbsp;
                        <a><i class="fa fa-check-square fa-lg" style="color:#FF0000"></i></a>&nbsp;Inactive
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        <!--</div>-->
        <!-- /.Icon Table -->
    </div>
</div>
<!-- Data Tables -->
<script>
$(document).ready(function() {
    $('#dataTablesApnd').DataTable({
            responsive: true,
            "ordering": false,
            "info":     false,
            "oLanguage":{
            "sEmptyTable":"No Records found.",
            "emptyTable": "No Records found.",
            },
            "language": {
                "emptyTable": "No Records found.",
            },
            "aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
            "iDisplayLength": '<?php echo PAGE_DATA_LIMIT;?>', //Pagination limit
    });
});
</script>


