<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>


<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Responsive Email Template</title>
    <style type="text/css">
        .ReadMsgBody {
            width: 100%;
            background-color: #ffffff;
        }
        
        .ExternalClass {
            width: 100%;
            background-color: #ffffff;
        }
        
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            font-family: Arial, Helvetica, sans-serif
        }
        
        table {
            border-collapse: collapse;
        }
        
        @media only screen and (max-width: 640px) {
            body[yahoo] .deviceWidth {
                width: 440px!important;
                padding: 0;
            }
            body[yahoo] .center {
                text-align: left !important;
            }
        }
        
        @media only screen and (max-width: 479px) {
            body[yahoo] .deviceWidth {
                width: 280px!important;
                padding: 0;
            }
            body[yahoo] .center {
                text-align: left!important;
            }
        }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Arial, Helvetica, sans-serif">
    <!-- Wrapper -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td width="100%" valign="top" bgcolor="#f2f2f2" style="padding-top:20px">
                <!--Start Header-->
                <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="border-top:8px solid #E9A500;">
                    <tr>
                        <td style="padding: 6px 0px 0px">
                            <table width="680" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#fff">
                                <tr>
                                    <td width="100%">
                                        <!--Start logo-->
                                        <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                            <tr>
                                                <td class="center" style="padding: 0px 0px 0px 10px; width:200px; ">
                                                    <a href="#"><img src="<?php echo BASE_URL;?>/frontend/images/logo.png" style="max-height:100%; max-width:100%; padding:10px 10px 10px 0px"></a>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <!--End logo-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--End Header-->
                <!--Start Support-->
                <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                    <tr>
                        <td width="100%" bgcolor="#ffffff" class="center" style="padding:40px 20px;">
                            <table border="0" cellpadding="0" cellspacing="0" align="left" style="border:1px solid #ffde8f; padding:10px; background-color:#fff7e2;" width="100%">
                                <tr>
                                    <td style="padding:10px;">
                                        <table>
                                           
                                           <tr>
                                                <td class="center" style="font-size: 14px; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 22px; vertical-align: middle; padding: 5px 10px; "> <strong  style="color:#000ca6;">Dear Admin,</strong><br>
                                                 <p style="text-indent: 105px; margin-top:0; margin-bottom:0;">Someone want to contact you, his/her information as follows:</p>
                                                
                                                </td>
                                                
                                            </tr>
                                           
                                           
                                           
                                            <tr>
                                                <td class="center" style="font-size: 14px; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 22px; vertical-align: middle; padding: 5px 10px 0px 10px; "> <strong>Name: </strong> <?php echo $name;?> </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="center" style="font-size: 14px; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 5px 10px 0px 10px; "> <strong>Email: </strong> <?php echo $email;?> </td>
                                            </tr>
                                            <tr>
                                                <td class="center" style="font-size: 14px; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 5px 10px 0px 10px; "> <strong>Phone: </strong> <?php echo $phone;?> </td>
                                            </tr>
                                            <tr>
                                                <td class="center" style="font-size: 14px; color: #000000; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 5px 10px; "> <strong>Message: </strong> <?php echo $message;?> </td>
                                            </tr>
                                            
                                            
                                            <tr>
                                                <td class="center" style="font-size: 14px; color: #000ca6; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 5px 10px 0px 10px; "> <strong></strong></td>
                                            </tr>
                                            
                                            <!--<tr>
                                                <td class="center" style="font-size: 14px; color: #000ca6; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 5px 10px 0px 10px; "> <strong>Kind regards,</strong></td>
                                            </tr>-->
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--End Support-->
                <!-- Footer -->
                <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#fff" style="background-color: #E9A500;">
                    <tr>
                        <td class="center" style="font-size: 12px; color: #000000; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 50px 20px 50px; "> Copyright &copy; 2016 Tradefix. All rights reserved. </td>
                    </tr>
                </table>
                <!--End Footer-->
                <div style="height:15px">&nbsp;</div>
                <!-- divider-->
            </td>
        </tr>
    </table>
    <!-- End Wrapper -->
</body>

</html>