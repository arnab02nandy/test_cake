 <div class="banner-wrapper container-fluid no-padd">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="banner">
                            <img src="images/banner.jpg" alt="banner">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="menu-wrapper container-fluid no-padd">
            <div class="container">
                <div class="dis-table">
                    <div class="menu-left dis-cell">
                        <h3>Venue Message List From Office</h3>
                    </div>
                    
                    <div class="menu-right dis-cell">
                         <div class="dropdown cust-dropdown-menu">
                          <button class="btn dropdown-toggle cust-navbar-toggle" type="button" data-toggle="dropdown">
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo $this->Url->build('/staff/dashboard');?>">Dashboard</a></li>
                            <li><a href="#">Record student attendance</a></li>
                            <li><a href="#">Enroll new student</a></li>
                            <li><a href="#">Communicate with office</a></li>
                            <li><a href="#">Class roster</a></li>
                            <li><a href="#">Receive instalment payment</a></li>
                            <li><a href="<?php echo $this->Url->build('/Staffs/logout');?>">Log out</a></li>
                          </ul>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        
        
        
    
    <div class="main-content container-fluid no-padd">
       
     
       <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        
                        

                            
                           <!--  <div class="alert alert-success">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success!</strong> Indicates a successful or positive action.
                            </div>
                            <div class="alert alert-danger">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                            </div>  --> 
<!--

                             <div class="table-responsive">
                            <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Sl No</th>
                                      <th>Category</th>
                                      <th>Job Title</th>
                                                              
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
-->
                  <?php 
                    
                  /*  $sl=1;
                    if(!empty($jobdata))  
                                        {

                    foreach($jobdata as $job)
                    {*/?>
                    
                    
<!--
                                    <tr>                                    
                                      <td scope="row"><?php //echo $sl;?></td>
                       <td><?php //echo $job->category->category_name;?></td>
                                      <td><?php //echo $job->job_title;?></td>
                                                                      
                                      
                                      <td class="text-center">
                                      <a href="<?php //echo $this->Url->build('/edit-job/'.$job->job_seo);?>" class="symbol-btn" title="Click to edit job"><?php //echo $this->Html->image(BASE_URL.'/frontend/images/edit.png')?></a>

                                      
                                     
                                     

                                      </td>
                                    </tr>                                   
                                   
                                  
-->
                
                     <?php
                   /* $sl++;
                    
                    }
                                     }

                                     else{*/?>
<!--                                     <tr ><td colspan="7" align="center">No record found !</td></tr>  -->
                                   <?php // }
                                     ?>
                                     </tbody>
                                </table>

                        </div>            
                            
                            
                             
                            
                            
                            <div class="col-md-10 col-md-offset-1">
                            
                            
                           <div class="panel panel-default panel-table venue-table">
                              <div class="panel-heading">
                                <div class="row">
<!--
                                  <div class="col col-xs-6">
                                    <h3 class="panel-title">Messages</h3>
                                  </div>
-->
                                  <div class="col col-xs-12 text-right">
                                   
<!-- <button type="button" class="btn btn-sm btn-default btn-create">Send Message</button> -->
                                      <a href="javascript:void(0)" alt="" title="Send Message" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/staff/send-message');?>"><button type="button" class="btn btn-sm btn-default btn-create">Send Message</button></a>
                                  </div>
                                </div>
                              </div>
                              <div class="panel-body">
                               
                               
                               <div class="table-responsive">
                                <table class="table table-striped table-bordered table-list">
                                  <thead>
                                    <tr>
                                       
                                
                                        <th><em class="fa fa-calendar"></em>Date</th>
                                        <th><em class="fa fa-envelope"></em>Message</th>
                                        <th class="text-center"><em class="fa fa-cog"></em>Action </th>
                                    </tr> 
                                  </thead>
                                  <tbody>

                  <?php                     
                  
                    if(!empty($rows))  
                                        {

                    foreach($rows as $row)
                    {?>
                                          <tr>
                                            
                                            <td><?php
                                            $date=explode(' ',$row['created']);

                                             $date = date('d-m-Y', strtotime($date[0]));
                      echo $date;?></td>
                                            <td><?php echo $row['message'];?></td>
                                            <td class="text-center">
                                              

                                              <a href="javascript:void(0)" alt="Edit" title="Click to edit" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr btn btn-icon" data-href="<?php echo $this->Url->build('/staff/view-message/'.$row['id']);?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          
                                            </td>
                                          </tr>
                                          
                                        


                     <?php
                  
                    
                    }
                                     }

                                     else{?>
                                    <tr ><td colspan="4" align="center">No record found !</td></tr> 
                                   <?php  }
                                     ?>
                                     </tbody>
                                </table>

                                  </div>
                             
                             
                             
                             
                              </div>
                            <div class="panel-footer">
                                <div class="row">
                                  <div class="col col-xs-4">Page 1 of 5
                                  </div>
                                  <div class="col col-xs-8">
                                    <ul class="pagination hidden-xs pull-right">
                                      <li><a href="#">1</a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">4</a></li>
                                      <li><a href="#">5</a></li>
                                    </ul>
                                    <ul class="pagination visible-xs pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                
                        
                    </div>
                </div>
                
                
                
                
                
                
                
                
            </div>
        </div>
        </div>
    </div>
    