 <div class="banner-wrapper container-fluid no-padd">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="banner">
                            <img src="images/banner.jpg" alt="banner">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="menu-wrapper container-fluid no-padd">
            <div class="container">
                <div class="dis-table">
                    <div class="menu-left dis-cell">
                        <h3>Student Attendance</h3>
                    </div>
                    
                    <div class="menu-right dis-cell">
                         <div class="dropdown cust-dropdown-menu">
                          <button class="btn dropdown-toggle cust-navbar-toggle" type="button" data-toggle="dropdown">
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Record student attendance</a></li>
                            <li><a href="#">Enroll new student</a></li>
                            <li><a href="#">Communicate with office</a></li>
                            <li><a href="#">Class roster</a></li>
                            <li><a href="#">Receive instalment payment</a></li>
                            <li><a href="#">Log out</a></li>
                          </ul>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        
        
        
    
    <div class="main-content container-fluid no-padd">
       
       
       <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        
                        
                            
                            <div class="alert alert-success">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success!</strong> Indicates a successful or positive action.
                            </div>
                            <div class="alert alert-danger">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
                            </div>  


                             <div class="table-responsive">
                            <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Sl No</th>
                                      <th>Category</th>
                                      <th>Job Title</th>
                                                              
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                  <?php 
                    
                  /*  $sl=1;
                    if(!empty($jobdata))  
                                        {

                    foreach($jobdata as $job)
                    {*/?>
                    
                    
                                    <tr>                                    
                                      <td scope="row"><?php //echo $sl;?></td>
                       <td><?php //echo $job->category->category_name;?></td>
                                      <td><?php //echo $job->job_title;?></td>
                                                                      
                                      
                                      <td class="text-center">
                                      <a href="<?php //echo $this->Url->build('/edit-job/'.$job->job_seo);?>" class="symbol-btn" title="Click to edit job"><?php //echo $this->Html->image(BASE_URL.'/frontend/images/edit.png')?></a>

                                      
                                     
                                     

                                      </td>
                                    </tr>                                   
                                   
                                  </tbody>
                
                     <?php
                   /* $sl++;
                    
                    }
                                     }

                                     else{*/?>
                                     <tr ><td colspan="7" align="center">No record found !</td></tr>  
                                   <?php // }
                                     ?>
                                </table>

                        </div>            
                            
                            
                             
                            
                            
                            
                            
                            
                           
                            
                            
                      
                        
                        
                        
                        
                    </div>
                </div>
                
                
                
                
                
                
                
                
            </div>
        </div>
        </div>
    </div>
    