        
        <div class="menu-wrapper container-fluid no-padd">
            <div class="container">
                <div class="dis-table">
                    <div class="menu-left dis-cell">
                        <h3>Enroll New Student</h3>
                    </div>
                    
                    <div class="menu-right dis-cell">
                         <div class="dropdown cust-dropdown-menu">
                          <button class="btn dropdown-toggle cust-navbar-toggle" type="button" data-toggle="dropdown">
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo $this->Url->build('/staff/dashboard');?>">Dashboard</a></li>
                            <li><a href="javascript:void(0);">Record student attendance</a></li>
                            <li><a href="javascript:void(0);">Enroll new student</a></li>
                            <li><a href="<?php echo $this->Url->build('/staff/dashboard');?>">Communicate with office</a></li>
                            <li><a href="javascript:void(0);">Class roster</a></li>
                            <li><a href="javascript:void(0);">Receive instalment payment</a></li>
                            <li><a href="<?php echo $this->Url->build('/Staffs/logout');?>">Log out</a></li>
                          </ul>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        
        
        
    
    <div class="main-content container-fluid no-padd">
       
       
       <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">

                    <!--  <div class="alert alert-success">                             
                            </div>
                            <div class="alert alert-danger">                             
                            </div> -->
                            <div id="notifyMessage"><?php echo $this->Flash->render('SUCCESS'); echo $this->Flash->render('ERROR'); ?> </div>

                        
                       
                         <?php echo $this->Form->create('',['type' => 'file',"name"=>"addform","id"=>"addform","role"=>"form", "class"=>"reg-form"]) ?>
                            
                           
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>First Name <span class="md-mark">*</span></label>
                                       
                                       <span class="note-label">Mandatory field</span>
                                        <?php echo $this->Form->input('firstname',['label' =>false,'placeholder' => __('First Name'),'autofocus',"class"=>"form-control required"]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Last Name<span class="md-mark">*</span></label>
                                        <span class="note-label">Mandatory field</span>
                                         <?php echo $this->Form->input('lastname',['label' =>false,'placeholder' => __('Last Name'),'autofocus',"class"=>"form-control required"]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Email<span class="md-mark">*</span></label>
                                       <span class="note-label">Mandatory field</span>
                                        <?php echo $this->Form->input('email',['label' =>["class"=>"control-label"],'placeholder' => __('Email'),'autofocus',"class"=>"form-control required email","id"=>'email','label' =>false]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Select Gender<span class="md-mark">*</span></label>
                                       <span class="note-label">Mandatory field</span>
                                        <?php
                                         $genderdata=array('Female'=>'Female','Male'=>'Male');
                                         echo $this->Form->select('gender',$genderdata,['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'gender']); ?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                            
                            
                      <?php /*      <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo $this->Form->select('state_id',$statedata,['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'state_id']); ?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo $this->Form->select('suburb_id',array(),['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'suburb_id']); ?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo $this->Form->select('venue_id',$venuedata,['empty' => '---Select---','class'=>'form-control required','id'=>'venue_id']); ?> 
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Zip for city and State*">
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>*/?>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Address</label>
                                        <?php echo $this->Form->textarea('address',['label' =>false,'placeholder' => __('Address'),'autofocus',"class"=>"form-control"]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>State</label>
                                        <?php echo $this->Form->input('state',['label' =>false,'placeholder' => __('State'),'autofocus',"class"=>"form-control "]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Postcode</label>
                                        <?php echo $this->Form->input('postcode',['label' =>false,'placeholder' => __('Postcode'),'autofocus',"class"=>"form-control  number_field"]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Landline Number</label>
                                        <?php echo $this->Form->input('phone',['label' =>false,'placeholder' => __('Landline Number'),'autofocus',"class"=>"form-control  number_field"]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Mobile Number</label>
                                        <?php echo $this->Form->input('mobile',['label' =>false,'placeholder' => __('Mobile Number'),'autofocus',"class"=>"form-control  number_field"]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                            

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Date of Birth</label>
                                        <?php echo $this->Form->input('dob',['label' =>false,'placeholder' => __('Date of Birth'),'autofocus',"class"=>"form-control ","id"=>"dob","autocomplete"=>'off']);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Select Customer Type</label>
                                        <?php echo $this->Form->select('customer_type_id',$customerdata,['empty' => '---Select---','class'=>'form-control ','autofocus','id'=>'customer_type_id','label'=>false]); ?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>  

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Comment</label>
                                        <?php echo $this->Form->textarea('comment',['label' =>false,'placeholder' => __('Comment'),'autofocus',"class"=>"form-control "]);?>
                                        <p class="val-error">Error</p>
                                        
                                    </div>
                                    
                                </div>
                            </div>
        
                           
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                      
                                        <?php echo $this->Form->button(__('Save'),["class"=>"cust-btn"]) ?>
                                    </div>
                                    
                                </div>
                                
        
                            </div>
                            
                            
                        <?php echo $this->Form->end(); ?>
                        
                        
                        
                        
                    </div>
                </div>
                
                
                
                
                
                
                
                
            </div>
        </div>
        </div>
    </div>
<script>
$(document).ready(function(){
    $("#addform").validate({
        rules: {                  
           
               },
                submitHandler:function(form) {
                $('button').prop('disabled',true);
                $.post('<?php echo $this->Url->build('/Staffs/emailexist')?>',{'email':$('#email').val()},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#emailERROR').remove();
                    $('#email').focus();
                    $('#email').after('<label for="Code" generated="true" class="error" id="emailERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                     form.submit();
                }
            });
        }
        
                  

    });
});
</script>

    <script>
$(document).ready(function(){

    $(".number_field").keyup(function() {
    var newval = $(this).val();    
    $(this).val(newval.replace(/[^0-9'\s]/gi, ''));                
    });

});
</script>

<?php echo $this->Html->css(array('../backend/bootstrap/date_picker/bootstrap-datepicker.min.css'));?>  
<?php echo $this->Html->script('../backend/bootstrap/date_picker/bootstrap-datepicker.min.js'); ?>
   <script>
     $(function(){
       $("#dob").datepicker({format: 'yyyy-mm-dd'});
    });

    
   </script>
    