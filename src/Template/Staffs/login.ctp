<div class="container login-form">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
           
           <?php echo $this->Form->create('',["name"=>'loginFrm',"id"=>"loginFrm","role"=>"form"]) ?>
            <fieldset>
                <h2>Venue Login</h2>
                <hr class="colorgraph">

                <span style="color:red;"><?php echo $this->Flash->render() ?></span>
                <div class="form-group">                  

                    <?php echo $this->Form->input('username',["class"=>"form-control input-lg required", "id"=>"userName","placeholder"=>"Username","autofocus"=>true,'autocomplete'=>'off']) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('password',["class"=>"form-control input-lg required", "placeholder"=>"Password","type"=>"password","autofocus"=>true]) ?>
                </div>
                 <div class="form-group">
                       <label>Select Event Type</label>
                       <?php echo $this->Form->select('event_type_id',$eventdata,['empty'=>'---Select---','class'=>'form-control required','id'=>'event_type_id']); ?>                        
                        </div>
                <span class="button-checkbox">
                   <!--  <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden"> -->
                    
                </span>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In">
                    </div>

                </div>
            </fieldset>
       <?php echo $this->Form->end() ?>
           
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
         $("#loginFrm").validate({
            rules:{
               
            },
         });
         
    });
</script>
