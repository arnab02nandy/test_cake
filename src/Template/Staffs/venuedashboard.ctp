 
        
        <div class="menu-wrapper container-fluid no-padd">
            <div class="container">
                <div class="dis-table">
                    <div class="menu-left dis-cell">
                        <p>Venue:</p>
                        <h3><?php echo $this->request->session()->read('venue.venue_name');?></h3>
                    </div>
                    
                    <div class="menu-right dis-cell">
                        <h3>Dashboard <div class="dropdown cust-dropdown-menu">
                          <button class="btn dropdown-toggle cust-navbar-toggle" type="button" data-toggle="dropdown">
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right">
                            <!-- <li><a href="<?php //echo $this->Url->build('/staff/dashboard');?>">Dashboard</a></li> -->
                            <li><a href="javascript:void(0);">Record student attendance</a></li>
                            <li><a href="<?php echo $this->Url->build('/staff/add-customer');?>">Enroll new student</a></li>
                            <li><a href="javascript:void(0);">Communicate with office</a></li>
                            <li><a href="javascript:void(0);">Class roster</a></li>
                            <li><a href="javascript:void(0);">Receive instalment payment</a></li>
                            <li><a href="<?php echo $this->Url->build('/Staffs/logout');?>">Log out</a></li>
                          </ul>
                        </div></h3>

                    </div>
                </div>
            </div>
        </div>


        

    
    <div class="main-content container-fluid no-padd">
       
       
       
       <div class="container">
        <div class="row">
            <div class="col-xs-12">
       
       
       <div class="panel panel-default panel-table venue-table">
                              <div class="panel-heading">
                                <div class="row">
<!--
                                  <div class="col col-xs-6">
                                    <h3 class="panel-title">Messages</h3>
                                  </div>
-->
                                  <div class="col col-xs-12 text-right">
                                   
<!-- <button type="button" class="btn btn-sm btn-default btn-create">Send Message</button> -->
                                      <a href="javascript:void(0)" alt="" title="Send Message" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr" data-href="<?php echo $this->Url->build('/staff/send-message');?>"><button type="button" class="btn btn-sm btn-default btn-create">Send Message</button></a>
                                  </div>
                                </div>
                              </div>
                              <div class="panel-body">
                               
                               
                               <div class="table-responsive">
                                <table class="table table-striped table-bordered table-list">
                                  <thead>
                                    <tr>
                                       
                                
                                        <th><em class="fa fa-calendar"></em>Date</th>
                                        <th><em class="fa fa-envelope"></em>Message From Office</th>
                                        <th class="text-center"><em class="fa fa-cog"></em>Action </th>
                                    </tr> 
                                  </thead>
                                  <tbody>

                  <?php                     
                  
                    if(!empty($rows))  
                                        {

                    foreach($rows as $row)
                    {?>
                                          <tr>
                                            
                                            <td><?php
                                            $date=explode(' ',$row['created']);

                                             $date = date('d-m-Y', strtotime($date[0]));
                      echo $date;?></td>
                                            <td><?php echo (strlen($row['message'])>150 ? substr($row['message'],0,150).'...': $row['message']);?></td>
                                            <td class="text-center">
                                              

                                              <a href="javascript:void(0)" alt="Edit" title="Click to view message" data-toggle="modal" data-target="#ModalPopUpID" class="Modal-Pop-Link-Apr btn btn-icon" data-href="<?php echo $this->Url->build('/staff/view-message/'.$row['id']);?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          
                                            </td>
                                          </tr>
                                          
                                        


                     <?php
                  
                    
                    }
                                     }

                                     else{?>
                                    <tr ><td colspan="4" align="center">No record found !</td></tr> 
                                   <?php  }
                                     ?>
                                     </tbody>
                                </table>

                                  </div>
                             
                             
                             
                             
                              </div>
                            <!-- <div class="panel-footer">
                                <div class="row">
                                  <div class="col col-xs-4">Page 1 of 5
                                  </div>
                                  <div class="col col-xs-8">
                                    <ul class="pagination hidden-xs pull-right">
                                      <li><a href="#">1</a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">4</a></li>
                                      <li><a href="#">5</a></li>
                                    </ul>
                                    <ul class="pagination visible-xs pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                  </div>
                                </div>
                              </div> -->
                            </div>
       
       
       
       
            </div>
           </div></div>
       
       
       
       
       
       
       <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row dash-item-wrapper">
                    <div class="col-sm-6 col-md-4">
                            <a href="#">
                            <div class="dash-item color-1">
                            
                                <div class="dash-inner-item">
                                    <div class="media">
                                        <div class="media-left">
                                          <!-- <img src="images/icon-1.png" alt="icon"> -->
                                           <?php echo $this->Html->image(BASE_URL.'/staff/images/icon-1.png')?>
                                        </div>
                                        <div class="media-body">
                                          <span class="content-text">Record student<br>attendance</span>
                                        </div>
                                    </div>
                                </div>
                                
                        </div>
                        </a>
                    </div>
                    
                     <div class="col-sm-6 col-md-4">
                       <a href="<?php echo $this->Url->build('/staff/add-customer');?>">
                        <div class="dash-item color-2">
                            
                                <div class="dash-inner-item">
                                    
                                    
                                    <div class="media">
                                        <div class="media-left">
                                          
                                          <?php echo $this->Html->image(BASE_URL.'/staff/images/icon-2.png')?>
                                        </div>
                                        <div class="media-body">
                                          <span class="content-text">Enroll new<br>
student</span>
                                        </div>
                                    </div>
                                </div>
                           
                            
                            
                        </div>
                         </a>
                    </div>
                    
        
                     <div class="col-sm-6 col-md-4">
                       <a href="#">
                        <div class="dash-item color-3">
                            
                                <div class="dash-inner-item">
                                    
                                    
                                    <div class="media">
                                        <div class="media-left">
                                          
                                          <?php echo $this->Html->image(BASE_URL.'/staff/images/icon-3.png')?>
                                        </div>
                                        <div class="media-body">
                                          <span class="content-text">Communicate<br>
with office</span>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            
                        </div>
                         </a>
                    </div>
                    
                    
                     <div class="col-sm-6 col-md-4">
                       <a href="#">
                        <div class="dash-item color-4">
                            
                                <div class="dash-inner-item">
                                    
                                    
                                    <div class="media">
                                        <div class="media-left">
                                         
                                          <?php echo $this->Html->image(BASE_URL.'/staff/images/icon-4.png')?>
                                        </div>
                                        <div class="media-body">
                                          <span class="content-text">Class roster</span>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            
                        </div>
                         </a>
                    </div>
                    
                    
                     <div class="col-sm-6 col-md-4">
                       
                       <a href="#">
                        <div class="dash-item color-5">
                            
                               
                                <div class="dash-inner-item">
                                    
                                    
                                    <div class="media">
                                        <div class="media-left">
                                         
                                          <?php echo $this->Html->image(BASE_URL.'/staff/images/icon-5.png')?>
                                        </div>
                                        <div class="media-body">
                                          <span class="content-text">Receive instalment<br>
payment</span>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            
                        </div>
                         </a>
                    </div>
                    
                     <div class="col-sm-6 col-md-4">
                       
                       <a href="<?php echo $this->Url->build('/Staffs/logout');?>">
                        <div class="dash-item color-6">
                            
                                <div class="dash-inner-item">
                                    
                                    
                                    <div class="media">
                                        <div class="media-left">                                         
                                          <?php echo $this->Html->image(BASE_URL.'/staff/images/icon-6.png')?>
                                        </div>
                                        <div class="media-body">
                                          <span class="content-text">Log out</span>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            
                        </div>
                         </a>
                    </div>


                   
                    
                    
                </div>
            </div>
        </div>
        </div>
    </div>