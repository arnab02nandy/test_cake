<input type="hidden" name="venue_name" id="venue_name"   value="<?php echo $this->request->session()->read('venue.venue_name');?>">
<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Message To Office</h4>
    </div>


    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                 <p class="pri-message success" id="success_msg" style="display:none;">                
              Your message has been sent successfully.               </p>
               <p class="pri-message faild" id="error_msg" style="display:none;">
                Error in mail sending, please try again.   
               </p>
                    <?php echo $this->Form->create('',['type' => 'file',"name"=>"f1","id"=>"f1","role"=>"form"]) ?>

                      
                        <div class="form-group">
                            <?php echo $this->Form->input('message',['label' =>["class"=>"control-label"],'rows' => '10', 'cols' => '10','placeholder' => __(''),'autofocus',"class"=>"form-control required","id"=>'message']);?>
                            <label for="message" generated="true" class="error validatorError" style="display:none;">This field is required.</label>
                        </div>               

                                                                   
                        
                        <div class="form-group">
                         <?php echo $this->Form->submit(__('Send'),["class"=>"btn btn-primary btn-bg-change",'id'=>'message_submit']) ?>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>                      
        </div>
    </div>
</div>



<script type="text/javascript">
   
    CKEDITOR.replace('message');
   
</script>

<style>
    .errorClass { border:  2px solid red !important; }
</style>


<script type="text/javascript">              
     function remove_style(id)
        {               
            $("#"+id).removeClass("errorClass");
        }  
    
     $(document).ready(function(){  
     $('#success_msg').hide();
     $('#error_msg').hide();     
        
     $("#message_submit").click(function(e){ 
    
     var venue_name               = $.trim($('#venue_name').val());     
     var message = CKEDITOR.instances['message'].getData();   
   
     if(!message)
      { 
        
           // $('#message').addClass('errorClass');  
           // $("#message").focus();
           $('.validatorError').show(); 
            return false;
      }
     
     
        
        e.preventDefault();
       
        $.ajax({         
              url : "<?php echo $this->Url->build('/Staffs/sendMail')?>",          
              type : "POST",
              data : {venue_name:venue_name,message:message},
              success:function(data)
              {                 
                if(data=="success") 
                {
                  $("#f1")[0].reset();
                  $('#error_msg').hide();                      
                  $('#success_msg').show(800).delay(4000).hide(800);  
                } 
                else 
                {
                  $("#f1")[0].reset();
                  $('#success_msg').hide();                     
                  $('#error_msg').show(800).delay(4000).hide(800); 
                }    
              } 
             });
    
        return false;   
             
    }); 

    }); 

 
         </script>

