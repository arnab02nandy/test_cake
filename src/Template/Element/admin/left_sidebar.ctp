<?php  $session=$this->request->session();
       $admin_id=$session->read('sp_admin.id');?>
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <!-- /.navbar-header -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
       
        
    </div>
    <!-- /.navbar-header -->
    <!-- /.dropdown -->
    <div class="btn-group nav navbar-top-links navbar-right drop-down-top">
        <a id="btn-append-to-single-button" type="button" class="btn btn-primary header-togl-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i></a>
        <ul class="dropdown-menu">
            <li><a href="<?php echo $this->Url->build('/admin/profile');?>">My Profile</a></li>
            <li><a href="<?php echo $this->Url->build('/admin/change-password');?>">Change Password</a></li>
            <li><a href="<?php echo $this->Url->build('/admin/logout');?>">Logout</a></li>
        </ul>
    </div>
    <!-- /.dropdown -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav left-side-ul-sec" id="side-menu">
                 <!--  :::: Dashboard ::::   -->
                <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='dashboard'){echo 'active left-sidebar-parent dashboard-home';} else{ echo 'left-sidebar-parent dashboard-home';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/home');?>"><i class="fa fa-fw fa-home fa-lg"></i>&nbsp;Dashboard</a>
                </li>
                <!--  :::: Site-Settings ::::   -->
                <?php if($session->read('sp_admin.type')==1){ ?>
                <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='SiteSetting'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="" href="<?php echo $this->Url->build('/admin/site-setting');?>"><i class="fa fa-fw fa-cogs fa-lg"></i>&nbsp;Site Settings</a>
                </li>
               
                

                 <li class="">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/state-list');?>"><i class="fa fa-university" aria-hidden="true"></i>&nbsp;Manage States</a>
                </li>

                <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='suburb-list'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/suburb-list');?>"><i class="fa fa-building" aria-hidden="true"></i>&nbsp;Manage Suburbs</a>
                </li> 
                
                 <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='Event-type-list'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/eventtype-list');?>"><i class="fa fa-first-order" aria-hidden="true"></i>&nbsp;Manage Event Types</a>
                </li>

                 <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='customer-type-list'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/customertype-list');?>"><i class="fa fa-first-order" aria-hidden="true"></i>&nbsp;Manage Customer Types</a>
                </li>

               
                <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='venue'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/venue-list');?>"><i class="fa fa-venus" aria-hidden="true"></i>&nbsp;Manage Venues</a>
                </li>
                

               


               


                <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='venue-staff'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/venue-staff-list');?>"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Manage Venue Staffs</a>
                </li>

                 <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='profile'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/profile');?>"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Manage Profile</a>
                </li>

                <li class="<?php if(isset($data['left_sidebar_parent']) && trim($data['left_sidebar_parent'])=='change-password'){echo 'active left-sidebar-parent';} else{ echo 'left-sidebar-parent';}?>">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/change-password');?>"><i class="fa fa-edit" aria-hidden="true"></i>&nbsp;Change Password</a>
                </li>

                          

                 <li class="">
                    <a class="nav-group-li-sec" href="<?php echo $this->Url->build('/admin/logout');?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
                </li>


               

                <?php } ?>
                
               
               
                
                
                
            </ul>
        </div>
    </div>
    <!-- /.navbar-static-side -->
</nav>