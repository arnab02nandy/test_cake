<!-- bootstrap JavaScript -->
<?php echo $this->Html->script('../backend/bootstrap/js/bootstrap.min'); ?>
<!-- Metis Menu Plugin JavaScript -->
<?php echo $this->Html->script('../backend/bootstrap/metisMenu/metisMenu.min'); ?>
<!-- bootstrap admin theme JavaScript -->
<?php echo $this->Html->script('../backend/bootstrap/js/admin_theme'); ?>
<!-- jquery validator -->
<?php echo $this->Html->script('../backend/js/jquery.validate'); ?>
<!-- app JavaScript -->
<?php echo $this->Html->script('../backend/js/common'); ?>
<!-- DataTables JavaScript -->
<?php echo $this->Html->script('../backend/bootstrap/datatables/media/js/jquery.dataTables.min'); ?>
<?php echo $this->Html->script('../backend/bootstrap/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min'); ?>


<!-- script Modal pop -->
<script>
	$("body").on("click",'.Modal-Pop-Link-Apr', function(){
		var action_link = $(this).data('href');
		//$('#ModalPopUpID').modal({backdrop: 'static',keyboard: true, show: true});
		$('#Modal-Pop-Content').attr('style','margin-top:113px !important;');
		$("#Modal-Pop-Content").html('<div style="text-align: center;text-align: center;font-size: 20px;font-weight: bold;margin-top: 70px; margin-bottom:40px;"><?php echo $this->Html->image(BASE_URL.'backend/images/loading-loader_m.gif');?></div>');
		$.get(action_link,function(return_data){
            $('#Modal-Pop-Content').attr('style','margin-top:70px !important;');
			$("#Modal-Pop-Content").html(return_data);
		});
	});
</script>
<script>
$(document).ready(function(){

    $(".order_field").keyup(function() {
    var newval = $(this).val();    
    $(this).val(newval.replace(/[^0-9'\s]/gi, ''));                
    });

    $(".number_field").keyup(function() {
    var newval = $(this).val();    
    $(this).val(newval.replace(/[^0-9'\s]/gi, ''));                
    });

});
</script>

 