<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="">
        <meta name="description" content="<?php  echo isset($meta_data['meta_desc']) ? trim($meta_data['meta_desc']) : "";?>">
        <title><?php  echo isset($meta_data['meta_title']) ? trim($meta_data['meta_title']) : "";?></title>
        <!-- Bootstrap Core CSS -->
        <?php echo $this->Html->css(array('../backend/bootstrap/css/bootstrap.min'));?>
        <!-- Custom Fonts -->
        <?php //echo $this->Html->css(array('../backend/font-awesome/css/font-awesome.min'));?>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Timmana" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
        <!-- Custom CSS -->
        <?php echo $this->Html->css(array('../backend/css/style'));?>
        <!-- DataTables CSS -->
        <?php echo $this->Html->css(array('../backend/bootstrap/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap'));?>
        <!-- DataTables Responsive CSS -->
        <?php //echo $this->Html->css(array('../backend/bootstrap/datatables-responsive/css/dataTables.responsive'));?>
        <!-- jquery JavaScript -->
        <?php echo $this->Html->script('../backend/jquery/jquery.min'); ?>
		<?php echo $this->Html->script('../backend/js/ckeditor/ckeditor.js');?>
    </head>
<body>
    <!-----./Modal Pop-Up------->
    <div id="ModalPopUpID" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog"><div class="modal-content" id="Modal-Pop-Content"></div></div>
    </div>