<?php if(isset($params['class'])){?>
<div class="alert <?php echo isset($params['class']) ? $params['class'] : ""; ?> alert-dismissable">
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
<?php echo isset($message) ? $message : "";?>
</div>
<?php } else{ ?>
<div class="message success" onclick="this.classList.add('hidden')"><?php echo $message;?></div>
<?php }?>
