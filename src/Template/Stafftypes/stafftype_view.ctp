<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Offer Details </h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">

             <div class="table-responsive tfModalTable">           
             <table class="table table-bordered">            
             <tbody>              
              <tr>
                <td><label>Offer Title:&nbsp;</label></td>
                <td><p><?php echo $row->offer_title ?></p></td>
              </tr>
              <tr>
                <td><label>Cost(AUD):&nbsp;</label></td>
                <td><p>$<?php echo $row->cost ?></p></td>
              </tr>
              <tr>
                <td><label>Offer Start Date:&nbsp;</label></td>
                <td> <p><?php echo $row->start_date ?></p></td>
              </tr>

              <tr>
                <td><label>Offer End Date:&nbsp;</label></td>
                <td> <p><?php echo $row->end_date ?></p></td>
              </tr>
             
              <tr>
                <td> <label>Description:&nbsp;</label></td>
                <td><p> <?php echo $row->description ?></p></td>
              </tr>

            </tbody>
            </table>
            </div>

        </div>
    </div>
</div>
