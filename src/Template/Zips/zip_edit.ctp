<div class="custom-modal-container-sec">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo isset($data['heading']) ? trim($data['heading']) : "Add";?></h4>
    </div>
    <div class="modal-body" id="ModalBodySec">
        <button class="btn btn-outline btn-lg btn-block" type="button" id="ModalNotify" style="display:none;"></button>
        <div class="modal-form-section">
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <?php echo $this->Form->create($zip,['type' => 'file',"name"=>"editZipFrm","id"=>"editZipFrm","role"=>"form"]) ?>
                        
					<div class="form-group">
                    <label>Select State</label>
                    <?php echo $this->Form->select('state_id',$statedata,['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'state_id']); ?>
                </div>	
					<div class="form-group">
					<label>Select Suburb</label>
                    <?php echo $this->Form->select('city_id',$citydata,['empty' => '---Select---','class'=>'form-control required','autofocus','id'=>'city_id']); ?>
                </div>	
					
					
					
					<div class="form-group">
                    <label for="zip" class="control-label">Zip Code</label>
                            <?php echo $this->Form->input('zip',['label' =>["class"=>"control-label"],'placeholder' => __('Zip Code'),'autofocus',"class"=>"form-control required","id"=>'zipName','label'=>false]);?>
                        </div>
                       
                        <div class="form-group">
                         <?php echo $this->Form->button(__('Save'),["class"=>"btn btn-primary btn-bg-change"]) ?>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>						
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $("#editZipFrm").validate({
        rules: {

             'zip':{'digits':$("#zipName").val(),'maxlength':4},
            
           
        },
       /* submitHandler:function(form) {
           $('button').prop('disabled',true);
            $.post('<?php echo $this->Url->build('/Zips/zipExixts')?>',{'id':'<?php echo $zip->id;?>','zip':$('#zipName').val()},function(rtn_data) {
                var rtn_data_arr=JSON.parse(rtn_data);
                if(rtn_data_arr.status!==undefined && rtn_data_arr.status===false){
                    $('#zipNameERROR').remove();
                    $('#zipName').after('<label for="Code" generated="true" class="error" id="zipNameERROR">'+rtn_data_arr.msg+'</label>');
                    $('button').prop('disabled',false);
                } else {
                     form.submit();
                }
            });
        }*/
    });
	
	
	$("#state_id").change(function(){
		var a=$(this).val();
		
    $("#city_id").html('<option value="">---Select---</option>');
    $.post('<?php echo $this->Url->build('/Zips/getCity');?>',{'state_id':$(this).val()},function(rtn_data){
        var rtn_data_arr=JSON.parse(rtn_data);
		
        if(rtn_data_arr.status!==undefined && rtn_data_arr.status===true){
            $.each(rtn_data_arr.data,function(k,val){
                $("#city_id").append('<option value="'+k+'">'+val+'</option>');
            });
        }
    });
});    
	
	
	
	
	
	
	
});
</script>