<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasOne('UserProfiles', [
            'className' => 'UserProfiles',
            'foreignKey' => false,
            'conditions' =>['Users.id = UserProfiles.user_id']
        ]);
       /* $this->hasOne('UserTypes',[
            'className' => 'UserTypes',
            'foreignKey' => false,
            'conditions' =>['Users.user_type_id = UserTypes.type_id']
        ]);*/
       
        
        
        
    }

    
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'Username is required')
            ->notEmpty('email', 'Email is required')
            ->notEmpty('password', 'Password is required')
            ->notEmpty('user_type_id', 'User Type is required')
            ->add('username', 'unique', ['rule' => 'validateUnique','provider' => 'table','message' => 'Username must be unique.'])
            ->add('email', 'valid' , ['rule'=> 'email']);
            //->add('email', 'unique', ['rule' => 'validateUnique','provider' => 'table','message' => 'Email must be unique.']);
            
    }

     public function validationUpdate($validator)
    {
        $validator
                ->requirePresence('firstname');

        $validator
                ->requirePresence('lastname');

        $validator
                ->requirePresence('username');

        $validator
                ->requirePresence('email')
                ->add('email', 'validFormat', [
                    'rule' => 'email',
                    'message' => 'E-mail must be valid'
        ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
