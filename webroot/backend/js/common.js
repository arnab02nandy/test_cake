$(document).ready(function(){   
	$("body").on("click",'.pop-link', function(){
        $("#Modal-Pop-Content").empty();
		$("#pop-content").html('<div style="text-align: center;text-align: center;font-size: 20px;font-weight: bold;margin-top: 70px;">Loading, Please Wait.</div>');
		var html = $(this).data('href');
		$("#pop-content").load(html, function(){});
	});
    $("body").on('click',"#ModalPopUpID .close",function(){
        $("#Modal-Pop-Content").empty();
    });

//==========================  Frontend =======================================//
//========================== Apply Job ======================================//
 //==========::: ChangeStatus :::=========
    $("body").on("click",'.ApplyJob', function(){
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        
        var job_id = $(this).data('jobid');        
        var job_member_id = $(this).data('jobmemberid');
        var apply_member_id = $(this).data('applyid');

        var content_id= $(this).attr('id');
        //console.log(member_id+"===>"+job_id+"=====>>"+content_id+"======>>"+action_link);
        $('#notifyMessage').empty();

        if(confirm('Are you sure want to apply the job?')){
         $.post(action_link,{"job_id":job_id,"job_member_id":job_member_id,"apply_member_id":apply_member_id },function(data_arr){
            var return_data=$.parseJSON(data_arr);
           // alert(return_data)
            //console.log(return_data);
            msg2=return_data.msg;
            //alert(msg2)
            if(msg2=="already_applied")
            {
            alert("You have already applied the job");
            }
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
                   //  $("#"+content_id).html('Applied');               
               
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    });


    //==========::: ChangeStatus :::=========
    $("body").on("click",'.ChangeStatus', function(){
        
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        var id = $(this).data('id');
        var content_id= $(this).attr('id');
        //console.log(content_id+"=====>>"+id+"======>>"+action_link);
        $('#notifyMessage').empty();
        if(confirm('Are you sure want to change the status?')){
         $.post(action_link,{"id":id},function(data_arr){
            var return_data=$.parseJSON(data_arr);
            //console.log(return_data);
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
                if(return_data.change_status!==undefined && return_data.change_status!==null && return_data.change_status.trim()==="ACTIVE"){
                     $("#"+content_id).html('<i style="color:#439f43" title="Click to Inactive" class="fa fa-check-square fa-lg" aria-hidden="true"></i>');
                }
                if(return_data.change_status!==undefined && return_data.change_status!==null && return_data.change_status.trim()==="INACTIVE"){
                     $("#"+content_id).html('<i style="color:#FF0000" title="Click to Active" class="fa fa-check-square fa-lg" aria-hidden="true"></i>');
                }
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    });

 //==========::: ChangeFeatured :::=========
    $("body").on("click",'.ChangePurchased', function(){

       
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        var id = $(this).data('id');
        var content_id= $(this).attr('id');
        //console.log(content_id+"=====>>"+id+"======>>"+action_link);
        $('#notifyMessage').empty();
        if(confirm('Are you sure want to change the purchased status?')){
         $.post(action_link,{"id":id},function(data_arr){
            var return_data=$.parseJSON(data_arr);
            console.log(return_data);
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
                if(return_data.change_status!==undefined && return_data.change_status!==null && return_data.change_status.trim()==="YES"){
                     $("#"+content_id).html('<span style="color:#439f43;text-decoration:underline;" title="Click to do No"  aria-hidden="true">Yes</span>');
                }
                if(return_data.change_status!==undefined && return_data.change_status!==null && return_data.change_status.trim()==="NO"){
                     $("#"+content_id).html('<span style="color:#FF0000;text-decoration:underline;" title="Click to do Yes"  aria-hidden="true">No</span>');
                }
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    });



    //================delete===========
    $("body").on("click",'.deleteDataRow', function(){
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        var id = $(this).data('id');
        var content_id='dataRow_'+id;
        //console.log(content_id+"=====>>"+id+"======>>"+action_link);
        $('#notifyMessage').empty();
        if(confirm('Are you sure want to delete?')){
         $.post(action_link,{"id":id},function(data_arr){
            var return_data=$.parseJSON(data_arr);
            //console.log(return_data);
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
                $("#"+content_id).remove();
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    }); 
    //===========change payment status=============
    $("body").on("click",'.PaymentChangeStatus', function(){
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        var id = $(this).data('id');
        var content_id= $(this).attr('id');
        //console.log(content_id+"=====>>"+id+"======>>"+action_link);
        $('#notifyMessage').empty();
        if(confirm('Are you sure want to change the status?')){
         $.post(action_link,{"id":id},function(data_arr){
            var return_data=$.parseJSON(data_arr);
            //console.log(return_data);
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
                $("#"+content_id).parent().text("Paid");
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    });
    
    //===========User-level-Upgrade=============
    $("body").on("click",'.UpgradeStudentLevel', function(){
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        var id = $(this).data('id');
        var content_id= $(this).attr('id');
        $('#notifyMessage').empty();
        if(confirm('Are you sure want to upgrade level?')){
         $.post(action_link,{"id":id},function(data_arr){
            var return_data=$.parseJSON(data_arr);
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    });
    
    
    //===========User-level-Decrease=============
    $("body").on("click",'.DecreaseStudentLevel', function(){
        $('#notifyMessage').empty();
        var action_link = $(this).data('href');
        var id = $(this).data('id');
        var content_id= $(this).attr('id');
        $('#notifyMessage').empty();
        if(confirm('Are you sure want to decrease level?')){
         $.post(action_link,{"id":id},function(data_arr){
            var return_data=$.parseJSON(data_arr);
            var alert_class="alert alert-danger alert-dismissable";
            var msg="Please try again later";
            if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='SUCCESS'){
                alert_class="alert alert-success alert-dismissable";
                msg=return_data.msg;
            }else if(return_data.status!==undefined && return_data.status!==null && return_data.status.trim()==='ERROR'){
                alert_class="alert alert-danger alert-dismissable";
                msg=return_data.msg;
            }
            var append_data='<div class="'+alert_class+'"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>'+msg+'</div>';
            $('#notifyMessage').html(append_data);
         });
        }
    });
    



    
});


		$(document).ready(function(){
            $('.work_container .slide-toggle-btn').click(function(){
                $('.work_container .item-wrapper').slideToggle();
				$('.slide-toggle-btn').toggleClass("icon-rotate");
        });

        $(window).resize(function(){
			var wsize = $(window).width();
			if(wsize >= 768){
				$('.item-wrapper').show();
				
			}else{
				$('.item-wrapper').hide();
				
			}
			
				
		});
		
		
       /* $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 2,
        itemsDesktop : [1000,2], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,2], // betweem 900px and 601px
        itemsTablet: [767,1], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        });

            $("#button").click(function() {
              $('html, body').animate({
                  scrollTop: $("#work").offset().top
              }, 2000);
          }); 

*/

      $('#send_cnt').click(function(){
      var tab = new Array();
      var name=$('#name').val();
      var email=$('#email').val();
      var phone =$('#phone_no').val();
      var message=$('#message').val();
      $(".form-control").removeClass("required_contact");
      if(name=="")
      {
         $( "#name" ).focus();
         $( "#name" ).addClass( "required_contact" );
         return false;
      }
      if(email=="")
      {
        $( "#email" ).focus();
        $( "#email" ).addClass( "required_contact" );
         return false;
      }
      if(!isValidEmailAddress(email))
            {
               $( "#email" ).focus();
                $( "#email" ).addClass( "required_contact" );
                 return false; 
            }

     if(!phone)
      { 
            $('#phone_no').addClass('required_contact');  
            $("#phone_no").focus();
            return false;
      }

    if(phone.length !=10){
            $('#phone_no').addClass('required_contact');  
            $('#phone_no').focus();
            
            return false;
        }

        if(!message)
      { 
            $('#message').addClass('required_contact');  
            $("#message").focus();
            return false;
      }
       $.ajax({
                type:"POST",
                data:{ name: name, email: email,phone:phone,message:message },
               // url:"<?php //echo Router::url(array('controller'=>'Home','action'=>'sendmail'));?>",
               url:"Home/sendmail",
                dataType: 'text',
                success: function(tab){
                    //alert('sucess');
                    if(tab=="SUCCESS")
                    {
                        $("#cntform")[0].reset();
                        $('#suc').hide();
                        $('#er').hide();
                       $('#suc').show(800).delay(4000).hide(800);  
                    }
                    else
                    {
                        $("#cntform")[0].reset();
                        $('#suc').hide();
                        $('#er').hide();
                        $('#er').show(800).delay(4000).hide(800);  
                    }
                   
                },
                error: function (tab) {
                    $('#suc').hide();
                        $('#er').hide();
                   $('#er').show(800).delay(4000).hide(800);;  
                }
            });
    });


    $("#phone_no").keyup(function() {

    var current_val = $(this).val();
        
        $(this).val(current_val.replace(/[^0-9'\s]/gi, ''));
                    
        });

            
        });

        function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

/*--------------------- Image validation --------------------------*/








  

    