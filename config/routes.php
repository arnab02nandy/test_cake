<?php
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
Router::extensions(['pdf']);
Router::defaultRouteClass('DashedRoute');




Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
    $routes->fallbacks('DashedRoute');
});


    
 



///////////////////////////////////////   Admn  /////////////////////////////////////////////////////////////

 Router::scope('/admin', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login','login']);

    $routes->connect('/home', ['controller' => 'Users', 'action' => 'index','home']);
    //==========Common========
    $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout','logout']);
    $routes->connect('/profile', ['controller' => 'Users', 'action' => 'myProfile','profile']);
    $routes->connect('/change-password', ['controller' => 'Users', 'action' => 'changePassword','change-password']);
    //=======SiteSetting======
    $routes->connect('/site-setting', ['controller'=>'SiteSettings','action'=>'siteSettingUpdate','site-setting']);
	 
	 

	 
	//=======State===================================================
    $routes->connect('/state-list', ['controller'=>'States','action'=>'stateList','state-list']); 
	$routes->connect('/state-add', ['controller'=>'States','action'=>'stateAdd','state-add']); 
	$routes->connect('/state-edit/:id', ['controller' => 'States', 'action' => 'stateEdit'],['id' => '\d+','pass' => ['id']],'state-edit');  
	 
	 
	 //=======City===================================================
    $routes->connect('/city-list', ['controller'=>'Cities','action'=>'cityList','city-list']); 
	$routes->connect('/city-add', ['controller'=>'Cities','action'=>'cityAdd','city-add']); 
	$routes->connect('/city-edit/:id', ['controller' => 'Cities', 'action' => 'cityEdit'],['id' => '\d+','pass' => ['id']],'city-edit');


	 //=======Suburb===================================================
    $routes->connect('/suburb-list', ['controller'=>'Suburbs','action'=>'suburbList','suburb-list']); 
	$routes->connect('/suburb-add', ['controller'=>'Suburbs','action'=>'suburbAdd','suburb-add']); 
	$routes->connect('/suburb-edit/:id', ['controller' => 'Suburbs', 'action' => 'suburbEdit'],['id' => '\d+','pass' => ['id']],'suburb-edit');

	//=======Event Type===================================================
    $routes->connect('/eventtype-list', ['controller'=>'Eventtypes','action'=>'eventtypeList','eventtype-list']); 
	$routes->connect('/eventtype-add', ['controller'=>'Eventtypes','action'=>'eventtypeAdd','eventtype-add']); 
	$routes->connect('/eventtype-edit/:id', ['controller' => 'Eventtypes', 'action' => 'eventtypeEdit'],['id' => '\d+','pass' => ['id']],'eventtype-edit');

	//=======Customer Type===================================================
    $routes->connect('/customertype-list', ['controller'=>'Customertypes','action'=>'customertypeList','customertype-list']); 
	$routes->connect('/customertype-add', ['controller'=>'Customertypes','action'=>'customertypeAdd','customertype-add']); 
	$routes->connect('/customertype-edit/:id', ['controller' => 'Customertypes', 'action' => 'customertypeEdit'],['id' => '\d+','pass' => ['id']],'customertype-edit');

	//=======     Venue   ===================================================
    $routes->connect('/venue-list', ['controller'=>'Venues','action'=>'venueList','venue-list']); 
	$routes->connect('/venue-add', ['controller'=>'Venues','action'=>'venueAdd','venue-add']); 
	$routes->connect('/venue-edit/:id', ['controller' => 'Venues', 'action' => 'venueEdit'],['id' => '\d+','pass' => ['id']],'venue-edit'); 
	

	//=======     Venue staff type  ===================================================
    $routes->connect('/staff-type-list', ['controller'=>'Stafftypes','action'=>'stafftypeList','staff-type-list']); 
	$routes->connect('/staff-type-add', ['controller'=>'Stafftypes','action'=>'stafftypeAdd','staff-type-add']); 
	$routes->connect('/staff-type-edit/:id', ['controller' => 'Stafftypes', 'action' => 'stafftypeEdit'],['id' => '\d+','pass' => ['id']],'staff-type-edit'); 

	//=======     Venue staff  ===================================================
    $routes->connect('/venue-staff-list', ['controller'=>'Users','action'=>'venuestaffList','venue-staff-list']); 
	$routes->connect('/venue-staff-add', ['controller'=>'Users','action'=>'venuestaffAdd','venue-staff-add']); 
	$routes->connect('/venue-staff-edit/:id', ['controller' => 'Users', 'action' => 'venuestaffEdit'],['id' => '\d+','pass' => ['id']],'venue-staff-edit'); 

	$routes->connect('/venue-staff-view/:id', ['controller' => 'Users', 'action' => 'venuestaffView'],['id' => '\d+','pass' => ['id']],'venue-staff-view'); 
	$routes->connect('/venue-staff-tagging/:id', ['controller' => 'Users', 'action' => 'venuestaffTag'],['id' => '\d+','pass' => ['id']],'venue-staff-tagging'); 

	//=======     Customer  ===================================================
    $routes->connect('/customer-list', ['controller'=>'Users','action'=>'customerList','customer-list']); 
	$routes->connect('/customer-add', ['controller'=>'Users','action'=>'customerAdd','customer-add']); 
	$routes->connect('/customer-edit/:id', ['controller' => 'Users', 'action' => 'customerEdit'],['id' => '\d+','pass' => ['id']],'customer-edit'); 

	$routes->connect('/customer-view/:id', ['controller' => 'Users', 'action' => 'customerView'],['id' => '\d+','pass' => ['id']],'customer-view');
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	 
    $routes->fallbacks('DashedRoute');
});
 


Plugin::routes();
